!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3760

! Object: Unglazed Transpired Collector System
! Simulation Studio Model: Type3760
! 

! Author: RS+DF
! Editor: 
! Date:	 September 05, 2013
! last modified: Juin 2014
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Collector area	m^2 [0;1000]
!			Collector height	m [0;1000]
!			Collector hole diameter	mm [0.5;10]
!			Collector hole pitch	mm [10;30]
!			Collector emissivity	- [0;1]
!			Collector absorptivity	- [0;1]
!			Plenum depth	mm [10;500]
!			Emissivity of the wall behind the collector	- [0;1]
!			R-value of the wall behind the collector	h.m2.K/kJ [0.5;+Inf]
!			Total UA-value of the building walls and roof	kJ/hr.K [0;+Inf]
!			Point 1 D�bit m1	m^3/hr [0;+Inf]
!			Point 1 dP 	Pa [-Inf;+Inf]
!			Point 2 D�bit m1	m^3/hr [0;+Inf]
!			Point 2 dP 	Pa [-Inf;+Inf]
!			Point 3 D�bit m1	m^3/hr [0;+Inf]
!			Point 3 dP 	Pa [-Inf;+Inf]
!			Rendement	% (base 100) [0;100]
!			% r�cup�ration r�chauffage air	% (base 100) [0;100]
!             Mode calcul coef hc

! *** 
! *** Model Inputs 
! *** 
!			Room air temperature	C [-Inf;+Inf]
!			Ambient temperature	C [-Inf;+Inf]
!			Sky temperature	C [-Inf;+Inf]
!			Vent	m/s [0;+Inf]
!			Atmospheric pressure	atm [0.5;2]
!			Radiation incident on the collector	kJ/hr.m^2 [0;+Inf]
!			Internal gains	kJ/hr [-Inf;+Inf]
!			Supply air flow rate from collector air-handling units	m^3/hr [-Inf;+Inf]
!			Minimum outdoor air flow rate collector/summer bypass damper	m^3/hr [-Inf;+Inf]
!			Supply air flow rate from no-collector air-handling units	m^3/hr [-Inf;+Inf]
!			Outdoor airflow rate through no collector	m^3/hr [-Inf;+Inf]
!			Ambiant air temperature above which the summer bypass is opened	C [-Inf;+Inf]
!			Maximum auxiliary heat rate available	kJ/hr [-Inf;+Inf]
!			 bypass open	- [0;1]

! *** 
! *** Model Outputs 
! *** 
!			Plenum air temperature	C [-Inf;+Inf]
!			Collector outlet air temperature	C [-Inf;+Inf]
!			Mixed air temperature	C [-Inf;+Inf]
!			Supply air temperature	C [-Inf;+Inf]
!			Collector surface air temperature	C [-Inf;+Inf]
!			Energy savings rate	kJ/hr [-Inf;+Inf]
!			Convection from collector	kJ/hr [-Inf;+Inf]
!			Convection from wall	kJ/hr [-Inf;+Inf]
!			Radiation from collector	kJ/hr [-Inf;+Inf]
!			Radiation from wall	kJ/hr [-Inf;+Inf]
!			Conduction through wall	kJ/hr [-Inf;+Inf]
!			Reduced conduction through wall because of collector	kJ/hr [-Inf;+Inf]
!			Absorbed energy rate	kJ/hr [-Inf;+Inf]
!			Auxiliary energy rate	kJ/hr [-Inf;+Inf]
!			Outdoor airflow rate through collector/summer bypass damper	m^3/hr [-Inf;+Inf]
!			Heat exchanger effectiveness of collector	- [0;1]
!			Solar efficiency of collector	% (base 1) [0;1]
!			Pressure drop across collector plate	kPa [-Inf;+Inf]
!			Bypass damper position 0=open ; 1=closed	- [-Inf;+Inf]
!			Heat rate supplied by traditional heating system	kJ/hr [-Inf;+Inf]
!			Additional fan power required	kJ/hr [-Inf;+Inf]
!			Qconv-col-sur	kJ/hr [-Inf;+Inf]

! *** 
! *** Model Derivatives 
! *** 
* For this model to correctly calculate the performance of transpired
* collectors, the approach velocity (appvel) should be greater than
* 72 m/hr.  Otherwise, there will be convection losses from the
* collector between the holes, and the collector's performance will be
* reduced.  Also, the collector pressure drop (pcol) should be at least
* 0.025 Pa to ensure uniform flow through the collector.  Otherwise,
* sections of the collector will become hotter than others, and
* radiation losses from the collector will increase.  Again, this will
* reduce the collector's performance.  To achieve a sufficient pressure
* drop, the porosity (por) should be about 0.005 to 0.01 for the given
* approach velocities.  If these approach velocity and pressure drop
* conditions are not met, this subroutine will write a warning to a
* file.  It is important to emphasize that a collector _can_ be operated
* at approach velocities and pressure drops below these values, but this
* model will just over predict the performance.
*
* Written by David Summers, Solar Energy Lab, U. Wisconsin - Madison
* for M.S. thesis, December 1995.
! (Comments and routine interface generated by TRNSYS Studio)
!************************************************************************



      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type3760

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      DOUBLE PRECISION area, ht, diam, pitch, emisc, absor, depth, emisw, rwall,ua
      DOUBLE PRECISION Qv_1, Qv_2, Qv_3
      DOUBLE PRECISION deltaP1, DeltaP2, DeltaP3
      DOUBLE PRECISION Rend_Vent
      DOUBLE PRECISION Recup
      INTEGER Mode_calcul_hc

!    INPUTS
      DOUBLE PRECISION troom
      DOUBLE PRECISION tamb
      DOUBLE PRECISION tsky
      DOUBLE PRECISION Vent
      DOUBLE PRECISION patm
      DOUBLE PRECISION rad
      DOUBLE PRECISION gain
      DOUBLE PRECISION flow1, minout1, flow2, out2

      DOUBLE PRECISION tbypass, qmax, nitebp

      
! Calculs
*     values calculated directly from parameters and inputs
      DOUBLE PRECISION por, surfa, udwall, qabs, tgnd, tsur, qbldg
      DOUBLE PRECISION aircp, aircond, airvisc, airden, a0, a1, a2
      DOUBLE PRECISION mflow1, mflow2, mflow, gmin, beta, qtrad


*     outputs and other variables
      DOUBLE PRECISION gamma, tout, tcol, tplen, twall, qvcol, qrcol, qvwall
      DOUBLE PRECISION qrwall, qdwall, red, effhx, appvel
      DOUBLE PRECISION tmix, tsup, qaux1, lowg, hig, oldg, dif, diflim, small, g
      DOUBLE PRECISION zeta, pcol, plenden, f, dh, plenvel, pfric, pbuoy, pacc
      DOUBLE PRECISION delp, fanpw
      DOUBLE PRECISION qaux2, qaux, film, tsolair, qpot, qred, qu, qsave, soleff
      integer istr, i, j, jmax
      DOUBLE PRECISION warn
      
      DOUBLE PRECISION bypass, qvcap, gammaflow1
      DOUBLE PRECISION coef_a, coef_b, coef_c 
      DOUBLE PRECISION CoefX_abc(3), CoefA_abc(3,3), dPreseau, fanpw_reseau
      DOUBLE PRECISION dT, Tsup_back, Alpha
      
     
*-----------------------------------------------------------------------

      diflim   = 1.0e-4
      small    = 1.0e-4
      jmax     = 1000
      g        = 9.8 * 3600**2    ! acceleration of gravity, m/hr2

*-----------------------------------------------------------------------
     


!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time        = getSimulationTime()
      Timestep    = getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then

		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(19)      !The number of parameters that the the model wants
		Call SetNumberofInputs(14)          !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)      !The number of derivatives that the the model wants
		Call SetNumberofOutputs(22)         !The number of outputs that the the model produces
		Call SetIterationMode(1)            !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(3,0)  !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0) !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      area                = getParameterValue(1)
      ht                  = getParameterValue(2)
      Diam                = getParameterValue(3)/1000.0
      pitch               = getParameterValue(4)/1000.0
      emisc               = getParameterValue(5)
      absor               = getParameterValue(6)
      depth               = getParameterValue(7)/1000.0
      emisw               = getParameterValue(8)
      rwall               = getParameterValue(9)
      ua                  = getParameterValue(10)
      Qv_1                = getParameterValue(11)
      deltaP1             = getParameterValue(12)
      Qv_2                = getParameterValue(13)
      deltaP2             = getParameterValue(14)
      Qv_3                = getParameterValue(15)
      deltaP3             = getParameterValue(16)
      Rend_Vent           = getParameterValue(17)/100.0
      Recup               = getParameterValue(18)/100.0
      Mode_calcul_hc      = NINT(getParameterValue(19))

      
      troom               = GetInputValue(1)
      tamb                = GetInputValue(2)
      tsky                = GetInputValue(3)
      Vent                = GetInputValue(4)
      patm                = GetInputValue(5)*101.325  !Conversion en kPa
      rad                 = GetInputValue(6)
      gain                = GetInputValue(7)
      flow1               = GetInputValue(8)
      minout1             = GetInputValue(9)
      flow2               = GetInputValue(10)
      out2                = GetInputValue(11)
      tbypass             = GetInputValue(12)
      qmax                = GetInputValue(13)
      nitebp              = NINT(GetInputValue(14))  !bypass capteurs

	
!Check the Parameters for Problems (#,ErrorType,Text)
!Sample Code: If( PAR1 <= 0.) Call FoundBadParameter(1,'Fatal','The first parameter provided to this model is not acceptable.')
      
      i=0
      Coef_a=0.0; Coef_b=0.0; Coef_c=0.0
      If ((Qv_1+Qv_2+Qv_3)<0.001)             i=1
      If ((DeltaP1+DeltaP2+DeltaP3)<0.001)    i=i
      If (i==1) then 
          Coef_a=0.0; Coef_b=0.0; Coef_c=0.0  
      Else
          CoefX_abc(1)=deltaP1; CoefX_abc(2)=deltaP2; CoefX_abc(3)=deltaP3
          
          CoefA_abc(1,1)=1.0      ; CoefA_abc(2,1)=1.0        ; CoefA_abc(3,1)=1.0 
          CoefA_abc(1,2)=Qv_1     ; CoefA_abc(2,2)=Qv_2       ; CoefA_abc(3,2)=Qv_3 
          CoefA_abc(1,3)=Qv_1**2  ; CoefA_abc(2,3)=Qv_2**2    ; CoefA_abc(3,3)=Qv_3**2 
          
          i=0
          Call InvertMatrix(3,3,CoefA_abc,i) 
          if ( i /= 0  ) then
                Call FoundBadParameter(1,'Fatal','Incoherence dans les parametres PAR(12) a PAR(16)')
          endif
          CoefX_abc=MATMUL(CoefA_abc,CoefX_abc)
      End if
      

!Set the Initial Values of the Outputs (#,Value)
		Call SetOutputValue(1, 20) ! Plenum air temperature
		Call SetOutputValue(2, 20) ! Collector outlet air temperature
		Call SetOutputValue(3, 20) ! Mixed air temperature
		Call SetOutputValue(4, 20) ! Supply air temperature
		Call SetOutputValue(5, 20) ! Collector surface air temperature
		Call SetOutputValue(6,  0) ! Energy savings rate
		Call SetOutputValue(7,  0) ! Convection from collector
		Call SetOutputValue(8,  0) ! Convection from wall
		Call SetOutputValue(9,  0) ! Radiation from collector
		Call SetOutputValue(10, 0) ! Radiation from wall
		Call SetOutputValue(11, 0) ! Conduction through wall
		Call SetOutputValue(12, 0) ! Reduced conduction through wall because of collector
		Call SetOutputValue(13, 0) ! Absorbed energy rate
		Call SetOutputValue(14, 0) ! Auxiliary energy rate
		Call SetOutputValue(15, 0) ! Outdoor airflow rate through collector/summer bypass damper
		Call SetOutputValue(16, 0) ! Heat exchanger effectiveness of collector
		Call SetOutputValue(17, 0) ! Solar efficiency of collector
		Call SetOutputValue(18, 0) ! Pressure drop across collector plate
		Call SetOutputValue(19, 0) ! Bypass damper position 0=open ; 1=closed
		Call SetOutputValue(20, 0) ! Heat rate supplied by traditional heating system
		Call SetOutputValue(21, 0) ! Additional fan power required
		Call SetOutputValue(22, 0) ! Qconv-col-sur

          

!If Needed, Set the Initial Values of the Static Storage Variables (#,Value)
      Call SetStaticArrayValue(1,CoefX_abc(1))
      Call SetStaticArrayValue(2,CoefX_abc(2))
      Call SetStaticArrayValue(3,CoefX_abc(3))
      
!If Needed, Set the Initial Values of the Dynamic Storage Variables (#,Value)
!Sample Code: Call SetDynamicArrayValueThisIteration(1,20.d0)

!If Needed, Set the Initial Values of the Discrete Controllers (#,Value)
!Sample Code for Controller 1 Set to Off: Call SetDesiredDiscreteControlState(1,0) 

		Return

      EndIf
      
!-----------------------------------------------------------------------------------------------------------------------
      CoefX_abc(1)= getStaticArrayValue(1)
      CoefX_abc(2)= getStaticArrayValue(2)      
      CoefX_abc(3)= getStaticArrayValue(3)      
!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
      area                = getParameterValue(1)
      ht                  = getParameterValue(2)
      Diam                = getParameterValue(3)/1000.0
      pitch               = getParameterValue(4)/1000.0
      emisc               = getParameterValue(5)
      absor               = getParameterValue(6)
      depth               = getParameterValue(7)/1000.
      emisw               = getParameterValue(8)
      rwall               = getParameterValue(9)
      ua                  = getParameterValue(10)
      Qv_1                = getParameterValue(11)
      deltaP1             = getParameterValue(12)
      Qv_2                = getParameterValue(13)
      deltaP2             = getParameterValue(14)
      Qv_3                = getParameterValue(15)
      deltaP3             = getParameterValue(16)
      Rend_Vent           = getParameterValue(17)/100.0
      Recup               = getParameterValue(18)/100.0
      Mode_calcul_hc      = NINT(getParameterValue(19))
		
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      troom               = GetInputValue(1)
      tamb                = GetInputValue(2)
      tsky                = GetInputValue(3)
      Vent                = GetInputValue(4)
      patm                = GetInputValue(5)*101.325  !Conversion en kPa
      rad                 = GetInputValue(6)
      gain                = GetInputValue(7)
      flow1               = GetInputValue(8)
      minout1             = GetInputValue(9)
      flow2               = GetInputValue(10)
      out2                = GetInputValue(11)
      tbypass             = GetInputValue(12)
      qmax                = GetInputValue(13)
      nitebp              = NINT(GetInputValue(14))
      
      if ( minout1 .gt. flow1  .or.  out2 .gt. flow2 ) then
          Call FoundBadInput(1,'Fatal','Check Air Flow Rates In Deck')
      End if
 
      If(ErrorFound()) Return

      
!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
      por    = 0.907 * ( diam / pitch )**2
      surfa  = ( 1 - por ) * area
      udwall = 1.0 / (rwall+0.13/3.6)  !Prise en compte coef �change 1/hi)
    
      
      qabs = rad * surfa * absor
      tgnd = tamb
      tsur = ( 0.5 * ((tsky+273.15)**4 + (tgnd+273.15)**4) )**0.25- 273.15

* calculate building loss, assuming no reduced wall loss

      qbldg = ua * (troom - tamb) - gain

* calculate ambient air properties (curve-fits from EES)

      aircp   = 1.0062 + 3.6028e-5*tamb- 1.0885e-6*tamb*tamb + 1.3791e-8*tamb**3
      aircond = 0.08659 + 2.6993e-4*tamb
      airvisc = 0.06201 + 1.7428e-4*tamb

      a0 = -5.1936e-5 + 1.2758e-2*patm
      a1 = -5.7037e-7 - 4.6865e-5*patm
      a2 = -5.6746e-9 + 1.5511e-7*patm
      airden = a0 + a1*tamb + a2*tamb*tamb

      mflow1 = flow1 * airden
      mflow2 = flow2 * airden
      mflow = mflow1 + mflow2

      qtrad = (minout1 + out2) * airden * aircp * (troom - tamb)+ qbldg
      if ( qtrad .lt. small ) qtrad = 0.0
      
      
      

*-----------------------------------------------------------------------
* check if summer bypass damper is open => 100% air neuf

      if ( (tamb > tbypass).or.(nitebp == 1)) then                
      ! Calcule dP r�seau
      dPreseau        = coefX_abc(1)+CoefX_abc(2)*flow1+CoefX_abc(3)*flow1*flow1
      fanpw_reseau    = (flow1*dPreseau*3.6)
      ! calculate total pressure drop and fan power
      delp = 0.0
      fanpw = fanpw_reseau/Rend_Vent       
      dt = fanpw*Recup/(Flow1*aircp)     
      
          tplen       =tamb           ! Plenum air temperature
          tout        =tamb           ! Collector outlet air temperature
		tmix        =tamb+dt        ! Mixed air temperature
		tsup        =tamb+dt        ! Supply air temperature
		tcol        =tamb           ! Collector surface air temperature
		qsave       =0.0            ! Energy savings rate
          qvcol       =0.0            ! Convection from collector
		qvwall      =0.0            ! Convection from wall
		qrcol       =0.0            ! Radiation from collector
		qrwall      =0.0            ! Radiation from wall
		qdwall      =0.0            ! Conduction through wall
		qred        =0.0            ! Reduced conduction through wall because of collector
		qabs        =0.0            ! Absorbed energy rate
		qaux        =0.0            ! Auxiliary energy rate
		gammaflow1  =Flow1          ! Outdoor airflow rate through collector/summer bypass damper
		effhx       =0.0            ! Heat exchanger effectiveness of collector
		soleff      =0.0            ! Solar efficiency of collector
		pcol        =0.0            ! Pressure drop across collector plate
		bypass      =1.0            ! Bypass damper position 0=Ferm� ; 1=ouvert
		qtrad       =0.0            ! Heat rate supplied by traditional heating system
		!fanpw       =0.0            ! Additional fan power required
		qvcap       =0.0            ! Qconv-col-sur
         Goto 2000
      endif

*-----------------------------------------------------------------------
* check air flow rates
      if ((minout1 > flow1).or.(out2 > flow2)) then
          Call messages(-1,'CHECK AIR FLOW RATES IN DECK (minout1 > flow1).or.(out2 > flow2)','FATAL',CurrentUnit,CurrentType)
      End If
      
      if ( flow2 > small ) then
         beta = out2 / flow2
      else
         beta = 0.0
      endif

      if ( flow1 > small ) then  !Calcul de gamma
         gmin = minout1 / flow1
      else
         ! Flow1=0.0
          qaux = qtrad

          tplen       =tamb           ! Plenum air temperature
          tout        =tamb           ! Collector outlet air temperature
		tmix        =tamb           ! Mixed air temperature
		tsup        =tamb           ! Supply air temperature
		tcol        =tamb           ! Collector surface air temperature
		qsave       =0.0            ! Energy savings rate
          qvcol       =0.0            ! Convection from collector
		qvwall      =0.0            ! Convection from wall
		qrcol       =0.0            ! Radiation from collector
		qrwall      =0.0            ! Radiation from wall
		qdwall      =0.0            ! Conduction through wall
		qred        =0.0            ! Reduced conduction through wall because of collector
		qabs        =0.0            ! Absorbed energy rate
         !qaux        =0.0            ! Auxiliary energy rate
		gammaflow1  =0.0            ! Outdoor airflow rate through collector/summer bypass damper
		effhx       =0.0            ! Heat exchanger effectiveness of collector
		soleff      =0.0            ! Solar efficiency of collector
		pcol        =0.0            ! Pressure drop across collector plate
		bypass      =0.0            ! Bypass damper position 0=Ferm� ; 1=Ouvert
         !qtrad       =0.0            ! Heat rate supplied by traditional heating system
		fanpw       =0.0            ! Additional fan power required
		qvcap       =0.0            ! Qconv-col-sur

         Goto 2000
      endif

*-----------------------------------------------------------------------

      if ( (qabs < small).and.(nitebp==1)) then 
         gamma = gmin
         tout = tamb
         tmix = gamma*tout + (1.0 - gamma)*troom
         tsup = troom + qbldg / (mflow * aircp)
         qaux = qtrad
         
      ! Calcule dP r�seau
      dPreseau        = coefX_abc(1)+CoefX_abc(2)*flow1+CoefX_abc(3)*flow1*flow1
      fanpw_reseau    = (flow1*dPreseau*3.6)
      ! calculate total pressure drop and fan power
      delp = 0.0
      fanpw = fanpw_reseau/Rend_Vent       
      dt = fanpw*Recup/(Flow1*aircp)
  
          tplen       =tamb           ! 1 Plenum air temperature
          tout        =tmix           ! 2 Collector outlet air temperature
		tmix        =tamb+dt        ! 3 Mixed air temperature
		tsup        =tamb+dt        ! 4 Supply air temperature
		tcol        =tamb           ! 5 Collector surface air temperature
		qsave       =0.0            ! 6 Energy savings rate
          qvcol       =0.0            ! 7 Convection from collector
		qvwall      =0.0            ! 8 Convection from wall
		qrcol       =0.0            ! 9 Radiation from collector
		qrwall      =0.0            ! 10 Radiation from wall
		qdwall      =0.0            ! 11 Conduction through wall
		qred        =0.0            ! 12 Reduced conduction through wall because of collector
		qabs        =0.0            ! 13 Absorbed energy rate
		!qaux        =0.0           ! 14 Auxiliary energy rate
		gammaflow1  =minout1        ! 15 Outdoor airflow rate through collector/summer bypass damper
		effhx       =0.0            ! 16 Heat exchanger effectiveness of collector
		soleff      =0.0            ! 17 Solar efficiency of collector
		pcol        =0.0            ! 18 Pressure drop across collector plate
		bypass      =1.0            ! 19 Bypass damper position 0=open ; 1=closed
		!qtrad       =0.0           ! 20 Heat rate supplied by traditional heating system
		!fanpw       =0.0            ! 21 Additional fan power required
		qvcap       =0.0            ! 22 Qconv-col-sur
         GOTO 2000
      endif
      
      
*-----------------------------------------------------------------------
* find gamma such that qaux1 is minimized.  in the winter, this will usually be when gamma = gmin.  if the air is too hot for this condition, then no auxiliary heat is needed (i.e. qaux1 = 0) and
* gamma > gmin.  if gamma is between gmin and 1.0, then use the bisection method to determine gamma.
*-----------------------------------------------------------------------

* calculate necessary supply air temperature to meet load
      tsup_back = troom + qbldg / (mflow * aircp)

* solve equations for gamma = gmin
       gamma = gmin    
       Call utcsolve( gamma, tout, tcol, tplen, twall,qvcol, qrcol, qvwall, qrwall, qdwall,red, effhx, appvel,
     & area, ht, diam, pitch, emisc, depth, emisw, troom, flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur, 
     & aircp, aircond, airvisc, airden, g, CurrentUnit, CurrentType, Qvcap, Mode_calcul_hc,vent)      
       
       Call S_fanpw(por,red,airden,appvel,a0,a1,a2,tplen,depth,area,ht,coefX_abc,Rend_Vent,flow1,gamma,fanpw)
       dT = fanpw*recup/(mflow1*aircp)
       tsup = tsup_back-dT
       
      Call utcsolve( gamma, tout, tcol, tplen, twall,qvcol, qrcol, qvwall, qrwall, qdwall,red, effhx, appvel,
     & area, ht, diam, pitch, emisc, depth, emisw, troom, flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur, 
     & aircp, aircond, airvisc, airden, g, CurrentUnit, CurrentType, Qvcap, Mode_calcul_hc,vent)

      tmix = gamma*tout + (1.0 - gamma)*troom 

      if ( tmix < tsup ) then 
*        calculate auxiliary heat
         qaux1 = mflow1 * aircp * ( tsup - tmix )
      else
*        solve equations for gamma = 1
         qaux1 = 0.0
         gamma = 1.0
         
      Call utcsolve( gamma, tout, tcol, tplen, twall,qvcol, qrcol, qvwall, qrwall, qdwall,red, effhx, appvel,
     & area, ht, diam, pitch, emisc, depth, emisw, troom, flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur, 
     & aircp, aircond, airvisc, airden, g, CurrentUnit, CurrentType, Qvcap, Mode_calcul_hc,vent)
     
      Call S_fanpw(por,red,airden,appvel,a0,a1,a2,tplen,depth,area,ht,coefX_abc,Rend_Vent,flow1,gamma,fanpw)
       dT = fanpw*recup/(mflow1*aircp)
       tsup = tsup_back-dT
       
      Call utcsolve( gamma, tout, tcol, tplen, twall,qvcol, qrcol, qvwall, qrwall, qdwall,red, effhx, appvel,
     & area, ht, diam, pitch, emisc, depth, emisw, troom, flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur, 
     & aircp, aircond, airvisc, airden, g, CurrentUnit, CurrentType, Qvcap, Mode_calcul_hc,vent)   
     
     
         tmix = tout
         if ( tmix < tsup ) then
*           use bisection method to find gamma
            j = 0
            lowg  = gmin
            hig = 1.0
            gamma = (lowg + hig) / 2.0
            
 100        Continue
            
               j = j + 1
               
       Call utcsolve( gamma, tout, tcol, tplen, twall,qvcol, qrcol, qvwall, qrwall, qdwall,red, effhx, appvel,
     & area, ht, diam, pitch, emisc, depth, emisw, troom, flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur, 
     & aircp, aircond, airvisc, airden, g, CurrentUnit, CurrentType, Qvcap, Mode_calcul_hc,vent)
     
      Call S_fanpw(por,red,airden,appvel,a0,a1,a2,tplen,depth,area,ht,coefX_abc,Rend_Vent,flow1,gamma,fanpw)
       dT = fanpw*recup/(mflow1*aircp)
       tsup = tsup_back-dT 
       
      Call utcsolve( gamma, tout, tcol, tplen, twall,qvcol, qrcol, qvwall, qrwall, qdwall,red, effhx, appvel,
     & area, ht, diam, pitch, emisc, depth, emisw, troom, flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur, 
     & aircp, aircond, airvisc, airden, g, CurrentUnit, CurrentType, Qvcap, Mode_calcul_hc,vent)
     
               tmix = gamma*tout + (1.0 - gamma)*troom
               if ( tmix < tsup ) then
                  hig = gamma
               else
                  lowg = gamma
               endif
               oldg = gamma
               gamma = (lowg + hig) / 2.0
               dif = abs( gamma - oldg )

 109        if ( dif > diflim  .and.  j < jmax ) go to 100
            if ( j >= jmax ) then
              Call messages(-1,'Probl�me de convergence','FATAL',CurrentUnit,CurrentType)
            endif
      endif
      !gamma=1 Tmix>Tsup
      If (nitebp==-1) then  !bypass regulation BP
          Alpha =(Tsup+Tout)/(Tout+Tamb)
          Tmix    = Tsup
          bypass  = Alpha
          Qaux1 = 0.0

      End if
      
      If (nitebp==-2) then !bypass regulation TOR
         gamma = gmin
         tout = tamb
         tmix = gamma*tout + (1.0 - gamma)*troom
         tsup = troom + qbldg / (mflow * aircp)
         qaux = qtrad
         
      ! Calcule dP r�seau
      dPreseau        = coefX_abc(1)+CoefX_abc(2)*flow1+CoefX_abc(3)*flow1*flow1
      fanpw_reseau    = (flow1*dPreseau*3.6)
      ! calculate total pressure drop and fan power
      delp = 0.0
      fanpw = fanpw_reseau/Rend_Vent       
      dt = fanpw*Recup/(Flow1*aircp)
  
          tplen       =tamb           ! 1 Plenum air temperature
          tout        =tmix           ! 2 Collector outlet air temperature
		tmix        =tamb+dt        ! 3 Mixed air temperature
		tsup        =tamb+dt        ! 4 Supply air temperature
		tcol        =tamb           ! 5 Collector surface air temperature
		qsave       =0.0            ! 6 Energy savings rate
          qvcol       =0.0            ! 7 Convection from collector
		qvwall      =0.0            ! 8 Convection from wall
		qrcol       =0.0            ! 9 Radiation from collector
		qrwall      =0.0            ! 10 Radiation from wall
		qdwall      =0.0            ! 11 Conduction through wall
		qred        =0.0            ! 12 Reduced conduction through wall because of collector
		qabs        =0.0            ! 13 Absorbed energy rate
		!qaux        =0.0           ! 14 Auxiliary energy rate
		gammaflow1  =minout1        ! 15 Outdoor airflow rate through collector/summer bypass damper
		effhx       =0.0            ! 16 Heat exchanger effectiveness of collector
		soleff      =0.0            ! 17 Solar efficiency of collector
		pcol        =0.0            ! 18 Pressure drop across collector plate
		bypass      =1.0            ! 19 Bypass damper position 0=open ; 1=closed
		!qtrad       =0.0           ! 20 Heat rate supplied by traditional heating system
		!fanpw       =0.0            ! 21 Additional fan power required
		qvcap       =0.0            ! 22 Qconv-col-sur
         GOTO 2000
      Endif
      
      End if
         

*-----------------------------------------------------------------------
* calculate reduced conduction through wall

      film = 54.0                              ! (kJ/hr-m2-C) film coefficient for air  15.0x3.60=54.0
                                               ! against original wall
      tsolair = tamb + (emisw * rad / film)    ! sol-air temp for
                                               ! absor = emis

      qpot = udwall * area * (troom - tsolair) ! potential conduction
      qred = qpot - qdwall                     ! reduced conduction

* calculate total auxiliary heat required, subtracting reduced wall
* loss since it was assumed to be zero when qbldg was calculated
      qaux2 = mflow2 * aircp * ( tsup - beta*tamb - (1.0-beta)*troom )
      qaux = qaux1 + qaux2 - qred
      if ( qaux .lt. small ) qaux = 0.0

* calculate energy savings
      qu    = qvcol + qvwall            ! useful energy gained by air
      qsave = qtrad - qaux              ! energy saved

* calculate solar efficiency
      if ( rad > small ) then
         soleff = qvcol / ( rad * area )
         if ( soleff > 1.0 ) soleff = 1.0
         if ( soleff < 0.0 ) soleff = 0.0
      else
         soleff = 0.0
      endif

*-----------------------------------------------------------------------
*warning
      warn = -1.0


      if ( appvel < 72.0  ) then
          Call messages(-1,'Approach velocity m/hr < 72 m/hr model overpredicts performance','Notice',CurrentUnit,CurrentType)
      endif

      if ( pcol < 0.025 ) then
          Call messages(-1,'pressure drop <0.025 kPa model overpredicts performance','Notice',CurrentUnit,CurrentType)
      endif

      if ( qaux > qmax ) then
          Call messages(-1,'qaux>qmax auxiliary heater(s) too small to meet load','Fatal',CurrentUnit,CurrentType)
      endif

      if ( tmix > tsup ) then
          Call messages(-1,'tmix>Tsup ','WARNING',CurrentUnit,CurrentType)
      endif

!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
 2000     CONTINUE 
		Call SetOutputValue(1, tplen)       ! Plenum air temperature
		Call SetOutputValue(2, tout)        ! Collector outlet air temperature
		Call SetOutputValue(3, tmix)        ! Mixed air temperature
		Call SetOutputValue(4, tsup_back)        ! Supply air temperature
		Call SetOutputValue(5, tcol)        ! Collector surface air temperature
		Call SetOutputValue(6, qsave)       ! Energy savings rate
		Call SetOutputValue(7, qvcol)       ! Convection from collector
		Call SetOutputValue(8, qvwall)      ! Convection from wall
		Call SetOutputValue(9, qrcol)       ! Radiation from collector
		Call SetOutputValue(10, qrwall)     ! Radiation from wall
		Call SetOutputValue(11, qdwall)     ! Conduction through wall
		Call SetOutputValue(12, qred)       ! Reduced conduction through wall because of collector
		Call SetOutputValue(13, qabs)       ! Absorbed energy rate
		Call SetOutputValue(14, qaux)       ! Auxiliary energy rate
		Call SetOutputValue(15, gammaflow1) ! Outdoor airflow rate through collector/summer bypass damper
		Call SetOutputValue(16, effhx)      ! Heat exchanger effectiveness of collector
		Call SetOutputValue(17, soleff)     ! Solar efficiency of collector
		Call SetOutputValue(18, pcol)       ! Pressure drop across collector plate
		Call SetOutputValue(19, bypass)     ! Bypass damper position 0=open ; 1=closed
		Call SetOutputValue(20, qtrad)      ! Heat rate supplied by traditional heating system
		Call SetOutputValue(21, fanpw)      ! Additional fan power required
		Call SetOutputValue(22, qvcap)      ! Qconv-col-sur

!----------------------------------------------------------------------------------------------------------------------- 
      Return
      End


     


!----------------------------------------------------------------------------------------------------------------------
!************************ Subroutine utcsolve ***************************************************************************
!-----------------------------------------------------------------------------------------------------------------------      
       

      subroutine utcsolve( gamma, tout, tcol, tplen, twall,qvcol, qrcol, qvwall, qrwall, qdwall,red, effhx, appvel,
     & area, ht, diam, pitch, emisc, depth, emisw, troom, flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur, 
     & aircp, aircond, airvisc, airden, g, CurrentUnit, CurrentType, Qvcap, Mode_calcul_hc,vent)

*-----------------------------------------------------------------------
* This subroutine solves the energy balances on the collector, air, and
* outside wall surface.  The temperatures and heat flows are output.
*
* The temperatures are:
*
*   tout  = air at the outlet from the collector
*   tcol  = collector surface
*   tplen = air in the plenum
*   twall = outside wall surface
*   troom = air in the room
*   tamb  = ambient air
*   tsur  = surroundings (sky & ground) for radiation calculation
*
* All temperatures in this subroutine are converted from Celsius to
* Kelvin at the beginning, and then they are converted back to Celsius
* at the end for the main UTC subroutine.
* The heat flows are labelled 'qXsource', where X = (r, v, d) for
* radiation, convection, conduction.  The source of the heat flow is
* defined for the usual direction of heat flow for winter operation,
* from the inside of the building to the outside.  So, qvwall is the
* convection from the wall to the plenum (not wall to the room).
* Similarly, qrcol is the radiation from the collector to the
* surroundings (not collector to wall).  The only exceptions are qabs,
* the absorbed solar energy, and qdwall, the conduction through the
* wall.  The heat transfer coefficients are labelled the same way.
*
* Written by David Summers, Solar Energy Lab, U. Wisconsin - Madison
* for M.S. thesis, December 1995.
*-----------------------------------------------------------------------

      implicit none

* TRNSYS variables

      integer CurrentUnit,CurrentType

* utcsolve variables

*     arguments
      DOUBLE PRECISION  gamma, tout, tcol, tplen, twall, qvcol, qrcol, qvwall
      DOUBLE PRECISION  qrwall, qdwall, red, effhx, appvel

*     UTC common block variables
      DOUBLE PRECISION area, ht, diam, pitch, emisc, depth, emisw, troom
      DOUBLE PRECISION flow1, mflow1, por, surfa, udwall, tamb, qabs, tsur
      DOUBLE PRECISION aircp, aircond, airvisc, airden, month, hour,g
      integer*4 unit, type

*     subroutine internal variables
      DOUBLE PRECISION pi, sb, diflim, small
      DOUBLE PRECISION holevel, plenvel, nud, hvcol, pr, reht, nuht, hvwall
      double precision a, b
      DOUBLE PRECISION x, res, dif
      DOUBLE PRECISION Qvcap, Vent
      Integer Mode_calcul_hc
      DOUBLE PRECISION h_conv, Coef_hc
      integer i, j, k, kmax, neq, one, ipiv, ifail
      

      DOUBLE PRECISION Inv_a
      DOUBLE PRECISION vs
      parameter ( neq = 9 )  !8+1
      dimension a(neq,neq), b(neq), x(neq), ipiv(neq), res(neq), Inv_a(neq,neq)
      


*-----------------------------------------------------------------------

      pi     = 3.14159265359
      sb     = 2.0412e-7        ! Stefan-Boltzmann, kJ/hr-m2-K4
      diflim = 1.0e-4
      small  = 1.0e-4
      kmax   = 100
      one    = 1
      ifail  = 0

* check for no flow through collector

      if ( gamma < small ) then
         tout   = tamb
         tcol   = tamb
         tplen  = tamb
         twall  = tamb
         qvcol  = 0.0
         qrcol  = 0.0
         qvwall = 0.0
         qrwall = 0.0
         qdwall = udwall * area * ( troom - tplen )
         red    = 0.0
         effhx  = 0.0
         appvel = 0.0
         Qvcap  =0.0
         return
      endif

* convert celsius to kelvin

      troom = troom   + 273.15
      tamb  = tamb    + 273.15
      tsur  = tsur    + 273.15

* calculate approach, hole, and plenum velocities

      appvel  = gamma * flow1 / area
      vs      =appvel/3600.0
      holevel = gamma * flow1 / ( area * por )
      plenvel = 0.5 * appvel * ht / depth

* calculate heat exchanger effectiveness for collector

      red = airden * holevel * diam / airvisc
      nud = 2.75 * ( pitch/diam )**( -1.2 ) * ( red )**( 0.43 )
      hvcol = nud * aircond / diam
      effhx = 1.0 - exp( - hvcol * surfa / (gamma * mflow1 * aircp) )

* calculate heat transfer coefficient for wall to air convection

      pr = 0.71
      reht = airden * plenvel * ht / airvisc
      if ( reht .gt. 500000 ) then
*        turbulent
         nuht = (0.037*reht**0.8 - 871.0) * pr**(1.0/3.0)
      else
*        laminar
         nuht = 0.664 * reht**0.5 * pr**(1.0/3.0)
      endif
      hvwall = nuht * aircond / ht

*-----------------------------------------------------------------------
* solve simultaneous equations for unknown temperatures and heat flows
* [a][x] = [b]
*-----------------------------------------------------------------------

* initial guesses for collector and wall temperatures

      tcol  = tamb
      twall = tamb

* calculate [a] matrix and [b] array

      do i = 1,neq
         do j = 1,neq    
            a(i,j) = 0.0
         enddo
         b(i) = 0.0
      enddo
      
       h_conv = Coef_hc(Mode_calcul_hc, Vent, Vs,Flow1,Area)
      
      a(1,1) = - effhx
      a(1,2) = 1.0
      a(2,1) = - emisc * sb * surfa* (tcol**2 + tsur**2) * (tcol + tsur)
      a(2,3) = 1.0
      a(3,1) = sb * area * (tcol**2 + twall**2) * (tcol + twall)/ ( 1.0/emisw + 1.0/emisc - 1.0 )
      a(3,4) = 1.0
      a(3,5) = - sb * area * (tcol**2 + twall**2) * (tcol + twall)/ ( 1.0/emisw + 1.0/emisc - 1.0 )
      a(4,2) = hvwall * area
      a(4,5) = - hvwall * area
      a(4,6) = 1.0
      a(5,2) = - gamma * mflow1 * aircp
      a(5,7) = 1.0
      a(6,3) = 1.0
      a(6,4) = - 1.0
      a(6,7) = 1.0
      a(6,9) = -1.0
      a(7,4) = - 1.0
      a(7,6) = - 1.0
      a(7,8) = 1.0
      a(8,5) = udwall * area
      a(8,8) = 1.0
      a(9,1) = h_conv*area
      a(9,9) = 1

      b(1) = tamb * ( 1.0 - effhx )
      b(2) = - emisc * sb * surfa* (tcol**2 + tsur**2) * (tcol + tsur) * tsur
      b(5) = - gamma * mflow1 * aircp * tamb
      b(6) = qabs
      b(8) = udwall * area * troom
      b(9) = h_conv*area*Tamb

* start iterations

      k = 0
 110  continue
      k = k + 1

      Inv_a=a; ifail=0
      Call InvertMatrix(neq,neq,Inv_a,ifail) 
      if ( ifail .ne. 0  ) then
                Call messages(-1,'MATRIX SOLVER ERROR','FATAL',CurrentUnit,CurrentType)
      endif
      x=MATMUL(Inv_a,b)


* calculate [a] matrix and [b] array

      tcol  = x(1)
      twall = x(5)

      do i = 1,neq
         do j = 1,neq
            a(i,j) = 0.0
         enddo
         b(i) = 0.0
      enddo

      a(1,1) = - effhx
      a(1,2) = 1.0
      a(2,1) = - emisc * sb * surfa* (tcol**2 + tsur**2) * (tcol + tsur)
      a(2,3) = 1.0
      a(3,1) = sb * area * (tcol**2 + twall**2) * (tcol + twall)/ ( 1.0/emisw + 1.0/emisc - 1.0 )
      a(3,4) = 1.0
      a(3,5) = - sb * area * (tcol**2 + twall**2) * (tcol + twall)/ ( 1.0/emisw + 1.0/emisc - 1.0 )
      a(4,2) = hvwall * area
      a(4,5) = - hvwall * area
      a(4,6) = 1.0
      a(5,2) = - gamma * mflow1 * aircp
      a(5,7) = 1.0
      a(6,3) = 1.0
      a(6,4) = - 1.0
      a(6,7) = 1.0
      a(6,9) = -1.0
      a(7,4) = - 1.0
      a(7,6) = - 1.0
      a(7,8) = 1.0
      a(8,5) = udwall * area
      a(8,8) = 1.0
      a(9,1) = h_conv*area
      a(9,9) = 1

      b(1) = tamb * ( 1.0 - effhx )
      b(2) = - emisc * sb * surfa* (tcol**2 + tsur**2) * (tcol + tsur) * tsur
      b(5) = - gamma * mflow1 * aircp * tamb
      b(6) = qabs
      b(8) = udwall * area * troom
      b(9) = h_conv*area*Tamb

* calculate residuals

      do i = 1,neq
         res(i) = 0.0
         do j = 1,neq
            res(i) = res(i) + a(i,j) * x(j)
         enddo
         res(i) = res(i) - b(i)
      enddo
            
* check for convergence

      dif = 0.0
      do i = 1,neq
         dif = sqrt( dif**2 + res(i)**2 )
      enddo
      
 119  if ( dif>diflim  .and.  k<kmax ) go to 110
      if ( k > kmax ) then
                Call messages(-1,'NO CONVERGENCE IN K LOOP','FATAL',CurrentUnit,CurrentType)
	RETURN
      endif

*-----------------------------------------------------------------------
* solution has been found

      tcol   = x(1)
      tplen  = x(2)
      qrcol  = x(3)
      qrwall = x(4)
      twall  = x(5)
      qvwall = x(6)
      qvcol  = x(7)
      qdwall = x(8)
      !qvcap  = x(9)

* calculate tout from energy balance

      tout = tplen + qvwall / ( gamma * mflow1 * aircp )

* convert kelvin to celsius

      tout  = tout    - 273.15
      tcol  = tcol    - 273.15
      tplen = tplen   - 273.15
      twall = twall   - 273.15
      troom = troom   - 273.15
      tamb  = tamb    - 273.15
      tsur  = tsur    - 273.15

*-----------------------------------------------------------------------

      return
      end
      
      
      
      !----------------------------------------------------------------------------------------------------------------------------------     
      
      Function Coef_hc(Mode_calcul_hc, Vent, Vs,Flow1,Area) !en W/m��C
       implicit none
       Double precision Coef_hc 
       Double precision Vent, Vs
       Integer Mode_calcul_hc
       Double precision h_utc, h_glaced
       Double precision h1, h2
       Double precision Hc1, Hc2
       Double precision Flow1,Area
      
       If (Mode_calcul_hc==0) Coef_hc=0.0         !Cas Non prise en compte de la convection
       
       If (Mode_calcul_hc==1) Then                !Cas calcul selon SWift99
           If (vs>0.01) then
               h_utc      = 0.02*Vent/Vs
               h_glaced   = 2.8+3.0*Vent
               Coef_hc = min(h_utc,h_glaced)
           Else
               Coef_hc = 2.8+3.0*Vent
          End if
       End if
       
        If (Mode_calcul_hc==2) Then                !Cas calcul selon STRL 1994
           Coef_hc=6.0+4.0*Vent-76.0*Vs
        End if   
       
        If (Mode_calcul_hc==3) then                    !50%calcul selon SWift99 et 50%calcul selon STRL 1994
           If (vs>0.01) then
               h_utc      = 0.02*Vent/Vs
               h_glaced   = 2.8+3.0*Vent
               h1 = min(h_utc,h_glaced)
           Else
               h1 = 2.8+3.0*Vent
           End if 
               h2 = 6.0+4.0*Vent-76.0*Vs
        Coef_hc = 0.5*(h1+h2)
        End If
        
        If (Mode_calcul_hc==4) Then                !Cas proportionnel SWift99 et STRL 1994 entre d�bit surfacique = 30 et d�bit surfacique = 60  
            If (vs>0.01) then
               h_utc      = 0.02*Vent/Vs
               h_glaced   = 2.8+3.0*Vent
               hc1 = min(h_utc,h_glaced)
                                                  hc2 = 6.0+4.0*Vent-76.0*Vs
                                                  Coef_hc = hc2 + ((flow1/area)-30)/30*(((hc2+hc1)/2)-hc2)
          Else
               hc1 = 2.8+3.0*Vent
                                                  hc2 = 6.0+4.0*Vent-76.0*Vs
                                                  Coef_hc = hc2 + ((flow1/area)-30)/30*(((hc2+hc1)/2)-hc2)
          End if
          End If

        
       Coef_hc = coef_hc*3.6  !convertion W/m��C => kJ/h m��C
      End function
      
      
      
      !----------------------------------------------------------------------------------------------------------------------------------     
      
       subroutine S_fanpw(por,red,airden,appvel,a0,a1,a2,tplen,depth,area,ht,coefX_abc,Rend_Vent,flow1,gamma,fanpw)
          implicit none
          DOUBLE PRECISION por, red, airden, appvel, zeta,pcol
          DOUBLE PRECISION a0 ,a1 ,a2, tplen, plenden
          DOUBLE PRECISION depth,  area, ht, f, dh, plenvel, pfric
          DOUBLE PRECISION pbuoy, pacc, dPreseau , fanpw_reseau
          DOUBLE PRECISION CoefX_abc(3), Rend_Vent, flow1, gamma, fanpw
          DOUBLE PRECISION g, delp, V, V1, Longueur
          
          g= 9.8    ! acceleration of gravity
          
          V           = appvel/3600.0
          Longueur    = area/ht
          V1          = flow1/(Longueur*depth)/3600.0
      
* calculate pressure drop across collector

          zeta  = 6.82 * ( (1-por)/por )**2 * ( red/3600.0 )**( -0.236 )
          pcol  = 0.5 * airden * V * V * zeta  !en Pa   
      
* calculate air density in plenum

          plenden = a0 + a1*tplen + a2*tplen*tplen  
* calculate friction pressure drop through plenum

          f = 0.05    ! estimate
          dh = 4.0 * ( depth * area/ht ) / ( 2.0 * ( area/ht + depth ) )
          plenvel = 0.5 * appvel * ht / depth
          pfric = f * (ht / dh)*airden * (V1/2.0)**2 / 2.0

      

* calculate buoyancy pressure term

          pbuoy = (airden - plenden) * g * ht

* calculate acceleration pressure drop

          pacc = plenden * V1**2 / 2.0
      
! Calcule dP r�seau
      
          dPreseau        = coefX_abc(1)+CoefX_abc(2)*flow1+CoefX_abc(3)*flow1*flow1
          fanpw_reseau    = (flow1/3600.*dPreseau)
      
* calculate total pressure drop and fan power

          delp = pcol + pfric - pbuoy + pacc
          fanpw = gamma * flow1/3600. * delp +fanpw_reseau
          fanpw = fanpw*3.6/Rend_Vent    
      End subroutine