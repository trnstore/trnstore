!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type4405

! Object: Rendement transformateur �lectrique HTA
! Simulation Studio Model: Type4405
! 

! Author: DF
! Editor: TEP2E
! Date:	 April 12, 2012
! last modified: April 12, 2012
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Puissance nominale du transo en kVA	kW [100;+Inf]
!			Pertes � vide	% (base 100) [-1;+Inf]
!			Pertes totale en 100% de charge	% (base 100) [-1;+Inf]
!			Nbre de transformateur	- [1;+Inf]

! *** 
! *** Model Inputs 
! *** 
!			Puissance sortie	kJ/hr [0;+Inf]
!			Cos phi	- [0;1]

! *** 
! *** Model Outputs 
! *** 
!			Puissance entr�e	kJ/hr [0;+Inf]
!			Pertes totales	kJ/hr [0;+Inf]
!			Pertes Fer	kJ/hr [0;+Inf]
!			Pertes joules	kJ/hr [0;+Inf]
!			Rendement	% (base 100) [0;100]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Simulation Studio)
!************************************************************************



      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type4405

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      DOUBLE PRECISION PnkVA
      DOUBLE PRECISION P0
      DOUBLE PRECISION Pcc
      DOUBLE PRECISION NbreT

!    INPUTS
      DOUBLE PRECISION Puis_sortie
      DOUBLE PRECISION Cos_phi

!    Calcul
      DOUBLE PRECISION Puis_entree
      DOUBLE PRECISION PertesF !W
      DOUBLE PRECISION PertesJ !W
      DOUBLE PRECISION PertesTotal
      DOUBLE PRECISION PertesJ100
      DOUBLE PRECISION Rendement
      DOUBLE PRECISION Var1
      DOUBLE PRECISION k

!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then

		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(4)           !The number of parameters that the the model wants
		Call SetNumberofInputs(2)                   !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)         !The number of derivatives that the the model wants
		Call SetNumberofOutputs(5)                 !The number of outputs that the the model produces
		Call SetIterationMode(1)                             !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)                   !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)               !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      PnkVA             = getParameterValue(1)*1000.0    !En Watt
      P0                = getParameterValue(2)/100.0
      Pcc               = getParameterValue(3)/100.0
      NbreT             = getParameterValue(4)
      
      Puis_sortie   = GetInputValue(1)/3.6               !En Watt          
      Cos_phi       = max(GetInputValue(2),0.01)
      
      Goto 1000
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
      PnkVA             = getParameterValue(1)*1000.0    !En Watt
      P0                = getParameterValue(2)/100.0
      Pcc               = getParameterValue(3)/100.0
      NbreT             = getParameterValue(4)
      EndIf
!-----------------------------------------------------------------------------------------------------------------------
!Read the Inputs
      Puis_sortie   = GetInputValue(1)/3.6          !En Watt
      Cos_phi       = max(GetInputValue(2),0.01)
		
      If(ErrorFound()) Return

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
1000 CONTINUE
     
       ! Calcul en W
        PertesF = P0*PnKVA*NbreT
        IF (P0<0.0) Then
            PertesF = (155+0.638*PnKVA/1000.0)*NbreT
        End If
        
        PertesJ100 =  Pcc*PnKVA
        IF (Pcc<0.0) Then
            PertesJ100  = (1420.0+8.126*PnKVA/1000.0)*NbreT
        End If
        K = (Puis_sortie/Cos_phi)/(PnkVA*NbreT)
        PertesJ = k*k*PertesJ100
       
        
        PertesTotal = PertesF+PertesJ
        
        Puis_entree = Puis_sortie+PertesTotal
        Rendement   = Puis_sortie/Puis_entree

!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
		Call SetOutputValue(1, Puis_entree*3.6)     ! Puissance entr�e
		Call SetOutputValue(2, PertesTotal*3.6)     ! Pertes totales
		Call SetOutputValue(3, PertesF*3.6)         ! Pertes Fer
		Call SetOutputValue(4, PertesJ*3.6)         ! Pertes joules
		Call SetOutputValue(5, Rendement*100.0)     ! Rendement

 
     Return
      End
!-----------------------------------------------------------------------------------------------------------------------

