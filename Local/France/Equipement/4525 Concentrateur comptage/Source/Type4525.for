!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type4525

! Object: Compteur energie
! Simulation Studio Model: Type4525
! 

! Author: DF
! Editor: TEP2E
! Date:	 August 03, 2012
! last modified: August 03, 2012
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Nbre INPUT	- [1;10]
!			T_Reset	- [0;2]
!			Unit�s Sortie	- [0;3]

! *** 
! *** Model Inputs 
! *** 
!			Entr�e N�	kJ/hr [0;+Inf]

! *** 
! *** Model Outputs 
! *** 
!           Puissance 
!			Pmax    Entr�e N�	- [-Inf;+Inf]     
!			Energie Entr�e N�	- [-Inf;+Inf]



      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type4525

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      INTEGER Nbre_INPUT
      INTEGER U_conversion

!    INPUTS
      DOUBLE PRECISION Entre(1:20)

!     Calculs
      INTEGER i,j
      DOUBLE PRECISION T_Energie, Energie(1:10), Energie_back(1:10)
      DOUBLE PRECISION Puissance(1:10)
      DOUBLE PRECISION T_Pmax, Pmax(1:10)
      DOUBLE PRECISION T_Puissance
      DOUBLE PRECISION T_Pmaxback, Pmax_back(1:10)
      DOUBLE PRECISION S_Pmax

      DOUBLE PRECISION C_Energie(0:3), C_Puissance(0:3)
 
      DATA C_Energie  /1.0,3.6,3600.0,3600000.0/
      DATA C_Puissance/1.0,3.6,3600.0,3600000.0/

!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time          =getSimulationTime()
      Timestep      =getSimulationTimeStep()
      CurrentUnit   =getCurrentUnit()
      CurrentType   =getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then

        Nbre_Input=NINT(getParameterValue(1))

        T_Pmax =getStaticArrayValue(2)             !back 
        Do i=1, Nbre_INPUT
            j=2+4*(i-1)
            Pmax(i)     =getStaticArrayValue(j+3)  !back
            Energie(i)  =getStaticArrayValue(j+4)  !back
        End do
        
      
        Call  setStaticArrayValue(1  ,T_Pmax)
        Do i=1, Nbre_INPUT
             j=2+4*(i-1)
             Call  setStaticArrayValue(j+1  ,Pmax(i))
             Call  setStaticArrayValue(j+2,Energie(i))
        End do
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then

        Nbre_Input=NINT(getParameterValue(1))
        i=3+3*Nbre_Input !Nbre OUTPUT
        j=3+4*Nbre_INPUT !Nbre STORED

		Call SetNumberofParameters(3)          !The number of parameters that the the model wants
		Call SetNumberofInputs(Nbre_Input)     !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)         !The number of derivatives that the the model wants
		Call SetNumberofOutputs(i)             !The number of outputs that the the model produces
		Call SetIterationMode(1)               !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(j,0)     !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)    !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then

      Nbre_INPUT    = NINT(getParameterValue(1))
      U_Conversion  = NINT(getParameterValue(3))

      Do i=1, Nbre_INPUT
        Entre(i) = GetInputValue(i)
      End do
	
   !Check the Parameters for Problems (#,ErrorType,Text)
   !Sample Code: If( PAR1 <= 0.) Call FoundBadParameter(1,'Fatal','The first parameter provided to this model is not acceptable.')

   !Set the Initial Values of the Outputs (#,Value)
		Do i=1,3+3*Nbre_INPUT
            Call SetOutputValue(1, 0.d0) ! Total Energie
        End do

   !If Needed, Set the Initial Values of the Static Storage Variables (#,Value)
      Do i=1,3+4*Nbre_INPUT
        Call SetStaticArrayValue(i,0.d0)
      End do
   !If Needed, Set the Initial Values of the Discrete Controllers (#,Value)
   !Sample Code for Controller 1 Set to Off: Call SetDesiredDiscreteControlState(1,0) 

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
            Nbre_INPUT    = NINT(getParameterValue(1))
            U_Conversion  = NINT(getParameterValue(3))
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Do i=1, Nbre_INPUT
        Entre(i) = GetInputValue(i)
      End do
 
      If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
       T_Pmaxback    =getStaticArrayValue(1) 
       Do i=1,Nbre_Input
            j=2+4*(i-1)
            Pmax_back(i)     =getStaticArrayValue(j+1)
            Energie_back(i)  =getStaticArrayValue(j+2)
        End do
        i=3+4*Nbre_INPUT
    

	!-----------------------------------------------------------------------------------------------------------------------
	!If Needed, Get the Initial Values of the Dynamic Variables from the Global Storage Array (#)
	!Sample Code: T_INITIAL_1=getDynamicArrayValueLastTimestep(1)
	!-----------------------------------------------------------------------------------------------------------------------
      
       Do i=1, Nbre_INPUT
        Pmax(i)     = Pmax_back(i);IF (Entre(i)>Pmax_back(i)) Pmax(i)=Entre(i)
        Energie(i)  = Energie_back(i)+Entre(i)*TimeStep
       End do
        
      T_Energie =0.0
      T_puissance=0.0    
      
      Do i=1, Nbre_INPUT
        Puissance(i)=Entre(i)
        T_Energie=T_Energie+Energie(i)
        T_Puissance=T_Puissance+Entre(i)
      End do
        T_Pmax= T_Pmaxback; If (T_Puissance>T_Pmaxback) T_Pmax=T_Puissance

!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
      Call SetOutputValue(1, T_Puissance/C_Puissance(U_Conversion))         
      Call SetOutputValue(2, T_Pmax/C_Puissance(U_Conversion))   
      Call SetOutputValue(3, T_Energie/C_Energie(U_Conversion))      
             
      Do i=1, Nbre_INPUT
        j=3+3*(i-1)
            Call SetOutputValue(1+j, Puissance(i)/C_Puissance(U_Conversion)) 
		    Call SetOutputValue(2+j, PMax(i)/C_Puissance(U_Conversion)) 
            Call SetOutputValue(3+j, Energie(i)/C_Energie(U_Conversion))    
        End do

!-----------------------------------------------------------------------------------------------------------------------
!If Needed, Store the Final value of the Dynamic Variables in the Global Storage Array (#,Value)
!Sample Code:  Call SetValueThisIteration(1,T_FINAL_1)
       Call setStaticArrayValue(2,T_Pmax)
       Do i=1,Nbre_INPUT
        j=2+4*(i-1)
        Call setStaticArrayValue(j+3,Pmax(i))
        Call setStaticArrayValue(j+4,Energie(i))
       End do
       i=3+4*Nbre_INPUT

      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

