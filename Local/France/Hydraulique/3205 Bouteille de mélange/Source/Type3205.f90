!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3205

! Object: Bouteille de m�lange
! Simulation Studio Model: Type3205
! 

! Author: Didier FOUQUET
! Editor: 
! Date:	 April 06, 2007
! last modified: April 06, 2019
! 
! 
! *** 
! *** Model Parameters 
! *** 

! *** 
! *** Model Inputs 
! *** 
!			Temperature entr�e primaire	C [-Inf;+Inf]
!			D�bit Primaire	kg/hr [-Inf;+Inf]
!			Temp�rature entr�e secondaire	C [-Inf;+Inf]
!			D�bit Secondaire	kg/hr [-Inf;+Inf]

! *** 
! *** Model Outputs 
! *** 
!			Temperature entr�e primaire	C [-Inf;+Inf]
!			Temperature sortie primaire	C [-Inf;+Inf]
!			D�bit primaire	kg/hr [-Inf;+Inf]
!			Temperature sortie secondaire	C [-Inf;+Inf]
!			Temperature entre secondaire	C [-Inf;+Inf]
!			D�bit secondaire	kg/hr [-Inf;+Inf]
!			Melange	- [-Inf;+Inf]

! *** 
! *** Model Derivatives 
! *** 

      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type3205

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS

!    INPUTS
      DOUBLE PRECISION Temp_PE
      DOUBLE PRECISION Debit_P
      DOUBLE PRECISION Temp_SE
      DOUBLE PRECISION Debit_S

!     Calcul
      DOUBLE PRECISION Temp_PS
      DOUBLE PRECISION Temp_SS
	  DOUBLE PRECISION Melange
	  DOUBLE PRECISION Debit
	  DOUBLE PRECISION, PARAMETER :: Epsilon=1E-30

!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then

		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(0)           !The number of parameters that the the model wants
		Call SetNumberofInputs(4)                   !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)         !The number of derivatives that the the model wants
		Call SetNumberofOutputs(7)                 !The number of outputs that the the model produces
		Call SetIterationMode(1)                             !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)                   !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)               !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then


      Temp_PE = GetInputValue(1)
      Debit_P = GetInputValue(2)
      Temp_SE = GetInputValue(3)
      Debit_S = GetInputValue(4)

	
    Goto 20

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File

		
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Temp_PE = GetInputValue(1)
      Debit_P = GetInputValue(2)
      Temp_SE = GetInputValue(3)
      Debit_S = GetInputValue(4)
		

	!Check the Inputs for Problems (#,ErrorType,Text)
	!Sample Code: If( IN1 <= 0.) Call FoundBadInput(1,'Fatal','The first input provided to this model is not acceptable.')
 
20 If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
    IF ((ABS(Debit_P)<Epsilon).and.(ABS(Debit_S)<Epsilon)) then 
 	    Melange=1.0
	    Temp_PS=Temp_PE
          Temp_SS=Temp_SE
          GOTO 1000
	End if

      If (ABS(Debit_P)<Epsilon) then
	    Temp_PS=Temp_SE
          Temp_SS=Temp_SE
          GOTO 1000
      End if

      If (ABS(Debit_S)<Epsilon) then
	    Temp_PS=Temp_PE
          Temp_SS=Temp_PE   
          GOTO 1000
      End if
      
      IF (Debit_P>debit_S) then
	    Melange=0.0
          Temp_SS=Temp_PE
          Debit=(Debit_P-Debit_S)
	    Temp_PS=(Temp_PE*Debit+Temp_SE*Debit_S)/Debit_P
          GOTO 1000
	    Else
	    Melange=1.0
	    Temp_PS=Temp_SE
          Debit=(Debit_S-Debit_P)
          Temp_SS=(Temp_SE*debit+Temp_PE*debit_P)/Debit_S
          GOTO 1000
	End if

!Set the Outputs from this Model (#,Value)
1000 CONTINUE        
		Call SetOutputValue(1, Temp_PE) ! Temperature entr�e primaire
		Call SetOutputValue(2, Temp_PS) ! Temperature sortie primaire
		Call SetOutputValue(3, Debit_P) ! D�bit primaire
		Call SetOutputValue(4, Temp_SS) ! Temperature sortie secondaire
		Call SetOutputValue(5, Temp_SE) ! Temperature entre secondaire
		Call SetOutputValue(6, Debit_S) ! D�bit secondaire
		Call SetOutputValue(7, Melange) ! Melange

      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

