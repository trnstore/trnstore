!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3220

! Object: Pompe - Circulateur calculs d�taill�s  Eau pure Pompe double
! Simulation Studio Model: Type3220
! 

! Author: Didier FOUQUET
! Editor: TEP2E
! Date:	 Avril 2007
! last modified: Juillet 2012
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Mode de r�gulation	- [0;2]
!			Qv_min D�bit min	m^3/hr [0.0;+Inf]
!			Qv_nom D�bit nominal	m^3/hr [0.0;+Inf]
!			Qv_max D�bit max	m^3/hr [0.0;+Inf]
!			Hmt � Qv_min	mCE_H20 [0;+Inf]
!			Hmt � Qv_nom	mCE_H20 [0;+Inf]
!			Hmt � Qv_max	mCE_H20 [0;+Inf]
!			Rend Hydraulique � Qv_min	% (base 100) [0;100]
!			Rend Hydraulique � Qv_nom	% (base 100) [0;100]
!			Rend Hydraulique � Qv_max	% (base 100) [0;100]
!			Puissance max moteur %de Pn	- [-1;100]
!			Efficacit� du moteur �lectrique 0 � 4	- [0;4]
!			Nombre de paire de P�les	- [1;3]
!			R�cup�ration pertes moteur	- [0.;1.0]
!			R�cup�ration �chauffement hydraulique	- [-1;1.0]
!			Pond�ration des pertes	- [0.5;2.0]
!			R�duction max vitesse	- [0;1.0]
!			Alpha 	- [0.70;1.0]
!			=====Caract�ristiques du r�seau	- [0;0]
!			D�bit Qv  reference  ou	m^3/hr [0;+Inf]
!			Hmt Statique+r�siduelle    reference	mCE_H20 [0;+Inf]
!			Hmt dynamique reference 	mCE_H20 [-1;+Inf]

! *** 
! *** Model Inputs 
! *** 
!			On/Off	- [0;1]
!			Temp�rature     Entr�e Fluide	C [-Inf;+Inf]
!			% Consigne D�bit Entr�e Fluide	% (base 1) [0.0;1.5]
!			1P ou 2P	- [1;2]

! *** 
! *** Model Outputs 
! *** 
!			Temp�rature   sortie fluide	C [-Inf;+Inf]
!			D�bit    Qm     sortie fluide	kg/hr [0.0;+Inf]
!			D�bit    Qv       sortie fluide	m^3/hr [0.0;+Inf]
!			Hmt 	mCE_H20 [0.0;+Inf]
!			Puissance �lectrique	kJ/hr [-Inf;+Inf]
!			Chaleur transfer�e au fluide	kJ/hr [-Inf;+Inf]
!			Chaleur perdue	kJ/hr [-Inf;+Inf]
!			Rendement hydraulique	% (base 100) [0;100]
!			Rendement moteur	% (base 100) [0;100]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Simulation Studio)
!************************************************************************

!-----------------------------------------------------------------------------------------------------------------------
! This TRNSYS component skeleton was generated from the TRNSYS studio based on the user-supplied parameters, inputs, 
! outputs, and derivatives.  The user should check the component formulation carefully and add the content to transform
! the parameters, inputs and derivatives into outputs.  Remember, outputs should be the average value over the timestep
! and not the value at the end of the timestep; although in many models these are exactly the same values.  Refer to 
! existing types for examples of using advanced features inside the model (Formats, Labels etc.)
!-----------------------------------------------------------------------------------------------------------------------


      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type3220

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      DOUBLE PRECISION Mode_de_r_gulation
      DOUBLE PRECISION Qv_min_D_bit_min
      DOUBLE PRECISION Qv_nom_D_bit_nominal
      DOUBLE PRECISION Qv_max_D_bit_max
      DOUBLE PRECISION Hmt___Qv_min
      DOUBLE PRECISION Hmt___Qv_nom
      DOUBLE PRECISION Hmt___Qv_max
      DOUBLE PRECISION Rend_Hydraulique___Qv_min
      DOUBLE PRECISION Rend_Hydraulique___Qv_nom
      DOUBLE PRECISION Rend_Hydraulique___Qv_max
      DOUBLE PRECISION Puissance_max_moteur__de_Pn
      DOUBLE PRECISION Efficacit__du_moteur__lectrique_0___4
      DOUBLE PRECISION Nombre_de_paire_de_P_les
      DOUBLE PRECISION R_cup_ration_pertes_moteur
      DOUBLE PRECISION R_cup_ration__chauffement_hydraulique
      DOUBLE PRECISION Pond_ration_des_pertes
      DOUBLE PRECISION R_duction_max_vitesse
      DOUBLE PRECISION Alpha_
      DOUBLE PRECISION _____Caract_ristiques_du_r_seau
      DOUBLE PRECISION D_bit_Qv__reference__ou
      DOUBLE PRECISION Hmt_Statique_r_siduelle____reference
      DOUBLE PRECISION Hmt_dynamique_reference_

!    INPUTS
      DOUBLE PRECISION On_Off
      DOUBLE PRECISION Temp_rature_____Entr_e_Fluide
      DOUBLE PRECISION __Consigne_D_bit_Entr_e_Fluide
      DOUBLE PRECISION A1P_ou_2P

!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then

		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(22)           !The number of parameters that the the model wants
		Call SetNumberofInputs(4)                   !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)         !The number of derivatives that the the model wants
		Call SetNumberofOutputs(9)                 !The number of outputs that the the model produces
		Call SetIterationMode(1)                             !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)                   !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)               !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      Mode_de_r_gulation = getParameterValue(1)
      Qv_min_D_bit_min = getParameterValue(2)
      Qv_nom_D_bit_nominal = getParameterValue(3)
      Qv_max_D_bit_max = getParameterValue(4)
      Hmt___Qv_min = getParameterValue(5)
      Hmt___Qv_nom = getParameterValue(6)
      Hmt___Qv_max = getParameterValue(7)
      Rend_Hydraulique___Qv_min = getParameterValue(8)
      Rend_Hydraulique___Qv_nom = getParameterValue(9)
      Rend_Hydraulique___Qv_max = getParameterValue(10)
      Puissance_max_moteur__de_Pn = getParameterValue(11)
      Efficacit__du_moteur__lectrique_0___4 = getParameterValue(12)
      Nombre_de_paire_de_P_les = getParameterValue(13)
      R_cup_ration_pertes_moteur = getParameterValue(14)
      R_cup_ration__chauffement_hydraulique = getParameterValue(15)
      Pond_ration_des_pertes = getParameterValue(16)
      R_duction_max_vitesse = getParameterValue(17)
      Alpha_ = getParameterValue(18)
      _____Caract_ristiques_du_r_seau = getParameterValue(19)
      D_bit_Qv__reference__ou = getParameterValue(20)
      Hmt_Statique_r_siduelle____reference = getParameterValue(21)
      Hmt_dynamique_reference_ = getParameterValue(22)


      On_Off = GetInputValue(1)
      Temp_rature_____Entr_e_Fluide = GetInputValue(2)
      __Consigne_D_bit_Entr_e_Fluide = GetInputValue(3)
      A1P_ou_2P = GetInputValue(4)

	
   !Check the Parameters for Problems (#,ErrorType,Text)
   !Sample Code: If( PAR1 <= 0.) Call FoundBadParameter(1,'Fatal','The first parameter provided to this model is not acceptable.')

   !Set the Initial Values of the Outputs (#,Value)
		Call SetOutputValue(1, 0.d0) ! Temp�rature   sortie fluide
		Call SetOutputValue(2, 0.d0) ! D�bit    Qm     sortie fluide
		Call SetOutputValue(3, 0.d0) ! D�bit    Qv       sortie fluide
		Call SetOutputValue(4, 0.d0) ! Hmt 
		Call SetOutputValue(5, 0.d0) ! Puissance �lectrique
		Call SetOutputValue(6, 0.d0) ! Chaleur transfer�e au fluide
		Call SetOutputValue(7, 0.d0) ! Chaleur perdue
		Call SetOutputValue(8, 0.d0) ! Rendement hydraulique
		Call SetOutputValue(9, 0.d0) ! Rendement moteur


   !If Needed, Set the Initial Values of the Static Storage Variables (#,Value)
   !Sample Code: SetStaticArrayValue(1,0.d0)

   !If Needed, Set the Initial Values of the Dynamic Storage Variables (#,Value)
   !Sample Code: Call SetDynamicArrayValueThisIteration(1,20.d0)

   !If Needed, Set the Initial Values of the Discrete Controllers (#,Value)
   !Sample Code for Controller 1 Set to Off: Call SetDesiredDiscreteControlState(1,0) 

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
      Mode_de_r_gulation = getParameterValue(1)
      Qv_min_D_bit_min = getParameterValue(2)
      Qv_nom_D_bit_nominal = getParameterValue(3)
      Qv_max_D_bit_max = getParameterValue(4)
      Hmt___Qv_min = getParameterValue(5)
      Hmt___Qv_nom = getParameterValue(6)
      Hmt___Qv_max = getParameterValue(7)
      Rend_Hydraulique___Qv_min = getParameterValue(8)
      Rend_Hydraulique___Qv_nom = getParameterValue(9)
      Rend_Hydraulique___Qv_max = getParameterValue(10)
      Puissance_max_moteur__de_Pn = getParameterValue(11)
      Efficacit__du_moteur__lectrique_0___4 = getParameterValue(12)
      Nombre_de_paire_de_P_les = getParameterValue(13)
      R_cup_ration_pertes_moteur = getParameterValue(14)
      R_cup_ration__chauffement_hydraulique = getParameterValue(15)
      Pond_ration_des_pertes = getParameterValue(16)
      R_duction_max_vitesse = getParameterValue(17)
      Alpha_ = getParameterValue(18)
      _____Caract_ristiques_du_r_seau = getParameterValue(19)
      D_bit_Qv__reference__ou = getParameterValue(20)
      Hmt_Statique_r_siduelle____reference = getParameterValue(21)
      Hmt_dynamique_reference_ = getParameterValue(22)

		
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      On_Off = GetInputValue(1)
      Temp_rature_____Entr_e_Fluide = GetInputValue(2)
      __Consigne_D_bit_Entr_e_Fluide = GetInputValue(3)
      A1P_ou_2P = GetInputValue(4)
		

	!Check the Inputs for Problems (#,ErrorType,Text)
	!Sample Code: If( IN1 <= 0.) Call FoundBadInput(1,'Fatal','The first input provided to this model is not acceptable.')
 
      If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------

	!-----------------------------------------------------------------------------------------------------------------------
	!If Needed, Get the Previous Control States if Discrete Controllers are Being Used (#)
	!Sample Code: CONTROL_LAST=getPreviousControlState(1)
	!-----------------------------------------------------------------------------------------------------------------------

	!-----------------------------------------------------------------------------------------------------------------------
	!If Needed, Get the Values from the Global Storage Array for the Static Variables (#)
	!Sample Code: STATIC1=getStaticArrayValue(1)
	!-----------------------------------------------------------------------------------------------------------------------

	!-----------------------------------------------------------------------------------------------------------------------
	!If Needed, Get the Initial Values of the Dynamic Variables from the Global Storage Array (#)
	!Sample Code: T_INITIAL_1=getDynamicArrayValueLastTimestep(1)
	!-----------------------------------------------------------------------------------------------------------------------

	!-----------------------------------------------------------------------------------------------------------------------
	!Perform All of the Calculations Here to Set the Outputs from the Model Based on the Inputs

	!Sample Code: OUT1=IN1+PAR1

	!If the model requires the solution of numerical derivatives, set these derivatives and get the current solution
	!Sample Code: T1=getNumericalSolution(1)
	!Sample Code: T2=getNumericalSolution(2)
	!Sample Code: DTDT1=3.*T2+7.*T1-15.
	!Sample Code: DTDT2=-2.*T1+11.*T2+21.
	!Sample Code: Call SetNumericalDerivative(1,DTDT1)
	!Sample Code: Call SetNumericalDerivative(2,DTDT2)

!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
		Call SetOutputValue(1, 0.d0) ! Temp�rature   sortie fluide
		Call SetOutputValue(2, 0.d0) ! D�bit    Qm     sortie fluide
		Call SetOutputValue(3, 0.d0) ! D�bit    Qv       sortie fluide
		Call SetOutputValue(4, 0.d0) ! Hmt 
		Call SetOutputValue(5, 0.d0) ! Puissance �lectrique
		Call SetOutputValue(6, 0.d0) ! Chaleur transfer�e au fluide
		Call SetOutputValue(7, 0.d0) ! Chaleur perdue
		Call SetOutputValue(8, 0.d0) ! Rendement hydraulique
		Call SetOutputValue(9, 0.d0) ! Rendement moteur

!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!If Needed, Store the Desired Disceret Control Signal Values for this Iteration (#,State)
!Sample Code:  Call SetDesiredDiscreteControlState(1,1)
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!If Needed, Store the Final value of the Dynamic Variables in the Global Storage Array (#,Value)
!Sample Code:  Call SetDynamicArrayValueThisIteration(1,T_FINAL_1)
!-----------------------------------------------------------------------------------------------------------------------
 
      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

