!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3040

! Object: Inperpolation Splines Cubique
! Simulation Studio Model: SplinesCubiques
! Author: DF
! Editor: DF
! Date:	 June 19, 2015
! last modified: June 19, 2018
! *** 
!************************************************************************
      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------
!DEC$Attributes DLLexport :: Type3040
!-----------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType
      
!     Limites
      Integer, parameter :: NmaxType      =10
      Integer, parameter :: NmaxCouple    =50
      Integer, parameter :: NmaxColonne   =20
      Integer, parameter :: NmaxNbreX     =20

!    PARAMETERS
      Integer Logical_unit
      Integer NbreXY
      Integer NbreX
      Integer NbreLignes
      Integer N_Colonne_Xi
      Integer N_Colonne_Yi
      Double precision Borne_max
      Double precision Borne_min
      Integer ModeExtremite
      Integer ModeExtrapolation
      Double precision deriveG
      Double precision deriveD

!    INPUTS
      DOUBLE PRECISION ValeurX(1:NmaxNbreX)
  
!     Calculs     
      INTEGER                     :: U = 0                 !N� du TYPE 3040 en cours  
      INTEGER                     :: NType(1:NmaxType)=0
      Double precision            :: DataXYZ(1:NmaxType,1:3, 1:NmaxCouple) =0.0  !N�  1=Xi  2=Yi  3=Zi
      Double precision               Xi(1:NmaxCouple)
      Double precision               Yi(1:NmaxCouple)
      Double precision               Zi(1:NmaxCouple)
      
      Double precision               Valeur_X
      Double precision               ValeurY(1:NmaxNbreX)
      Double precision               Lecture(1:Nmaxcolonne)
      Double precision               Var1
      
      Double precision F_spline3_eval3040
      
      
      CHARACTER(Len=MaxLabelLength)   Fichier_Data
      CHARACTER(LEN=MaxMessageLength) MessageErr    
      CHARACTER(LEN=1)                NbreC
      
      Integer i,j,k,y
      Integer Ok,Lu
      Integer max
      
!-----------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()

!-----------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
      
!-----------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf

!-----------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf
      
!-----------------------------------------------------------------------------------------------

!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then
          U = U+1
          If (U>NmaxType) Then
              MessageErr = 'Il y trop de TYPE 3040 dans ce projet'
              Call FoundBadParameter(1,'Fatal',MessageErr)
          End If
          NType(U)=CurrentUnit
          
          NbreX       = NINT(getParameterValue(3))         
          
		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(12)          !The number of parameters that the the model wants
		Call SetNumberofInputs(NbreX)           !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)          !The number of derivatives that the the model wants
		Call SetNumberofOutputs(NbreX)          !The number of outputs that the the model produces
		Call SetIterationMode(1)                !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)      !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)     !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf

!-----------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      Do U=1, NmaxType
	  if (NType(U)==CurrentUnit) EXIT
      end do
      
          Logical_unit        = NINT(getParameterValue(1))
          NbreXY              = NINT(getParameterValue(2))
          NbreX               = NINT(getParameterValue(3))
          NbreLignes          = NINT(getParameterValue(4))
          N_Colonne_Xi        = NINT(getParameterValue(5))
          N_Colonne_Yi        = NINT(getParameterValue(6))
          Borne_max           = getParameterValue(7)
          Borne_min           = getParameterValue(8)
          ModeExtremite       = NINT(getParameterValue(9))
          ModeExtrapolation   = NINT(getParameterValue(10))
          DeriveG             = getParameterValue(11)
          DeriveD             = getParameterValue(12)

      Do i=1, NbreX
          ValeurX(i)= GetInputValue(i)
      End do
 
      If (Borne_min>Borne_max) Then
              MessageErr = 'La borne min est Sup � la borne max'    
              Call FoundBadParameter(1,'Fatal',MessageErr)
      End If
      
C----------------------------------------------------------------------
      LU=Logical_unit
      Fichier_Data=getLUfileName(LU)
      OPEN(Lu,File=Fichier_Data,Action="Read",Position="Rewind",IOSTAT=Ok)   
      REWIND(Lu)
      If (ok>0) then
	    MessageErr='**** Erreur dans Fichier des donnees des Xi et Yi *** '  
			CALL FoundBadInput(1,'Fatal',MessageErr)			
		Return 
      ENDIF 

      If (Nbrelignes>0) then
          Do i=1, Nbrelignes
              READ(Lu,*)
          End do
      End if
      
      Max=N_Colonne_Yi; if (N_Colonne_Xi>N_Colonne_Yi) Max=N_Colonne_Xi
      Do i=1, NbreXY
          READ (Lu,*,ERR=2000) (Lecture(y),y=1,Max)
          DataXYZ(U,1,i)=lecture(N_Colonne_Xi)
          DataXYZ(U,2,i)=lecture(N_Colonne_Yi)
      End do
      Close (Lu)

      If (Borne_min>DataXYZ(U, 1, NbreXY)) Then
              MessageErr = 'La borne min est Sup � Ximax (couples)'    
              Call FoundBadParameter(1,'Fatal',MessageErr)
      End If
      
      !Calcul coef Zi
      Do i=1,NbreXY
          Xi(i)=DataXYZ(U,1,i)
          Yi(i)=DataXYZ(U,2,i)
      End do
            
      Call Spline3_CoefDZ_3040(NmaxCouple, NbreXY, Xi, Yi, Zi, modeExtremite, DeriveG, DeriveD)
      
      Do i=1,NbreXY
          DataXYZ(U,3,i)=Zi(i)
      End do
      
	Goto 1000
		Return

      EndIf

!-----------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
1000  If(getIsReReadParameters()) Then
      Do U=1, NmaxType
	  if (Ntype(U)==CurrentUnit) EXIT
      end do
      
      !Read in the Values of the Parameters from the Input File
          Logical_unit        = NINT(getParameterValue(1))
          NbreXY              = NINT(getParameterValue(2))
          NbreX               = NINT(getParameterValue(3))
          NbreLignes          = NINT(getParameterValue(4))
          N_Colonne_Xi        = NINT(getParameterValue(5))
          N_Colonne_Yi        = NINT(getParameterValue(6))
          Borne_max           = getParameterValue(7)
          Borne_min           = getParameterValue(8)
          ModeExtremite       = NINT(getParameterValue(9))
          ModeExtrapolation   = NINT(getParameterValue(10))
          DeriveG             = getParameterValue(11)
          DeriveD             = getParameterValue(12)
	
      EndIf
!-----------------------------------------------------------------------------------------------

!Read the Inputs
      Do i=1, NbreX
          ValeurX(i)= GetInputValue(i)
      End do

      If(ErrorFound()) Return

!-----------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------
      Do i=1,NbreXY
          Xi(i)=DataXYZ(U,1,i)
          Yi(i)=DataXYZ(U,2,i)
          Zi(i)=DataXYZ(U,3,i)
      End do
      
      ValeurY = 0.0
      Do i=1, NbreX
          Valeur_X = ValeurX(i)
          If (Valeur_X<Borne_min) Then 
              If (i<10) Then
                  MessageErr='**** Erreur Input-'//ACHAR(48+i)//' inf�rieur � la borne Inf'
              Else
                  If (i<20) Then
                      MessageErr='**** Erreur Input-1'//ACHAR(38+i)//' inf�rieur � la borne Inf'
                  Else
                      MessageErr='**** Erreur Input-20 inf�rieur � la borne Inf'
                  End If
              End If      
			CALL FoundBadInput(1,'Fatal',MessageErr)
          End If
          
          If (Valeur_X>Borne_max) Then  
              If (i<10) Then
                  MessageErr='**** Erreur Input-'//ACHAR(48+i)//' sup�rieur � la borne Sup'
              Else
                  If (i<20) Then
                      MessageErr='**** Erreur Input-1'//ACHAR(38+i)//' sup�rieur � la borne Sup'
                  Else
                      MessageErr='**** Erreur Input-20 sup�rieur � la borne Sup'
                  End If
              End If      
			CALL FoundBadInput(1,'Fatal',MessageErr)
          End If 
          
          IF (ModeExtrapolation==2) Then
              If (Valeur_X<Xi(1))         Valeur_X = Xi(1)
              If (Valeur_X>Xi(NbreXY))    Valeur_X = Xi(NbreXY)
          End If

          ValeurY(i)=F_Spline3_eval3040(NmaxCouple,NbreXY, Xi,Yi,Zi,Valeur_X)
      End do

!Set the Outputs from this Model (#,Value)
      Do i=1, NbreX
		Call SetOutputValue(i, ValeurY(i)) ! Valeur interpoler Y
      End do

       Goto 5000
       
 2000 CONTINUE
      If ((i==1).or.(y==1)) Then 
          MessageErr='**** Erreur Nbre colonnes PAR(4) ou PAR(5) > nbre donn�es disponible dans le fichier' 
      Else
          MessageErr='**** Erreur Nbre couple PAR(1) > nbre donn�es (lignes) disponible dans le fichier'  
      End If
      Call MESSAGES(-1,MessageErr,'FATAL',CurrentUnit,CurrentType)
      Return
      
 5000 Continue
      Return
      End Subroutine      
      
!-----------------------------------------------------------------------------------------------      

      Subroutine Spline3_CoefDZ_3040(NmaxCouple, ncouple, Xi, Yi, Zi, mode, DeriveG, DeriveD)
          Implicit none
          Integer NmaxCouple, ncouple, mode, n, iFlag
          Double precision, dimension(1:NmaxCouple)   :: Xi, Yi, Zi
          Double precision DeriveG, DeriveD
          Double precision, dimension(1:NCouple,1:NCouple) :: Matrice
          Double precision, dimension(1:ncouple,1:1) :: Resultats
          Double precision, dimension(0:nCouple) :: h
          Double precision, dimension(1:ncouple,1:1) :: v
          Integer i
          
          Matrice = 0.0; h = 0.0; v = 0.0
          n = ncouple
          
          Do i=1, n-1
              h(i)= Xi(i+1)-Xi(i)
          End do
          
          Do i=2,n-1
              v(i,1)=6.0*(((Yi(i+1)-Yi(i))/h(i))-((Yi(i)-Yi(i-1))/h(i-1)))
          End do
          
          Do i=3,n-2
              Matrice(i,i-1)  = h(i-1)
              Matrice(i,i)    = 2.0*(h(i-1)+h(i))
              Matrice(i,i+1)  = h(i+1)
          End Do
          
          If (Mode==1) Then 
              Matrice(1,1)    = 1.0
              Matrice(1,2)    = 0.0            
              Matrice(2,2)    = 2.0*(h(1)+h(2))
              Matrice(2,3)    = h(2)
              
              Matrice(n-1,n-2)= h(n-2)
              Matrice(n-1,n-1)= 2.0*(h(n-2)+h(n-1))
              Matrice(n,n-1)  = 0.0
              Matrice(n,n)    = 1.0
              
              v(1,1) = 0.0
              v(n,1) = 0.0
          Else
              If (Mode==2) Then 
                  Matrice(1,1)    =  1.0
                  Matrice(1,2)    = -1.0
                  Matrice(2,2)    = 3.0*h(1)+2.0*h(2)
                  Matrice(2,3)    = h(2)
                  
                  Matrice(n-1,n-2)= h(n-2)
                  Matrice(n-1,n-1)= 2.0*h(n-2)+3.0*h(n-1) 
                  Matrice(n,n-1)  =  1.0
                  Matrice(n,n)    = -1.0
                  
                  v(1,1) = 0.0
                  v(n,1) = 0.0
                       
              Else  
                  If (Mode==3) Then!Mode==3
                      Matrice(1,1)    = 1.0
                      Matrice(1,2)    = -1.0*(h(1)+h(2))/h(2)
                      Matrice(1,3)    = h(1)/h(2)
                      Matrice(2,2)    = ((h(1)+h(2))*(h(1)+2.0*h(2)))/h(2)
                      Matrice(2,3)    = (h(2)*h(2)-h(1)*h(1))/h(2)
                  
                      Matrice(n-1,n-2)= (h(n-2)*h(n-2)-h(n-1)*h(n-1))/h(n-2)
                      Matrice(n-1,n-1)= ((h(n-1)+h(n-2))*(h(n-1)+2.0*h(n-2)))/h(n-2)
                      Matrice(n,n-2)  = h(n-1)/h(n-2)
                      Matrice(n,n-1)  = -1.0*(h(n-2)+h(n-1))/h(n-2)
                      Matrice(n,n)    = 1.0
                  
                      v(1,1) = 0.0
                      v(n,1) = 0.0
                      
                  Else  !Mode==4
                      Matrice(1,1)    = h(1)/3.0
                      Matrice(1,2)    = h(1)/6.0
                      Matrice(1,3)    = 0.0
                      
                      Matrice(2,1)    = h(1)
                      Matrice(2,2)    = 2.0*(h(1)+h(2))
                      Matrice(2,3)    = h(2)
                  
                      Matrice(n-1,n-2)= h(n-2)
                      Matrice(n-1,n-1)= 2.0*(h(n-2)+h(n-1))
                      Matrice(n-1,n)  = h(n-1)
                      
                      Matrice(n,n-2)  = 0.0
                      Matrice(n,n-1)  = h(n-1)/6.0
                      Matrice(n,n)    = h(n-1)/3.0
                  
                      v(1,1) = ((Yi(2)-Yi(1))/h(1))-DeriveG
                      v(n,1) = DeriveD-((Yi(n)-Yi(n-1))/h(n-1))
                  End If
              End If
          End IF
          
         Call InvertMatrix(ncouple, ncouple, Matrice, iflag)
          
         Resultats  = MATMUL(Matrice,V)
         Do i=1,n
              Zi(i) = Resultats(i,1)
         End Do
          
      End Subroutine
      

      Function F_spline3_eval3040(NmaxCouple,ncouple, Xi, Yi , Si, X)
          Implicit none
          Double precision F_spline3_eval3040
          Integer NmaxCouple, n, ncouple
          Double precision X
          Double precision, dimension(1:NmaxCouple)   :: Xi, Yi, Si
          Double precision ai, bi, ci, di
          Double precision hi, deltaX
          Integer i
          
          If (X>Xi(ncouple-1)) Then
              i=ncouple-1
          Else
              Do i=1, ncouple-2
                  If (X<=Xi(1+i)) Exit
              End Do
          End If
      
          hi = Xi(i+1)-Xi(i)
          ai = (Si(i+1)-Si(i))/(6.0*hi)
          bi = Si(i)/2.0
          Ci = ((Yi(i+1)-Yi(i))/hi)-((2.0*Si(i)+Si(i+1))*hi/6.0)
          di = Yi(i)
          
          DeltaX = (X-Xi(i))
          
          F_spline3_eval3040 = ai*DeltaX*DeltaX*DeltaX+bi*DeltaX*DeltaX+ci*DeltaX+di   
      End Function
      
