!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3070

! Object: Analyse spectrale
! Simulation Studio Model: Type3070
! 
! Author: DF
! Editor: DF
! Date:	 July 07, 2018
! last modified: July 07, 2018 
! *** 
! *** Model Parameters 
!			Nbre Harmonique	- [1;1000]
! *** 
! *** Model Inputs 
!			Signal 	- [-Inf;+Inf]
!			CoefA	- [-Inf;+Inf]
!			B	- [-Inf;+Inf]
!			CoefC	- [-Inf;+Inf]
! *** 
! *** Model Outputs 
!			Signal	- [-Inf;+Inf]
      
! (Comments and routine interface generated by TRNSYS Studio)
!************************************************************************
      Use TrnsysConstants
      Use TrnsysFunctions
!-----------------------------------------------------------------------------------------------------------------------
!DEC$Attributes DLLexport :: Type3070
!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      Integer Nbre_Harmonique
      Integer LUi


!    INPUTS
      DOUBLE PRECISION SignalBrut
      DOUBLE PRECISION CoefA
      DOUBLE PRECISION B
      DOUBLE PRECISION CoefC
      
!  Calculs
      DOUBLE PRECISION Signal, Signal_A(0:8760)
      DOUBLE PRECISION A0
      Double precision APhi(1:2,1:1000)
      Double precision TimeStart, TimeStop
      Double precision somme
      DOUBLE PRECISION :: PI=3.141592653589793
      Double precision Var1, Var2
      Integer          NbrePoint
      Character (len=maxLabelLength) FichierHarmonique
      Character*1 TAB
      Character*128 MessageErr
      
      INTEGER i,j,k
      
      Save Signal
      Save Signal_A
      Save Somme
      Save NbrePoint
      Save Nbre_Harmonique

      
      TAB=CHAR(9)
      
!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time        = getSimulationTime()
      Timestep    = getSimulationTimeStep()
      TimeStop    = getSimulationStopTime()
      TimeStart   = getSimulationStartTime()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
          
          Call S_Harmonique(TimeStart, TimeStop, TimeStep, Signal_A,Nbre_Harmonique,A0,APhi)    
                          
          Var1 = (TimeStop-TimeStart)/(2.*PI)
          FichierHarmonique="T3070_FichierHarmoniques.txt"
          Do i=1,Nbre_Harmonique
              APhi(2,i) = APhi(2,i)*Var1
              If (APhi(2,i)<0.0) APhi(2,i) = 8760.0+APhi(2,i)
              If ((APhi(2,i)>8759.99999).and.(APhi(2,i)<8760.00001)) APhi(2,i) = 0.0
          End Do
          
          Lui=3070
          Open(Lui,File=FichierHarmonique, Action="Write", Status="Replace")
          Write(Lui,*) "Decomposition en COSINUS"
          Write(Lui,*) "Col 1 => N� harmonique"  
          Write(Lui,*) "Col 2 => 0.5xAmplitude"  
          Write(Lui,*) "Col 3 => Dephasage du max en heures"
          Write(Lui,*)
          Write(Lui,*) "Nbre Harmonique et A0 " 
          Write(Lui,*) Nbre_Harmonique
          Write(Lui,*) A0
          Write(Lui,*)
          Write(Lui,*) "========================================="          
          Do i=1,Nbre_Harmonique
              Write(Lui,*) i,TAB,APhi(1,i),TAB,APhi(2,i)
          End Do
          
          Close(Lui)              

		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
          Signalbrut  = GetInputValue(1)
          CoefA   = GetInputValue(2)
          B       = GetInputValue(3)
          CoefC   = GetInputValue(4)
          Signal = CoefC*(CoefA*SignalBrut+B)
          NbrePoint = NbrePoint+1
          If (NbrePoint>8760) then
           Call FoundBadParameter(1,'Fatal','Probleme controler que START=0h STOP=8760h et STEP=1h')   
          End If
          Signal_A(NbrePoint) = Signal
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then
		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(1)           !The number of parameters that the the model wants
		Call SetNumberofInputs(4)               !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)          !The number of derivatives that the the model wants
		Call SetNumberofOutputs(2)              !The number of outputs that the the model produces
		Call SetIterationMode(1)                !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)      !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)     !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

          !-----------------------------------------------------------------------------------------------------------------------
      !Initialisation
      NbrePoint   = 0
      Signal_A    = 0.0
          
		Return

      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      Nbre_Harmonique     = NINT(getParameterValue(1))


      Signalbrut  = GetInputValue(1)
      CoefA   = GetInputValue(2)
      B       = GetInputValue(3)
      CoefC   = GetInputValue(4)
            
      If ((TimeStep>1.000001).or.(TimeStep<0.99999999)) then
           MessageErr='Step doit etre egale a 1h'
          Call FoundBadParameter(1,'Fatal',MessageErr)   
      End If
       
      IF (TimeStart> 0.000001) then
     	    MessageErr='*** Erreur  START doit �tre �gal � 0'
          CALL FoundBadParameter(1,'Fatal',MessageErr)
          
      End if
      
      IF ((TimeStop>8760.0).or.(TimeStop<8760.0)) then
     	    MessageErr='*** Erreur  STOP doit �tre �gal � 8760';
          CALL FoundBadParameter(1,'Fatal',MessageErr)
      End if          

      !Set the Initial Values of the Outputs (#,Value)
		Call SetOutputValue(1, Signalbrut)  ! Signal en input
 		Call SetOutputValue(2, Signal)      ! Signal analys�         
		Return

      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
          Nbre_Harmonique     = NINT(getParameterValue(1))

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Signalbrut  = GetInputValue(1)
      CoefA   = GetInputValue(2)
      B       = GetInputValue(3)
      CoefC   = GetInputValue(4)
      
      Signal =CoefC*(CoefA*SignalBrut+B)
		
      If(ErrorFound()) Return

		Call SetOutputValue(1, Signalbrut)  ! Signal en input
 		Call SetOutputValue(2, Signal)      ! Signal analys�         


      Return
          End

          
!------------------------------------------------------------------------------------
          SUBROUTINE S_Harmonique(TStart, TStop, TStep, Signal, NbreHarmonique,A0,APhi)
             Implicit none
             Integer, parameter :: Nbrepointmax       =20000
             Integer, parameter :: NbreHarmoniquemax  = 1000
             
             Double precision TStart, TStop, TStep
             Double precision Signal(0:Nbrepointmax)
             Integer          NbreHarmonique, NbrePoint 
             Double precision NbreHeure, heure
             Double precision A0
             Double precision APhi(1:2,1:NbreHarmoniquemax)
             Double precision Omega, Var1, var2
             Double precision a(1:NbreHarmonique)
             Double precision b(1:NbreHarmonique)
             DOUBLE PRECISION :: PI=3.141592653589793
             INTEGER Debut, Fin
             INTEGER i,j
             
             Nbrepoint = NINT((TStop-TStart)/TStep)
             Nbreheure = TStop-TStart
             Debut     = NINT(TStart)
             Fin       = NINT(TStop)
             
             A0=0.0
             Do i= 1 ,NbrePoint
                 A0=A0+Signal(i)
             End Do
             A0 = A0/DBLE(NbrePoint)
             
             Omega = 2.*PI/Nbreheure
             
              Do j=1, NbreHarmonique
                 Var2 = 0.0 
                 Do i=1, NbrePoint
                     heure = TStart+DBLE(i*TStep-0.5*TStep)
                     Var2 = Var2 + Signal(i)*cos(j*Omega*heure)
                 End Do
                 a(j) = Var2/(0.5*NbreHeure)
             End Do  
             
              Do j=1, NbreHarmonique
                 Var2 = 0.0 
                 Do i=1, NbrePoint
                     heure = TStart+DBLE(i*TStep-0.5*TStep)
                     Var2 = Var2 + Signal(i)*sin(j*Omega*heure)
                 End Do
                 b(j) = Var2/(0.5*NbreHeure)
             End Do  
                 
             ! Coef pour une d�composition en Cosinus
             Do j=1, NbreHarmonique
                APhi(1,j) = (a(j)*a(j)+b(j)*b(j))**0.5
                Aphi(2,J) = ATAN2(b(j),a(j))
             End Do      
      End

