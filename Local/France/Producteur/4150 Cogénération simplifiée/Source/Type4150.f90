!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type4150

! Object: Cog�n�ration MOTEUR de 20 � 3000 kW � combustion interne en sous tirage alternateur
! Simulation Studio Model: Type4150
! 

! Author: Didier FOUQUET
! Editor: 
! Date:	 mai 03, 2004
! last modified: janvier 2019
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Pn Puissance �lectrique	kW [20;3000]
!			Production en HTA	- [0;1]
!			% de Pn pour Puis Elect Aux 100%	- [0;100]
!			% de Pn pour Puis Elect Aux 0%	- [0;100]
!			Temp. entr�e EC nominale	C [50;105]
!			Temp. sortie  EC nominale	C [50;105]
!			Rendement �lectrique � 100% de charge	% (base 100) [-1;100]
!			Rendement thermique � 100% de charge	% (base 100) [-1;100]
!			Rendement �lectrique �   50% de charge	% (base 100) [-1;100]
!			Rendement thermique �   50% de charge	% (base 100) [-1;100]
!			Puissance min �lectrique	% (base 100) [0;100]

! *** 
! *** Model Inputs 
! *** 
!			Engagement	- [0;1]
!			Temp. Entr�e cog�n�ration	C [-Inf;+Inf]
!			R�gulation de la cog�n�ration	- [-2;1]
!			Si Input(3)=-2  Ts consigne	- [20;110]

! *** 
! *** Model Outputs 
! *** 
!			Puiss th valoris�e	kJ/hr [-Inf;+Inf]
!			Puiss gaz en PCI	kJ/hr [-Inf;+Inf]
!			Puiss Elect net	kJ/hr [-Inf;+Inf]
!			Temp�rature eau sortie 	C [-Inf;+Inf]
!			D�bit sortie Qm	kg/hr [0;+Inf]
!			Puiss Elect produite	kJ/hr [-Inf;+Inf]
!			Puiss th valorisable	kJ/hr [-Inf;+Inf]
!			Charge moteur	- [0;1]
!			Rend Electrique	% (base 100) [0;100]
!			Rend Thermique	% (base 100) [0;100]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Simulation Studio)
!************************************************************************

!-----------------------------------------------------------------------------------------------------------------------
! This TRNSYS component skeleton was generated from the TRNSYS studio based on the user-supplied parameters, inputs, 
! outputs, and derivatives.  The user should check the component formulation carefully and add the content to transform
! the parameters, inputs and derivatives into outputs.  Remember, outputs should be the average value over the timestep
! and not the value at the end of the timestep; although in many models these are exactly the same values.  Refer to 
! existing types for examples of using advanced features inside the model (Formats, Labels etc.)
!-----------------------------------------------------------------------------------------------------------------------

      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type4150

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      DOUBLE PRECISION Pn
      INTEGER          HTA
      DOUBLE PRECISION Paux0
      DOUBLE PRECISION Paux50
      DOUBLE PRECISION Paux100
      DOUBLE PRECISION Te_max
      DOUBLE PRECISION Ts_max
      DOUBLE PRECISION Rend100Elec
      DOUBLE PRECISION Rend100Th
      DOUBLE PRECISION Rend50Elec
      DOUBLE PRECISION Rend50Th
      DOUBLE PRECISION P_Pn_min
      DOUBLE PRECISION P3_Veille

!    INPUTS
      INTEGER          Engagement
      DOUBLE PRECISION Temp_em
      DOUBLE PRECISION Regulation
      DOUBLE PRECISION Temp_Consigne
      
 !     Calculs 
      INTEGER           i,j
      DOUBLE PRECISION  PTh_Valorisable
      DOUBLE PRECISION  Pth_valorisee
      DOUBLE PRECISION  Pth_aeroHT
      DOUBLE PRECISION  Puis_Gaz
      DOUBLE PRECISION  PUIS_ELEC
      DOUBLE PRECISION  Puis_Elecnet
      DOUBLE PRECISION  Ts, Te
      DOUBLE PRECISION Rend100ElecV0, Rend50ElecV0 
      DOUBLE PRECISION Rend100ThV0, Rend50ThV0 
      DOUBLE PRECISION PnV0, DebitV0
      DOUBLE PRECISION RendElec, RendTh
      DOUBLE PRECISION Charge
      DOUBLE PRECISION Puissance
      DOUBLE PRECISION Puis_gazavide
      DOUBLE PRECISION Var1, Var2, Var3
      DOUBLE PRECISION Cp
      DOUBLE PRECISION Etat
      DOUBLE PRECISION Pn_min
      DOUBLE PRECISION DebitVolEntree
      DOUBLE PRECISION Tmoyen, Dt
      DOUBLE PRECISION Tsback, Puissanceback
      DOUBLE PRECISION h1, h2, Deltah
      DOUBLE PRECISION Qm_min
      DOUBLE PRECISION a0, a1, a2
      DOUBLE PRECISION b0, b1, b2
      DOUBLE PRECISION MatRend(1:3,1:3)
      DOUBLE PRECISION MatAux(1:3,1:3)
     
      DOUBLE PRECISION MatCoefa(1:3,1:1)
      DOUBLE PRECISION MatCoefb(1:3,1:1)
      DOUBLE PRECISION MatCoefy(1:3,1:1)
      
      DOUBLE PRECISION P_QV_min
      DOUBLE PRECISION QV_Min
      DOUBLE PRECISION DEBIT
      DOUBLE PRECISION CONSOELECT, P_ConsoElect
      DOUBLE PRECISION AutoconsoElec
      DOUBLE PRECISION RendTransfo
      DOUBLE PRECISION PTh_valomax
     

      DOUBLE PRECISION F_VolumeMassiqueEaupure_4150
      DOUBLE PRECISION F_EnthalpieEaupure_4150
      DOUBLE PRECISION F_Inv_EnthalpieEaupure_4150
      DOUBLE PRECISION F_Rtf_4150

      DATA MatRend /8.0,-10.0,3.0, -16.0,24.0,-8.0, 8.0,-14.0, 6.0/
      DATA MatAux /2.0,-3.0,1.0, -4.0,4.0,0.0, 2.0,-1.0,0.0/

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time          = getSimulationTime()
      Timestep      = getSimulationTimeStep()
      CurrentUnit   = getCurrentUnit()
      CurrentType   = getCurrentType()

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then

		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(13)      !The number of parameters that the the model wants
		Call SetNumberofInputs(4)           !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)      !The number of derivatives that the the model wants
		Call SetNumberofOutputs(11)         !The number of outputs that the the model produces
		Call SetIterationMode(1)            !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(11,0) !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0) !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      Pn            = getParameterValue(1)
      HTA           = NINT(getParameterValue(2))
      Paux0         = getParameterValue(3)/100.*Pn
      Paux50        = (1.0-getParameterValue(4)/100.)*Paux0
      Paux100       = (1.0-getParameterValue(5)/100.)*Paux0
      Te_max        = getParameterValue(6)
      Ts_max        = getParameterValue(7)
      Rend100Elec   = getParameterValue(8)/100.
      Rend100Th     = getParameterValue(9)/100.
      Rend50Elec    = getParameterValue(10)/100.
      Rend50Th      = getParameterValue(11)/100.
      P_Pn_min      = getParameterValue(12)/100.
      ConsoElect    = getParameterValue(13)/100.*Pn

      Engagement    = NINT(GetInputValue(1))
      Temp_em       = GetInputValue(2)
      Regulation    = GetInputValue(3)
      Temp_consigne = GetInputValue(4)

!   ------------------------------------------caract�ristiques du module       
                
       Rend100ElecV0=Rend100Elec
        If (Rend100Elec<0.0) Then
            IF (Pn<=70.0) Then 
                Rend100ElecV0=0.033*log(Pn)+0.208
            Else 
                If (Pn<=230) Then
                Rend100ElecV0=0.3422+0.0000885*Pn
                Else
                Rend100ElecV0=0.018*log(Pn)+0.264
                End If
            End if
        End if
        Rend50ElecV0=Rend50Elec
        If (Rend50Elec<0.0) Rend50ElecV0 = 1.063*Rend100ElecV0-0.060
        MatCoefy(1,1)=Rend100ElecV0 ; MatCoefy(2,1)=0.65*Rend100ElecV0+0.35*Rend50ElecV0; MatCoefy(3,1)=Rend50ElecV0
        MatCoefa = MatMul(MatRend,Matcoefy)
        
        MatCoefy(1,1)=Paux0; MatCoefy(2,1)=Paux50; MatCoefy(3,1)=Paux100
        MatCoefb = MatMul(MatAux,Matcoefy)

        Rend100ThV0=Rend100th
        If (Rend100th<0.0)  Rend100ThV0  =-7.379*Rend100ElecV0*Rend100ElecV0+3.573*Rend100ElecV0+0.196

        Rend50ThV0=Rend50Th
        If (Rend50Th<0.0)   Rend50ThV0   =-7.379*Rend50ElecV0*Rend50ElecV0+3.573*Rend50ElecV0+0.196
        
        PnV0    = Pn
        
        If (P_Qv_min>0.0) Then
            Qv_min = P_Qv_min
        Else
            h1      = F_EnthalpieEaupure_4150(Te_max)
            h2      = F_EnthalpieEaupure_4150(Ts_max)
            Var1    = Pn/Rend100ElecV0*Rend100ThV0*3600.
            Qm_min  = Var1/(h2-h1)
        End If
        
        Pn_min = PnV0*P_Pn_min/100.0
          
        Call SetStaticArrayValue(1,MatCoefa(1,1))
        Call SetStaticArrayValue(2,MatCoefa(2,1))  
        Call SetStaticArrayValue(3,MatCoefa(3,1))
        Call SetStaticArrayValue(4,Rend100ThV0)
        Call SetStaticArrayValue(5,Rend50ThV0)
        Call SetStaticArrayValue(6,PnV0)
        Call SetStaticArrayValue(7,Qm_min)
        Call SetStaticArrayValue(8,Pn_min)
        Call SetStaticArrayValue(9, MatCoefb(1,1))
        Call SetStaticArrayValue(10,MatCoefb(2,1))  
        Call SetStaticArrayValue(11,MatCoefb(3,1))

        Goto 1000
		Return

      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
1000 CONTINUE
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
      Pn            = getParameterValue(1)
      HTA           = NINT(getParameterValue(2))
      Paux0         = getParameterValue(3)/100.*Pn
      Paux50        = (1.0-getParameterValue(4)/100.)*Paux0
      Paux100       = (1.0-getParameterValue(5)/100.)*Paux0
      Te_max        = getParameterValue(6)
      Ts_max        = getParameterValue(7)
      Rend100Elec   = getParameterValue(8)/100.
      Rend100Th     = getParameterValue(9)/100.
      Rend50Elec    = getParameterValue(10)/100.
      Rend50Th      = getParameterValue(11)/100.
      P_Pn_min      = getParameterValue(12)/100.
      P_ConsoElect  = getParameterValue(13)/100.*Pn	
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Engagement    = NINT(GetInputValue(1))
      Temp_em       = GetInputValue(2)
      Regulation    = GetInputValue(3)
      Temp_consigne = GetInputValue(4)
      
      If(ErrorFound()) Return

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
        a2              = GetStaticArrayValue(1)
        a1              = GetStaticArrayValue(2)
        a0              = GetStaticArrayValue(3)
        Rend100ThV0     = GetStaticArrayValue(4)
        Rend50ThV0      = GetStaticArrayValue(5)        
        PnV0            = GetStaticArrayValue(6)
        debit           = GetStaticArrayValue(7)         
        Pn_min          = GetStaticArrayValue(8)     
        b2              = GetStaticArrayValue(9)
        b1              = GetStaticArrayValue(10)
        b0              = GetStaticArrayValue(11)
        
! -----------------------------------------------------------------------------------------
            RendElec    = a1+a2+a0
            RendTh      = Rend100ThV0
! -----------------------------------------------------------------------------------------
!Cas 0 Module Off --------------------------------------------------------------------
        If (Engagement==0) then
            Pth_valorisable     = 0.0
            Pth_valorisee       = 0.0
            Puis_gaz            = 0.0
            Puis_Elec           = 0.0
            Puis_Elecnet        = 0.0
            Ts                  = Temp_em
            Charge              = 0.0
            ConsoElect          = 0.0
            GOTO 3000
        End if  

!Cas 1 Module est engag�  Engagement = 1 => On -----------------------------------------   
            Puissance       = PnV0
            RendElec        = Rend100ElecV0
            RendTh          = Rend100ThV0
            Puis_gaz        = Puissance/RendElec
            PTh_valomax     = Puis_gaz*RendTh 
            
        If (Temp_em>Ts_max) then
            Pth_valorisable     = 0.0
            Pth_valorisee       = 0.0
            Puis_gaz            = 0.0
            Puis_Elec           = 0.0
            Puis_Elecnet        = 0.0
            Ts                  = Temp_em
            Charge              = 0.0
            ConsoElect          = P_ConsoElect
            GOTO 3000
        End if  
       
       !Module en veille 
       If ((Regulation>-0.00001).and.(Regulation<0.00001)) then  
            Pth_valorisable     = 0.0
            Pth_valorisee       = 0.0
            Puis_gaz            = 0.0
            Puis_Elec           = 0.0
            Puis_Elecnet        = 0.0
            Ts                  = Temp_em
            ConsoElect          = P_ConsoElect
            Charge              = 0.0
            GOTO 2000
       End if
       
       Te = Temp_em; PTh_aeroHT = 0.0
       If (Temp_em>Te_max) Then
           Te = Te_max
           Pth_aeroHT = Debit*(F_EnthalpieEaupure_4150(Temp_em)-F_EnthalpieEaupure_4150(Te_max))/3600.
       End If
       
        If (NINT(Regulation)==-1) Then !Fonction du module en fonction de Pn 
            Puissance       = PnV0
            Charge          = 1.0
            RendElec        =Rend100ElecV0
            RendTh          =Rend100ThV0
            Puis_gaz        =Puissance/RendElec
            PTh_valorisable =Puis_gaz*RendTh   
            h2 = PTh_valorisable*3600.0/debit+F_EnthalpieEaupure_4150(Te)
            Ts = F_Inv_EnthalpieEaupure_4150(h2)
            Puis_Elec   = Puissance
            ConsoElect = 0.0
            GOTO 2000
        End If
       
         If (Regulation>=0.0) Then !Fonction du module en fonction de Pn 
            Puissance       = PnV0*Regulation
            If (Puissance<Pn_min) Then !Controle de la charge min
                Puissance = Pn_min
            End If
            
            Charge          = Puissance/PnV0
            RendElec        = a2*Charge*Charge+a1*charge+a0
            RendTh          = Rend50ThV0+(Rend100ThV0-Rend50ThV0)/0.5*(Charge-0.5)
            Puis_gaz        = Puissance/RendElec
            PTh_valorisable = Puis_gaz*RendTh
            
            h2 = PTh_valorisable*3600.0/debit+F_EnthalpieEaupure_4150(Te)
            Ts = F_Inv_EnthalpieEaupure_4150(h2)
            Puis_Elec   = Puissance
            ConsoElect = 0.0
            GOTO 2000
         End If

         If (NINT(Regulation)==-2) Then !Fonction de la consigne Ts
            Do j=0,200
                Puissanceback   = PnV0*(1.0-DBLE(j)/200.)
                If (Puissanceback<Pn_min) Then !Controle de la charge min
                    Pth_valorisable     = 0.0
                    Puis_gaz            = 0.0
                    Puis_Elec           = 0.0
                    Ts                  = Temp_em
                    ConsoElect          = P_ConsoElect 
                    Charge              = 0.0
                    goto 2000
                End If 
                
            Charge          = Puissanceback/PnV0
            RendElec        = a2*Charge*Charge+a1*charge+a0
            RendTh          = Rend50ThV0+(Rend100ThV0-Rend50ThV0)/0.5*(Charge-0.5)
            Puis_gaz        = Puissanceback/RendElec
            PTh_valorisable = Puis_gaz*RendTh
            
            h2 = PTh_valorisable*3600.0/debit+F_EnthalpieEaupure_4150(Te)
            Ts = F_Inv_EnthalpieEaupure_4150(h2)
            
            If (Ts<=Temp_consigne) Then
                Puis_Elec   = Puissanceback
                ConsoElect  = 0.0
                Exit
            End If
            End Do
         End If
                  
2000     CONTINUE
        Pth_valorisee   = Debit*(F_EnthalpieEaupure_4150(Ts)-F_EnthalpieEaupure_4150(Temp_em))/3600.
        Var1            = 1.0-(Pth_valorisable-min(0.0,Pth_valorisee))/PTh_valomax
        AutoconsoElec   = b2*Var1*Var1+b1*Var1+b0    
        Var2            = (Pn*Charge-AutoconsoElec)/Pn
        RendTransfo     = F_Rtf_4150(Pn, HTA, Var2)
        Puis_Elecnet    = (Pn*Charge-AutoconsoElec)*RendTransfo     
        
        
3000     CONTINUE              
!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
		Call SetOutputValue(1, Pth_valorisee*3600.)     ! Puiss th valoris�e
		Call SetOutputValue(2, Puis_gaz*3600.)          ! Puiss gaz en PCI
		Call SetOutputValue(3, Puis_Elecnet*3600.)      ! Puiss Elect moins les besoins pour la centrale
		Call SetOutputValue(4, Ts)                      ! Temp�rature eau sortie 
		Call SetOutputValue(5, Debit)                   ! D�bit
		Call SetOutputValue(6, Puis_Elec*3600.)         ! Puiss Elect sortie alternateur
		Call SetOutputValue(7, Pth_valorisable*3600.)   ! Puiss Thermique valorisable
		Call SetOutputValue(8, Charge)                  ! Charge du moteur entre 0 et 1
		Call SetOutputValue(9, RendElec*100.0)          ! Rend Electrique  entre 0 et 100
		Call SetOutputValue(10,RendTh  *100.0)          ! Rend Thermique   entre 0 et 100
 		Call SetOutputValue(11,ConsoElect*3600.)        ! Consommation electrique

      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

    Function F_VolumeMassiqueEaupure_4150(T)  !m3/kg  � la pression de 3 bar
      Implicit None
      Double precision F_VolumemassiqueEaupure_4150
      Double precision T
          If ((T<0.0).or.(T>120.0)) F_VolumemassiqueEaupure_4150=-999999.0
          
          If (T<10.0) then
              F_VolumemassiqueEaupure_4150= (0.001000067804 - 7.88986124E-08*T + 9.86075690E-09*T**2 - 1.02591815E-10*T**3)/0.99985
          Else
              F_VolumemassiqueEaupure_4150=  0.000999155653 + 3.98284772E-08*T + 4.64249589E-09*T**2 - 6.18019299E-12*T**3
          End IF
    End Function
    
    
    FUNCTION F_EnthalpieEaupure_4150(T)  !kJ/kg  entre 0 et 120�C
      IMPLICIT NONE
        DOUBLE PRECISION F_EnthalpieEaupure_4150
        DOUBLE PRECISION T
        If ((T<0.0).or.(T>120.0)) F_EnthalpieEaupure_4150=-999999.0
          If (T<15.0) then
          F_EnthalpieEaupure_4150      =0.269857583 + 4.22415437*T - 0.00307496692*T**2 + 0.0000791360745*T**3
          Else 
              If (T<50) then
              F_EnthalpieEaupure_4150  =0.480874333 + 4.18089545*T + 0.0000690985917*T**2 - 9.60903175E-07*T**3
              Else
              F_EnthalpieEaupure_4150  =0.0771232789 + 4.21113135*T - 0.000644367575*T**2 + 0.00000446853967*T**3
              End If
          End If
    END FUNCTION    
      

    FUNCTION F_Inv_EnthalpieEaupure_4150(Enthalpie) !Returne la temp�rature correspondant � l'Enthalpie
      !kJ/kg  entre 0 et 120�C
      Implicit None
          Double precision F_Inv_EnthalpieEaupure_4150
          Double precision F_EnthalpieEaupure_4150
          Double precision Enthalpie
          Double precision Var1, Var2, Delta
          Double precision Tmin, Tmax, T, h1, h
          Integer i,j
          
          If ((Enthalpie<0.0).or.(Enthalpie>600.0)) F_Inv_EnthalpieEaupure_4150=-999999.0
          T= Enthalpie/4.18  !Estimation de la temperature
          Tmin =T-2.; Tmax=T+2.; h=Enthalpie
          Do i=1,100
              h1=F_EnthalpieEaupure_4150(T)
              If ((ABS(h1-h)/h)<0.00001) Exit
              If (h1>h) then
                  Tmax=T
              Else
                  Tmin=T
              End If
              T=0.5*(Tmax+Tmin)
          End DO
          F_Inv_EnthalpieEaupure_4150=0.5*(Tmax+Tmin)
    End Function
    
    FUNCTION F_Rtf_4150(Pn,HTA,Charge)
        Implicit none
        Double precision F_Rtf_4150
        Double precision Pn
        Integer          HTA
        Double precision Charge
        Double precision PertesF
        Double precision PertesJ100
        Double precision PertesJ
        Double precision Puissance
        F_Rtf_4150  = 1.0
        If (HTA==1) Then
            PertesF     = 155+0.638*Pn/1000.0
            PertesJ100  = 1420.0+8.126*Pn/1000.0
            PertesJ     = Charge*Charge*PertesJ100   
            Puissance   = Pn*1000.0*Charge
            F_Rtf_4150  = Puissance/(Puissance+PertesJ100+PertesJ)
        End If
    End Function
    
        
        
        
