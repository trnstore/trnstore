!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type4725

! Object: R�gulation BP confort ISO 1521
! Simulation Studio Model: Type4725
! 

! Author: Didier FOUQUET
! Editor: TEP2E
! Date:	 July 10, 2021
! last modified: July 10, 2020
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Mode de calcul	- [0;1]
!			Niveau du confort	- [1;3]
!			BP Chauffage	- [0;10]
!			%BP chauffage au dessus	% (base 100) [0;100]
!			BP Rafraichissement	- [0;10]
!			%BP Rafraichissement au dessous	% (base 100) [0;100]
!			Nbre de Thermostats	- [1;5]
!           Temp Initialisation 7J

! *** 
! *** Model Inputs 
! *** 
!			Engagement de la r�gulation	- [0;3]
!			Temp Ext	C [-Inf;+Inf]
!			Top_	- [-Inf;+Inf]

! *** 
! *** Model Outputs 
! *** 
!			Temp Trm 	- [-Inf;+Inf]
!			Top de confort_	C [-Inf;+Inf]
!			Besoin de chauffage_	% (base 100) [0;100]
!			Besoin de rafraichissement_	% (base 100) [0;100]
!			Variation des besoins_	- [-1;1]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Simulation Studio)
!************************************************************************


      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type4725

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      INTEGER           ModeCalcul
      INTEGER           Nconfort
      DOUBLE PRECISION  BP_Chauffage
      DOUBLE PRECISION  BP_chauffage_dessus
      DOUBLE PRECISION  BP_Rafraichissement
      DOUBLE PRECISION  BP_Rafraichissement_dessous
      INTEGER           Nbrethermostats
      DOUBLE PRECISION  TempInit

!    INPUTS
      INTEGER          Engagement
      DOUBLE PRECISION Temp_Ext
      DOUBLE PRECISION Top(10)
      
!    OUTPUT
      DOUBLE PRECISION Tmglissante
      DOUBLE PRECISION Topconfort
      DOUBLE PRECISION BChauffage(10)
      DOUBLE PRECISION BRafraichissement(10)
      DOUBLE PRECISION Variation(10)
      
!    Calculs
      INTEGER i,j, k
      DOUBLE PRECISION TopBack(10), TopRetenu(10)
 
      DOUBLE PRECISION, PARAMETER :: Epsilon=1.0D-12
      INTEGER,SAVE :: NbreStep=0                
      DOUBLE PRECISION Tmh            
      DOUBLE PRECISION Text24h(1:24)
      DOUBLE PRECISION Coef(7)
      DOUBLE PRECISION Tmjour(1:7)
      DOUBLE PRECISION Delta
      DOUBLE PRECISION Temp
      DOUBLE PRECISION Temp1, Temp2
      



	  CHARACTER*450	MessageErr
      Double precision TimeStart
      Double precision Var1

      Data Coef/1.0,0.8,0.6,0.5,0.4,0.3,0.2/


!-----------------------------------------------------------------------------------------------------------------------
      TimeStart   =getSimulationStartTime()
!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
          
      NbreThermostats = NINT(getParameterValue(7))
      Engagement                    = NINT(GetInputValue(1))
      Temp_Ext                      = GetInputValue(2)
      Do i=1, NbreThermostats
        Top(i) = GetInputValue(2+i)
      End Do
      
      i = 24+7+1
      Do j=1, NbreThermostats
        Call SetStaticArrayValue(i+j,Top(j))
      End Do


		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then
        NbreThermostats = NINT(getParameterValue(7))
        i = 2+NbreThermostats
        j = 32+NbreThermostats
        
		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(8)       !The number of parameters that the the model wants
		Call SetNumberofInputs(i)           !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)      !The number of derivatives that the the model wants
		Call SetNumberofOutputs(2+3*i)      !The number of outputs that the the model produces
		Call SetIterationMode(1)            !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(j,0)  !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0) !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

!=========== VERIFICATION DE LA COMPATIBILITE 

!     TimeStep peut etre un sous-multiple 
    If (TimeStep<1.0) then
      k=0
	IF ((Jfix(TimeStep*1000.))==250)	k=1
	IF ((Jfix(TimeStep*1000.))==500)	k=1

      IF (k==0) then
    	    MessageErr='*** ERREUR dans le Type Moyenne glissante *** &
     &  *** Erreur  TimeStep non sous-multiple de l''heure � 0.25h ou 0.50h'
          
     		Call Messages(-1,trim(MessageErr),'Fatal',CurrentUnit,CurrentType)
	 
      End If
      
      End If

!START doit etre en multiple de 24h.
	Var1=TimeStart/24.
	If (abs(INT(Var1)-Var1)>Epsilon) then
    	    MessageErr='*** ERREUR dans le Type Moyenne glissante *** &
     & *** Erreur  TimeStart doit etre un multiple de 24h';
          Call Messages(-1,trim(MessageErr),'Fatal',CurrentUnit,CurrentType)
	End if
        
		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      ModeCalcul                    = NINT(getParameterValue(1))
      Nconfort                      = NINT(getParameterValue(2))
      BP_Chauffage                  = getParameterValue(3)
      BP_chauffage_dessus           = getParameterValue(4)
      BP_Rafraichissement           = getParameterValue(5)
      BP_Rafraichissement_dessous   = getParameterValue(6)
      NbreThermostats               = NINT(getParameterValue(7))
      TempInit                      = getParameterValue(8)

      Engagement                    = NINT(GetInputValue(1))
      Temp_Ext                      = GetInputValue(2)
      Do i=1, NbreThermostats
        Top(i) = GetInputValue(2+i)
      End Do
      
      Do i=1,7
              Tmjour(i) = TempInit
      End Do
      
      Tmglissante = 0.0
      Do i=1, 7
          Tmglissante = Tmglissante+Coef(i)*TmJour(i)
      End Do
      Tmglissante = Tmglissante/3.8
      
      Do i=1, 24
          Text24h(i) = TempInit
      End Do

      NbreStep=1
      If (TimeStep<0.999999) then
	    IF ((Jfix(TimeStep*1000.))==250)	NbreStep=4
	    IF ((Jfix(TimeStep*1000.))==500)	NbreStep=2
      End If
      
          DO i=1,24
              Call SetStaticArrayValue(i,Text24h(i))
          END DO
          
          Do i=1,7
              Call SetStaticArrayValue(24+i,Tmjour(i))
          End Do
              
          i=24+7 
              Call SetStaticArrayValue(1+i,Tmh)
          
          i = 24+7+1
          Do j=1, NbreThermostats
              Call SetStaticArrayValue(i+j,Top(j))
          End Do
              
   Goto 1000   
      
		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
      ModeCalcul                    = NINT(getParameterValue(1))
      Nconfort                      = NINT(getParameterValue(2))
      BP_Chauffage                  = getParameterValue(3)
      BP_chauffage_dessus           = getParameterValue(4)
      BP_Rafraichissement           = getParameterValue(5)
      BP_Rafraichissement_dessous   = getParameterValue(6)
      NbreThermostats               = NINT(getParameterValue(7))
      TempInit                      = getParameterValue(8)
		
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Engagement                    = NINT(GetInputValue(1))
      Temp_Ext                      = GetInputValue(2)
      Do i=1, NbreThermostats
        Top(i) = GetInputValue(2+i)
      End Do
		

	!Check the Inputs for Problems (#,ErrorType,Text)
	!Sample Code: If( IN1 <= 0.) Call FoundBadInput(1,'Fatal','The first input provided to this model is not acceptable.')
 
      If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
          DO i=1,24
              Text24h(i) = getStaticArrayValue(i)
          END DO
          
          Do i=1,7
              Tmjour(i) = getStaticArrayValue(24+i)
          End Do
              
          i=24+7 
              Tmh = getStaticArrayValue(1+i)
          
          i = i+1
          Do j=1, NbreThermostats
              Topback(j) = getStaticArrayValue(i+j)
          End Do
          
!-----------------------------------------------------------------------------------------------------------------------
      If (getTimestepIteration()>0) goto 400
      
      If (NbreStep==4) then
              Tmh=Tmh+Temp_Ext/4.
      End If
      
       If (NbreStep==2) then
              Tmh=Tmh+Temp_Ext/2.
      End If     
 
      If (NbreStep==1) then
          Tmh=Tmh+Temp_Ext
      End If
      
      If (ABS(Time-NINT(Time))<Epsilon) then      !Heure  enti�re
          Do  i=24,2,-1
               Text24h(i)=Text24h(i-1)
          End do
              Text24h(1)=Tmh
              Tmh = 0.0
      End If 

      Var1=Time/24.                            
      If (ABS(Var1-NINT(Var1))<Epsilon) Then !Changement de jour
          Do i=7,2,-1
              Tmjour(i)=Tmjour(i-1)
          End Do
          TmJour(1) = 0.0
          Do i=1,24
              TmJour(1) = Tmjour(1)+Text24h(i)
          End Do
          TmJour(1) = TmJour(1)/24.
      End If
      
      
      DO i=1,24
        Call SetStaticArrayValue(i,Text24h(i))
      END DO
          
      Do i=1,7
        Call SetStaticArrayValue(24+i,Tmjour(i))
      End Do
              
      i=24+7 
        Call SetStaticArrayValue(1+i,Tmh)     
                
 !---------------------------------------------------------------------------     
400  CONTINUE
      Tmglissante = 0.0
      Do i=1, 7; Tmglissante = Tmglissante+Coef(i)*Tmjour(i); End Do
      Tmglissante = Tmglissante/3.8      
    
      
If (ModeCalcul==0) Then
    Do i=1, NbreThermostats
        TopRetenu(i) = Top(i)
    End Do
Else
    Do i=1, NbreThermostats
        TopRetenu(i) = Topback(i)
    End Do  
End If

If (Engagement==0) Then
    Topconfort = -99.9
    Do i=1, NbreThermostats
        BChauffage(i)       =0.0
        BRafraichissement(i)=0.0
        Variation(i)        =0.0
    End Do
    Goto 1000
End If

If (Engagement==1) Then  !=>Chauffage
      Delta = 1.+ DBLE(Nconfort)
      Temp = Tmglissante
      If (Temp>30.0) Temp=30.0
      If (Temp<15.0) Temp=15.0
      Topconfort = 0.33*Temp+18.8-Delta
    
    Do i=1, NbreThermostats
        Temp2 = Topconfort+BP_chauffage_dessus/100.0*BP_chauffage
        Temp1 = Topconfort-(1.0-BP_chauffage_dessus/100.0)*BP_chauffage
      If (TopRetenu(i)< Temp1) Then
          BChauffage(i) = 100.0
      Else
          If (TopRetenu(i)>Temp2) Then
            BChauffage(i) = 0.0
          Else
              Var1 = 1.0/(Temp1-Temp2)
              BChauffage(i) = (Var1 *(TopRetenu(i)-Temp2))*100.0
          End If
      End If
    End Do  
    
    Do i=1, NbreThermostats
        BRafraichissement(i)=0.0
        Variation(i)        =BChauffage(i)/100.0
    End Do
    Goto 1000
End If

If (Engagement==2) Then  !=>Rafraichissement
      Delta = 1.+ DBLE(Nconfort)
      Temp = Tmglissante
      If (Temp>30.0) Temp=30.0
      If (Temp<10.0) Temp=10.0
      Topconfort  = 0.33*Temp+18.8+Delta
      
     Do i=1, NbreThermostats
        Temp1 = Topconfort-BP_rafraichissement_dessous/100.0*BP_Rafraichissement
        Temp2 = Topconfort+(1.0-BP_rafraichissement_dessous/100.0)*BP_Rafraichissement
      If (TopRetenu(i)> Temp2) Then
          BRafraichissement(i) = 100.0
      Else
          If (TopRetenu(i)<Temp1) Then  
            BRafraichissement(i) = 0.0
          Else
              Var1 = 1.0/(Temp2-Temp1)
              BRafraichissement(i) = (Var1 *(TopRetenu(i)-Temp1))*100.0
          End If
      End If
    End Do  
      
    Do i=1, NbreThermostats
        BChauffage(i)       = 0.0
        Variation(i)        =-1.00*BRafraichissement(i)/100.0
    End Do
    Goto 1000
End If
      
      
!-----------------------------------------------------------------------------------------------------------------------
1000 CONTINUE
!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
		Call SetOutputValue(1, Tmglissante)     ! Temp Trm 
        Call SetOutputValue(2, Topconfort)      ! Top de confort
        
        Do i=1, NbreThermostats
		    Call SetOutputValue(3+3*(i-1), BChauffage(i))       ! Besoin de chauffage
		    Call SetOutputValue(4+3*(i-1), BRafraichissement(i))! Besoin de rafraichissement
		    Call SetOutputValue(5+3*(i-1), Variation(i))        ! Variation des besoins
        End Do
        

 
      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

