!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3500

! Object: Sc�nario annuel
! Simulation Studio Model: Type3500
! 

! Author: DF
! Editor: TEP2E
! Date:	 November 09, 2013
! last modified: November 09, 2013
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Nbre profil	- [1;5]
!			Logical unit	- [60;+Inf]
!			Mode 	- [1;2]
!			Coef A	- [-Inf;+Inf]
!			Coef B	- [-Inf;+Inf]
!			Coef C	- [-Inf;+Inf]
!			Coef D	- [-Inf;+Inf]
!           Ponderation

! *** 
! *** Model Inputs 
! *** 
!			Coef multiplicateur P	- [-Inf;+Inf]

! *** 
! *** Model Outputs 
! *** 
!			N� du jour type	- [-Inf;+Inf]
!			Valeur horaire	- [-Inf;+Inf]
!			Valeur Profil P	- [-Inf;+Inf]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Studio)
!************************************************************************

!-----------------------------------------------------------------------------------------------------------------------
! This TRNSYS component skeleton was generated from the TRNSYS studio based on the user-supplied parameters, inputs, 
! outputs, and derivatives.  The user should check the component formulation carefully and add the content to transform
! the parameters, inputs and derivatives into outputs.  Remember, outputs should be the average value over the timestep
! and not the value at the end of the timestep; although in many models these are exactly the same values.  Refer to 
! existing types for examples of using advanced features inside the model (Formats, Labels etc.)
!-----------------------------------------------------------------------------------------------------------------------


      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type3500

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType
      Integer n_maxProfil
      Integer n_maxType
      Integer nout
      Parameter (n_maxProfil=5, n_maxType=20, nout=2+n_maxProfil)

!    PARAMETERS
      Integer           Nbre_profil
      Integer           Logical_unit
      Integer           Mode(n_maxProfil)
      DOUBLE PRECISION  Coef_A(n_maxProfil)
      DOUBLE PRECISION  Coef_B(n_maxProfil)
      DOUBLE PRECISION  Coef_C(n_maxProfil)
      DOUBLE PRECISION  Coef_D(n_maxProfil)
      Integer           Ponderation(n_maxProfil)
!    INPUTS
      DOUBLE PRECISION  Coef_mult(n_maxProfil)

! OUTPUT
      Integer           JourType
      DOUBLE PRECISION  valeur(0:n_maxProfil)


! Calculs 
      Integer,save ::NType=0
      Integer,save ::NUnit(1:10)
      Integer, save             ::Annee(1:n_maxType,1:365)
      Double precision, save    ::scenario(1:n_maxType,1:9,1:24)
      Double precision, save    ::Coef_Pond(1:n_maxType,1:53)
      Double precision Time_1, time_2
      Double precision Var1, Var2, Var3, Var4

      Integer lu, Luw                    !the logical unit for the list file
      CHARACTER (len=maxLabelLength) Fichierscenario
      Character(LEN=MaxMessageLength) Message1,Message2
      Character(LEN=80) Descriptif
      Integer   heure, jour, semaine
      Integer   i, j, y
       
      Data Message1 /'Probleme concernant le fichier scenario'/
      Data Message2 /'End of file encountered'/

!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
      luw=getListingFileLogicalUnit()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then
        Ntype=Ntype+1
        Nunit(Ntype)=CurrentUnit

        Nbre_profil   = Nint(getParameterValue(1))
		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(2+6*Nbre_profil)           !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)                          !The number of derivatives that the the model wants
		Call SetNumberofOutputs(2+Nbre_profil)                  !The number of outputs that the the model produces
		Call SetIterationMode(1)                                !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)                      !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)                     !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      Nbre_profil   = Nint(getParameterValue(1))
      Logical_unit  = Nint(getParameterValue(2))
      y=2
      Do i=1, Nbre_profil
        Mode(i)   = NINT(getParameterValue(y+1))
        Coef_A(i) = getParameterValue(y+2)
        Coef_B(i) = getParameterValue(y+3)
        Coef_C(i) = getParameterValue(y+4)
        Coef_D(i) = getParameterValue(y+5)
        Ponderation(i) = NINT(getParameterValue(y+6))
        y=y+6
        Coef_mult(i)= GetInputValue(i)
      End do
     
      Do i=1, Nbre_profil
          If ((Ponderation(i)/=100).and.(Ponderation(i)/=101).and.(Ponderation(i)/=110).and.(Ponderation(i)/=111)) then
              Call foundBadParameter(1,"FATAL","PAR() Probleme ponderation valeur obligatoire 100, 101, 110 ou 111")
          End If
      End Do
      
      

      Do i=1, n_maxType
	  if (NUnit(i)==CurrentUnit) EXIT
      end do
	NType=i

      !Lecture du fichier sc�nario
      LU=Logical_unit
      Fichierscenario=getLUfileName(LU)
      Open(LU,File=Fichierscenario,err=4000)
        REWIND(LU)
        Do i=1,6
            Read(LU,*,err=4000,end=4000)
        End do

        Do i=1,9
            Read(LU,*,err=4000,end=4000) Descriptif, (Scenario(Ntype,i,y),y=1,24)
        End do

        Read(LU,*,err=4000,end=4000)

        Do i=1,53
            Read(LU,*,err=4000,end=4000) Annee(Ntype,i), Coef_Pond(Ntype,i)
        End do
        Do i=54,365
            Read(LU,*,err=4000,end=4000) Annee(Ntype,i)
        End do
      Close (LU)
	

      !Check the Parameters for Problems (#,ErrorType,Text)
      !Sample Code: If( PAR1 <= 0.) Call FoundBadParameter(1,'Fatal','The first parameter provided to this model is not acceptable.')

      !Set the Initial Values of the Outputs (#,Value)
        Goto 2000

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
      Nbre_profil   = Nint(getParameterValue(1))
      Logical_unit  = Nint(getParameterValue(2))
      y=2
      Do i=1, Nbre_profil
        Mode(i)   = NINT(getParameterValue(y+1))
        Coef_A(i) = getParameterValue(y+2)
        Coef_B(i) = getParameterValue(y+3)
        Coef_C(i) = getParameterValue(y+4)
        Coef_D(i) = getParameterValue(y+5)
        Ponderation(i) = NINT(getParameterValue(y+6))
        y=y+6
      End do

      Do i=1, n_maxType
	  if (NUnit(i)==CurrentUnit) EXIT
      end do
	NType=i

		
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Do i=1, Nbre_profil
        Coef_mult(i)= GetInputValue(i)
      End do

      Do i=1, n_maxType
	  if (NUnit(i)==CurrentUnit) EXIT
      end do
	NType=i		

	!Check the Inputs for Problems (#,ErrorType,Text)
	!Sample Code: If( IN1 <= 0.) Call FoundBadInput(1,'Fatal','The first input provided to this model is not acceptable.')
 
      If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------


	!-----------------------------------------------------------------------------------------------------------------------
	!Perform All of the Calculations Here to Set the Outputs from the Model Based on the Inputs
2000  CONTINUE
      Time_1        =Time+8760.0-Timestep/2.
      Time_2        =DMOD(Time_1,8760.d0)
      heure         =Jfix(DMOD(Time_1,24.d0))+1         !Heure 1 � 24
      jour          =jfix(Time_2/24.d0)+1               !N� du jour 1 � 365
      Semaine       =NINT(Time_2/168.0)+1               !N� semaine 1 � 53

      JourType  = Annee(Ntype,jour)
      Var1      = scenario(Ntype,JourType,heure)
      Valeur(0) =Var1
      Var2      =Var1     

      Do i=1, Nbre_profil
        If ((Ponderation(i)==111).or.(Ponderation(i)==110)) then
            Var2=Var1*Coef_Pond(Ntype,semaine)
          End If
        If ((Ponderation(i)==111).or.(Ponderation(i)==101)) then
          Var2=Var2*Coef_mult(i)
        End If
      
        If (Mode(i)==1) then
            Var3=Coef_A(i)*Var2+Coef_B(i)
            If (Var3>Coef_D(i)) Var3=Coef_D(i)
            If (Var3<Coef_C(i)) Var3=Coef_C(i)

        Else
            Var3=Coef_B(i)
            If (Var2<-99.d0)  Var3=Coef_A(i)
            If (Var2> 99.d0)  var3=Coef_C(i)
        End if
        Valeur(i)=Var3     
      End do


!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
      
		Call SetOutputValue(1, DBLE(JourType))      ! N� du jour type
          Call SetOutputValue(2, Valeur(0))           ! Valeur Profil horaire        
        Do i=1,Nbre_Profil
		    Call SetOutputValue(2+i, Valeur(i))     ! Valeur Profil calcule
        End do
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
       goto 4010
 4000  Call MESSAGES(-1,Message1,'FATAL',CurrentUnit,CurrentType)
      Return
 4010 Continue
      End subroutine  
!-----------------------------------------------------------------------------------------------------------------------
