!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


			SUBROUTINE TYPE3505 (TIME,XIN,OUT,T,DTDT,PAR,INFO,ICNTRL,*) 
C************************************************************************
C Object: G�n�rateur de Profil d' ECS
C IISiBat Model: Type806
C 
C Author: PERRET A. et FOUQUET D.
C Editor: 
C Date:	 15/03/2003 last modified: 05/11/2004
C 
C Profil ECS Principe AICVF
C *** 
C *** Model Parameters 
C *** 
C			Unit� logique des fichiers de coef		any [-Inf;+Inf]
C			Profil des conso. du profil n�1			any [1;20]
C			Consommation annuelle du profil n�1		m^3 [0;+Inf]
C			Somme des coef pour le profil n�1		any [-Inf;+Inf]
C			Profil des conso. du profil n�2			any [1;20]
C			Consommation annuelle du profil n�2		m^3 [0;+Inf]
C			Somme des coef pour le profil n�2		any [-Inf;+Inf]
C			Profil des conso. du profil n�3			any [1;20]
C			Consommation annuelle du profil n�3		m^3 [0;+Inf]
C			Somme des coef pour le profil n�3		any [-Inf;+Inf]
C			Profil des conso. du profil n�4			any [1;20]
C			Consommation annuelle du profil n�4		m^3 [0;+Inf]
C			Somme des coef pour le profil n�4		any [-Inf;+Inf]
C			Profil des conso. du profil n�5			any [1;20]
C			Consommation annuelle du profil n�5		m^3 [0;+Inf]
C			Somme des coef pour le profil n�5		any [-Inf;+Inf]

C *** 
C *** Model Inputs 
C *** 
C			Code du temps l�gal	any [0;0]
C			Contr�leur de fonction du profil n�1	any [0;1]
C			Contr�leur de fonction du profil n�2	any [0;1]
C			Contr�leur de fonction du profil n�3	any [0;1]
C			Contr�leur de fonction du profil n�4	any [0;1]
C			Contr�leur de fonction du profil n�5	any [0;1]
C			Temperature_EF							C [0;150]

C *** 
C *** Model Outputs 
C *** 
C			D�bit horaire instantan� au temps TIME (profil 1)		kg/hr [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 1)		m^3/hr [0;+Inf]
C			Consommation depuis START (profil 1)					m^3 [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 2)		kg/hr [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 2)		m^3/hr [0;+Inf]
C			Consommation depuis START (profil 2)					m^3 [0;+Inf
C			D�bit horaire instantan� au temps TIME (profil 3)		kg/hr [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 3)		m^3/hr [0;+Inf]
C			Consommation depuis START (profil 3)					m^3 [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 4)		kg/hr [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 4)		m^3/hr [0;+Inf]
C			Consommation depuis START (profil 4)					m^3 [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 5)		kg/hr [0;+Inf]
C			D�bit horaire instantan� au temps TIME (profil 5)		m^3/hr [0;+Inf]
C			Consommation depuis START (profil 5)					m^3 [0;+Inf]


C *** 
C *** Model Derivatives 
C *** 
C============================== Mise � jour =============================
C         
C     N�1 Le 10 Juin 2003 par Didier FOUQUET
C     *** Contr�le de STEP, START et STOP
C	N�2 Le 29 Juin 2003 par Didier FOUQUET
C	*** Cr�ation d'une sortie en kg/h et m3/h en fonction de Temp EF
C	N�3 Le 29 Juin 2003 par Didier FOUQUET
C	*** Prise de compte de la conversion des unit�s
C     N�4 Le 4 Janvier 2004 par Didier FOUQUET
C     *** Limitation du calcul � un STEP >0.015 
C	N�5 Le 5 Novembre 2004 par Didier FOUQUET
C     *** Passage de la Version 15 � la Version 16


C (Comments and routine interface generated by TRNSYS Studio)
C************************************************************************

C    TRNSYS acess functions (allow to acess TIME etc.) 
      USE TrnsysConstants
      USE TrnsysFunctions

C-----------------------------------------------------------------------------------------------------------------------
C    REQUIRED BY THE MULTI-DLL VERSION OF TRNSYS
      !DEC$ATTRIBUTES DLLEXPORT :: TYPE3505				!SET THE CORRECT TYPE NUMBER HERE
C-----------------------------------------------------------------------------------------------------------------------
C-----------------------------------------------------------------------------------------------------------------------
C    TRNSYS DECLARATIONS
      IMPLICIT NONE			!REQUIRES THE USER TO DEFINE ALL VARIABLES BEFORE USING THEM

	DOUBLE PRECISION XIN	!THE ARRAY FROM WHICH THE INPUTS TO THIS TYPE WILL BE RETRIEVED
	DOUBLE PRECISION OUT	!THE ARRAY WHICH WILL BE USED TO STORE THE OUTPUTS FROM THIS TYPE
	DOUBLE PRECISION TIME	!THE CURRENT SIMULATION TIME - YOU MAY USE THIS VARIABLE BUT DO NOT SET IT!
	DOUBLE PRECISION PAR	!THE ARRAY FROM WHICH THE PARAMETERS FOR THIS TYPE WILL BE RETRIEVED
	DOUBLE PRECISION STORED !THE STORAGE ARRAY FOR HOLDING VARIABLES FROM TIMESTEP TO TIMESTEP
	DOUBLE PRECISION T		!AN ARRAY CONTAINING THE RESULTS FROM THE DIFFERENTIAL EQUATION SOLVER
	DOUBLE PRECISION DTDT	!AN ARRAY CONTAINING THE DERIVATIVES TO BE PASSED TO THE DIFF.EQ. SOLVER
	INTEGER*4 INFO(15)		!THE INFO ARRAY STORES AND PASSES VALUABLE INFORMATION TO AND FROM THIS TYPE
	INTEGER*4 NP,NI,NOUT,ND	!VARIABLES FOR THE MAXIMUM NUMBER OF PARAMETERS,INPUTS,OUTPUTS AND DERIVATIVES
	INTEGER*4 NPAR,NIN,NDER	!VARIABLES FOR THE CORRECT NUMBER OF PARAMETERS,INPUTS,OUTPUTS AND DERIVATIVES
	INTEGER*4 IUNIT,ITYPE	!THE UNIT NUMBER AND TYPE NUMBER FOR THIS COMPONENT
	INTEGER*4 ICNTRL		!AN ARRAY FOR HOLDING VALUES OF CONTROL FUNCTIONS WITH THE NEW SOLVER
	INTEGER*4 NSTORED		!THE NUMBER OF VARIABLES THAT WILL BE PASSED INTO AND OUT OF STORAGE
	CHARACTER*3 OCHECK		!AN ARRAY TO BE FILLED WITH THE CORRECT VARIABLE TYPES FOR THE OUTPUTS
	CHARACTER*3 YCHECK		!AN ARRAY TO BE FILLED WITH THE CORRECT VARIABLE TYPES FOR THE INPUTS
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    USER DECLARATIONS - SET THE MAXIMUM NUMBER OF PARAMETERS (NP), INPUTS (NI),
C    OUTPUTS (NOUT), AND DERIVATIVES (ND) THAT MAY BE SUPPLIED FOR THIS TYPE
      PARAMETER (NP=17,NI=7,NOUT=15,ND=0,NSTORED=0)
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    REQUIRED TRNSYS DIMENSIONS
      DIMENSION XIN(NI),OUT(NOUT),PAR(NP),YCHECK(NI),OCHECK(NOUT),
	1   STORED(NSTORED),T(ND),DTDT(ND)
      INTEGER NITEMS
C-----------------------------------------------------------------------------------------------------------------------
C-----------------------------------------------------------------------------------------------------------------------
C    ADD DECLARATIONS AND DEFINITIONS FOR THE USER-VARIABLES HERE
C    Nombre max TYPE 806 par simulation
	INTEGER NbTYPE
	INTEGER NmaxMAX
	PARAMETER NmaxTYPE=1

C    PARAMETERS
	INTEGER					NumProfil(5)		!N� du profil
	DOUBLE PRECISION		ConsoAnProfil(5)	!Consommation annuelle  
	DOUBLE PRECISION		CoefAnnuel(5)		!Coef de passage Annuel en horaire de r�f

C    INPUTS
	INTEGER					Controleur(5)		!Controle du d�bit
	DOUBLE PRECISION        TempEF				!Temp�rature Ef de r�f�rence

C
C     Variables pour le calcul	
	LOGICAL       exists			!existences d'un fichier

	INTEGER		  NumSType(5,12)	!R�partition mensuelles des									        
	INTEGER       NumJType(5,8,7)	!R�partition hebdomadaire des jours types


	INTEGER		NumCycle		!n� du cycle de base, [1,5]
	INTEGER		NumTran			!n� d'une tranche horaire de 12 mn
	INTEGER     CodeDate(9)     !Information sur la date
	INTEGER     NSemType		!n� d'une semaine type
	INTEGER     NJourType		!n� d'un jour type
	INTEGER		LUi,NumJourSem,NumMois,MinJour,HreJour
	INTEGER		ProfilCyclique,ModeI
	INTEGER		i,j,k,n,A1,A2,A3,AA
	
	CHARACTER*460 MessageErr	!message d'erreur
	CHARACTER*460 MessageErr1	!message d'erreur
	CHARACTER*100 FICHIER		!POUR TESTER le TYPE
      CHARACTER (len=maxLabelLength) FichierECS


	DOUBLE PRECISION		ChaineTmp,A,B,CycleBase(6,5)
	DOUBLE PRECISION		Valeur,C1,C2,C3,RO_eau
	DOUBLE PRECISION		CoefCours		!Coef de pond. � TIME
	DOUBLE PRECISION		CoefMens(5,12)		!Coef. de pond�ration mensuels
	DOUBLE PRECISION        CoefHebdo(5,8,7)	!Coef. hebdo. de pond�ration
	DOUBLE PRECISION        CoefHre(5,8,24)		!Coef. horaires de pond�ration
	DOUBLE PRECISION        ConsoHre(5)			!d�bit horaire r�f (m3/h) 
	DOUBLE PRECISION		CoefHrePrec(5)		!coef de pond. du pas pr�c�dent
	DOUBLE PRECISION		CoefPondDeb(5)		!coef de pond. de l'hre de d�but
	DOUBLE PRECISION		DebitRef(5)			!D�bit horaire de r�f�rence   
	DOUBLE PRECISION		CumulConso(5)		!consommation depuis START
	DOUBLE PRECISION		CoefA_P(5)			!Coef de passage Annuel en horaire provisoire
	DOUBLE PRECISION		CoefCycle(5,5)		!Coef. du cycle de base
	DOUBLE PRECISION		DebitH		        !d�bit horaire instantan�
	DOUBLE PRECISION		TSTART, TSTOP, STEP
	DOUBLE PRECISION		Epsilon
      INTEGER                 Nbrlog
      DOUBLE PRECISION        Amplitude, Var1
      INTEGER                 Hj, Jour, JP  !1=Impaire  0=paire



	SAVE NbTYPE,CoefCycle,CoefMens,CoefHebdo,CoefHre,
     &      NumStype,NumJType

	DATA Epsilon/1.0D-20/
	DATA NbTYPE/0/
      DATA CumulConso/5*0/

	TSTART	=getSimulationStartTime()
	TSTOP	=getSimulationStopTime()
	STEP	=getSimulationTimeStep()


C-----------------------------------------------------------------------------------------------------------------------
C       READ IN THE VALUES OF THE PARAMETERS IN SEQUENTIAL ORDER

C-----------------------------------------------------------------------------------------------------------------------
C    RETRIEVE THE CURRENT VALUES OF THE INPUTS TO THIS MODEL FROM THE XIN ARRAY IN SEQUENTIAL ORDER

	   IUNIT=INFO(1)
	   ITYPE=INFO(2)

C-----------------------------------------------------------------------------------------------------------------------
C    SET THE VERSION INFORMATION FOR TRNSYS
      IF(INFO(7).EQ.-2) THEN
	   INFO(12)=16

	   RETURN 1
	ENDIF
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    DO ALL THE VERY LAST CALL OF THE SIMULATION MANIPULATIONS HERE
      IF (INFO(8).EQ.-1) THEN
	   RETURN 1
	ENDIF
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    PERFORM ANY 'AFTER-ITERATION' MANIPULATIONS THAT ARE REQUIRED HERE
C    e.g. save variables to storage array for the next timestep
      IF (INFO(13).GT.0) THEN
	   NITEMS=0
C	   STORED(1)=... (if NITEMS > 0)
C        CALL SET_STORAGE_VARS(STORED,NITEMS,INFO)
	   RETURN 1
	ENDIF
C
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    DO ALL THE VERY FIRST CALL OF THE SIMULATION MANIPULATIONS HERE
      IF (INFO(7).EQ.-1) THEN

C       SET SOME INFO ARRAY VARIABLES TO TELL THE TRNSYS ENGINE HOW THIS TYPE IS TO WORK
         INFO(6)=NOUT				
         INFO(9)=1				
	   INFO(10)=0	!STORAGE FOR VERSION 16 HAS BEEN CHANGED				

C       SET THE REQUIRED NUMBER OF INPUTS, PARAMETERS AND DERIVATIVES THAT THE USER SHOULD SUPPLY IN THE INPUT FILE
C       IN SOME CASES, THE NUMBER OF VARIABLES MAY DEPEND ON THE VALUE OF PARAMETERS TO THIS MODEL....
         NIN=NI
	   NPAR=NP
	   NDER=ND
	       
C       CALL THE TYPE CHECK SUBROUTINE TO COMPARE WHAT THIS COMPONENT REQUIRES TO WHAT IS SUPPLIED IN 
C       THE TRNSYS INPUT FILE
	   CALL TYPECK(1,INFO,NIN,NPAR,NDER)

C       SET THE NUMBER OF STORAGE SPOTS NEEDED FOR THIS COMPONENT
         NITEMS=0
C	   CALL SET_STORAGE_SIZE(NITEMS,INFO)



C	***************Contr�le des param�tres ************************
		NbTYPE=NbTYPE+1
		
	IF (NbTYPE.GT.NmaxTYPE) THEN
    	 MessageErr='******ERREUR dans le TYPE Profil ECS ****   
     & Attention un TYPE PROFIL ECS par simulation'		
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
	ENDIF

C----------------------------------------------------------------------
C       VERIFICATION DE LA COMPATIBILITE DE STEP
C       STEP doit etre un sous-multiple de l'heure 
C       STEP doit etre inferieur ou �gal � 1h
C       STEP doit etre un sup�rieur � 0.01h 
C
		A=1./STEP; k=0

	IF ((Jfix(STEP*100.))== 1) k=1
	IF ((Jfix(STEP*100.))== 5) k=1
	IF ((Jfix(STEP*100.))==10) k=1
	IF ((Jfix(STEP*100.))==20) k=1
	IF ((Jfix(STEP*100.))==25) k=1
	IF ((Jfix(STEP*100.))==50) k=1
	IF ((jfix(STEP*100.))==100) k=1
	  
	IF (STEP<0.01) then
      MessageErr='*** ERREUR  Type G�n�rateur de profil ECS ****
     & Erreur  STEP inf�rieure � 0.01heure'
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
	End If

	IF ((STEP-1.)>Epsilon) then
     	MessageErr='*** ERREUR  Type G�n�rateur de profil ECS
     & Erreur  STEP sup�rieur � l''heure'
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
       Else
	  IF (k==0) then
     	MessageErr='*** ERREUR Type G�n�rateur de profil ECS***
     & Erreur  STEP non sous-multiple de l''heure choisir entre les steps suivants
     &        0.01h 0.05h, 0.10 , 0.20 , 0.25 ou 0,50h '
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
		End if
	End If
       
	 A=TSTART
	 IF (abs(ANINT(A)-A)>Epsilon) then
     	MessageErr='*** ERREUR Type G�n�rateur de profil ECS***
     & Erreur  START non multiple de l''heure'
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
	  End if
	 
	 A=TSTOP
       IF (abs(ANINT(A)-A)>Epsilon) then
     	MessageErr='*** ERREUR Type G�n�rateur de profil ECS***
     & Erreur  STOP non multiple de l''heure'
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
       End if


	LUi=jfix(PAR(1)+0.01)
	
	MessageErr1=" "

	! Lire les Coef de Cycle des TOP 12 mn
	CALL LireCoefCycle(LUi,CoefCycle,MessageErr1)

	IF (MessageErr1 /= '') THEN
	MessageErr='**** Erreur dans G�n�rateur de profil d''ECS ***
     &  Concernant le fichier txt des Coef de pond�ration  '//MessageErr1
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
	END IF

c	!Initialisation des coef. de pond�ration 
	DO i=1,5 !Profil 1 � 5
		n=3+(i-1)*3
		IF (jfix(PAR(n)+0.01).NE.0) THEN
				NumProfil(i) = jfix(PAR(n)+0.01)

			 CALL LireProfil(LUi,i,NumProfil,NumSType,NumJType
     &                 ,CoefMens,CoefHebdo,CoefHre,CoefA_P,MessageErr)
			 
			IF (MessageErr1 /= '') THEN
	MessageErr='***Erreur dans G�n�rateur de profil d''ECS ***
     & Concernant le profil n�'//ACHAR(i)
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))
			END IF
c			
		END IF
		CumulConso(i)=0.0
	END DO


	
C       RETURN TO THE CALLING PROGRAM
         RETURN 1

      ENDIF
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    DO ALL OF THE INITIAL TIMESTEP MANIPULATIONS HERE - THERE ARE NO ITERATIONS AT THE INTIAL TIME
      IF (TIME .LT. (getSimulationStartTime() +
     . getSimulationTimeStep()/2.D0)) THEN

C       SET THE UNIT NUMBER FOR FUTURE CALLS
         IUNIT=INFO(1)
         ITYPE=INFO(2)

C       CHECK THE PARAMETERS FOR PROBLEMS AND RETURN FROM THE SUBROUTINE IF AN ERROR IS FOUND
C         IF(...) CALL TYPECK(-4,INFO,0,"BAD PARAMETER #",0)

C       PERFORM ANY REQUIRED CALCULATIONS TO SET THE INITIAL VALUES OF THE OUTPUTS HERE
C		 D�bit kg/h instantan� au temps TIME (profil 1)
			OUT(1)=0.0
C		 D�bit m3/h instantan� au temps TIME(profil 1)
			OUT(2)=0.0
C		 Consommation m3 depuis START (profil 1)
			OUT(3)=0.0
C		 D�bit kg/h instantan� au temps TIME (profil 2)
			OUT(4)=0.0
C		 D�bit m3/h instantan� au temps TIME(profil 2)
			OUT(5)=0.0
C		 Consommation depuis START (profil 2)
			OUT(6)=0.0
C		 D�bit kg/h instantan� au temps TIME (profil 3)
			OUT(7)=0.0
C		 D�bit m3/h instantan� au temps TIME(profil 3)
			OUT(8)=0.0
C		 Consommation depuis START (profil 3)
			OUT(9)=0.0
C		 D�bit kg/h instantan� au temps TIME (profil 4)
			OUT(10)=0.0
C		 D�bit m3/h instantan� au temps TIME(profil 4)
			OUT(11)=0.0
C		 Consommation depuis START (profil 4)
			OUT(12)=0.0
C		 D�bit kg/h instantan� au temps TIME (profil 5)
			OUT(13)=0.0
C		 D�bit m3/h instantan� au temps TIME(profil 5)
			OUT(14)=0.0
C		 Consommation depuis START (profil 5)
			OUT(15)=0.0

C       PERFORM ANY REQUIRED CALCULATIONS TO SET THE INITIAL STORAGE VARIABLES HERE
         NITEMS=0
C	   STORED(1)=...

C       PUT THE STORED ARRAY IN THE GLOBAL STORED ARRAY
C         CALL SET_STORAGE_VARS(STORED,NITEMS,INFO)

C       RETURN TO THE CALLING PROGRAM
         RETURN 1

      ENDIF
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    *** ITS AN ITERATIVE CALL TO THIS COMPONENT ***
C-----------------------------------------------------------------------------------------------------------------------

	    
C-----------------------------------------------------------------------------------------------------------------------
C    RETRIEVE THE VALUES IN THE STORAGE ARRAY FOR THIS ITERATION
C      NITEMS=
C	CALL GET_STORAGE_VARS(STORED,NITEMS,INFO)
C      STORED(1)=
C-----------------------------------------------------------------------------------------------------------------------
C-----------------------------------------------------------------------------------------------------------------------
C    CHECK THE INPUTS FOR PROBLEMS
C      IF(...) CALL TYPECK(-3,INFO,'BAD INPUT #',0,0)
C	IF(IERROR.GT.0) RETURN 1
C-----------------------------------------------------------------------------------------------------------------------
C-----------------------------------------------------------------------------------------------------------------------
C    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
C-----------------------------------------------------------------------------------------------------------------------

C		ADD YOUR COMPONENT EQUATIONS HERE; BASICALLY THE EQUATIONS THAT WILL
C		CALCULATE THE OUTPUTS BASED ON THE PARAMETERS AND THE INPUTS.	REFER TO
C		CHAPTER 3 OF THE TRNSYS VOLUME 1 MANUAL FOR DETAILED INFORMATION ON
C		WRITING TRNSYS COMPONENTS.

	    LUi=jfix(PAR(1)+0.01)


		ModeI=PAR(2)  !PAS d'action pour les Coef AICVF
		
		DO i=1,5
		  j=3+(i-1)*3
			NumProfil(i)      = jfix(PAR(j)+0.01)
			ConsoAnProfil(i)  = PAR(j+1)
			CoefAnnuel(i)     = PAR(j+2)
			Controleur(i)     = jfix(XIN(1+i)+0.01)
			IF (CoefAnnuel(i).EQ.-1) CoefAnnuel(i)=CoefA_P(i)
			DebitRef(i)		  = ConsoAnProfil(i)/CoefAnnuel(i) 
		END DO
			TempEF				=XIN(7)
c
		If (ABS(XIN(1))<Epsilon) then
	MessageErr='**** Erreur dans G�n�rateur de profil d''ECS ***
     &  Probl�me de liaison avec le TYPE Calendrier Fran�ais  
     &  Soit pas de raccordement du code temps ---  Controler que  
     &  le TYPE calendrier est bien avant le TYPE 806'
			CALL Messages(-1,trim(MessageErr),'Fatal',INFO(1),INFO(2))			
		Return 1
		ENDIF
c
C	GET THE VALUES OF THE INPUTS TO THIS COMPONENT
		Call CodeCalendrier(XIN(1),CodeDate)
c		
		HreJour     = CodeDate(5)
		MinJour     = CodeDate(6)
		NumJourSem  = CodeDate(8)
		NumMois     = CodeDate(2)
		


C-----------------------------------Partie Principale
	DO i=1,5
	 IF (NumProfil(i) == 0) goto 110
		! Coef de pond�ration horaire
		A1	=NumStype(i,NumMois)		!Numerot semaine Type du mois
		A2	=NumJType(i,A1,NumJourSem)  !Numerot jour type de la semaine	
c
		C1	=CoefMens(i,NumMois)
c	    
		C2	=CoefHebdo(i,A1,NumJourSem)
		
		IF (STEP==1) THEN
			IF (Hrejour==0) then
				AA=24; Hj=24
				Else
				AA=Hrejour; Hj=Hrejour
			END IF	
			C3	=CoefHre(i,A2,(AA))
			ELSE 
			C3	=CoefHre(i,A2,(HreJour+1))
              Hj=HreJour+1
              END IF
              

          Amplitude = 1.0
          If (ModeI==1) then
              Jour = INT(Time/24.)+1
              JP=1; If (MODULO(Jour,2)==0) JP=0
              Nbrlog = NINT((ConsoAnProfil(i)*CoefAnnuel(i)/8765.0/30.0)+0.5)
              If (Nbrlog>300) Nbrlog=300
              Var1 = (0.17+(Nbrlog-1)**-0.5)/0.223
              If (Var1<1.0) Var1 =1
              
              If (Var1<1.5) then
                  If (JP==0) then
                  Select case (Hj)
                      Case (1,3,5,7,9,11,13,15,17,19,21,23)    ;Amplitude = Var1
                      Case (2,4,6,8,10,12,14,16,18,20,22,24)   ;Amplitude = 2.0-Var1
                      End select
                  Else
                  Select case (Hj)
                      Case (1,3,5,7,9,11,13,15,17,19,21,23)     ;Amplitude = 2.0-Var1
                      Case (2,4,6,8,10,12,14,16,18,20,22,24)    ;Amplitude = Var1
                      End select
                  End if
              Else
                  If (Var1>2.8) Var1=2.8
                  If (JP==0) then
                  Select case (Hj)
                      Case (1,4,7,10,13,16,19,22)             ;Amplitude = (3.0-Var1)/2.
                      Case (2,5,8,11,14,17,20,23)             ;Amplitude = (3.0-Var1)/2.
                      Case (3,6,9,12,15,18,21,24)             ;Amplitude = Var1
                      End select
                  Else
                  Select case (Hj)
                      Case (1,4,7,10,13,16,19,22)             ;Amplitude = (3.0-Var1)/2.
                      Case (2,5,8,11,14,17,20,23)             ;Amplitude = Var1
                      Case (3,6,9,12,15,18,21,24)             ;Amplitude = (3.0-Var1)/2.
                  End select
                  End if
             End If
          End if
          
		!Amplitude =1.
          CoefCours=C1*C2*C3*Amplitude
		
		Valeur=1
	    Call IndiceTranche(TIME,STEP,CoefCycle,Valeur)
      

		DebitH=CoefCours*Valeur*Controleur(i)*DebitRef(i)

		IF (TIME>TSTART) THEN
			CumulConso(i)=CumulConso(i)+DebitH*STEP
		END IF

		Ro_eau=1000-0.0036*TempEF*TempEF-0.0687*TempEF

		j		=1+3*(i-1)
		OUT(j)	=DebitH*Ro_eau	
		OUT(j+1)=DebitH
		OUT(j+2)=CumulConso(i)
110		CONTINUE
	END DO
		
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C-----------------------------------------------------------------------------------------------------------------------
C    SET THE STORAGE ARRAY AT THE END OF THIS ITERATION IF NECESSARY
C      NITEMS=
C      STORED(1)=
C	CALL SET_STORAGE_VARS(STORED,NITEMS,INFO)
C-----------------------------------------------------------------------------------------------------------------------
C-----------------------------------------------------------------------------------------------------------------------
C    REPORT ANY PROBLEMS THAT HAVE BEEN FOUND USING CALLS LIKE THIS:
C      CALL MESSAGES(-1,'put your message here','MESSAGE',IUNIT,ITYPE)
C      CALL MESSAGES(-1,'put your message here','WARNING',IUNIT,ITYPE)
C      CALL MESSAGES(-1,'put your message here','SEVERE',IUNIT,ITYPE)
C      CALL MESSAGES(-1,'put your message here','FATAL',IUNIT,ITYPE)
C-----------------------------------------------------------------------------------------------------------------------
C-----------------------------------------------------------------------------------------------------------------------

C-----------------------------------------------------------------------------------------------------------------------
C    EVERYTHING IS DONE - RETURN FROM THIS SUBROUTINE AND MOVE ON
      RETURN 1
      END
C-----------------------------------------------------------------------------------------------------------------------



C*******************************************************************************
C
C						   SUBROUTINE POUR TYPE806 
C
C*******************************************************************************

	SUBROUTINE LireCoefCycle(LUi,CoefCycle,MessErr)
	!Permet de lire les coefficients du cycle de base,
	!dans le fichier texte des coefficients (d�bit de pointe).
      USE TrnsysConstants
      USE TrnsysFunctions

		IMPLICIT NONE
		DOUBLE PRECISION	  CoefCycle(5,5) 
		DOUBLE PRECISION	  A		
		CHARACTER*160         MessErr				!message d'erreur
        CHARACTER (len=maxLabelLength) FichierECS
	  INTEGER LUi,i,j,k 

		MessErr = ' '
		FichierECS=getLUfileName(Lui)
        OPEN (Lui,File=FichierECS)
        REWIND(LUi)
		DO i=1,5
			READ(LUi,'(5F4.1)',END=2001,ERR=2002) (CoefCycle(i,j),j=1,5)
		END DO
		
		!Controle que la somme des coef par cycle = 5
		A=0
		DO j=1,5
			Do i=1,5
				A=A+CoefCycle(i,j)
			End Do
		End Do

		
		If (ABS(A-25)>1E-8) goto 2003			
	
		GOTO 2050
C		Traitements des erreurs
2001		MessErr = 'Erreur 2001 de fin de fichier
     . (coef. du cycle de base).'
		GOTO 2050
2002		MessErr = 'Erreur 2002 de lecture de fichier
     . (coef. du cycle de base).'
		GOTO 2050
2003		MessErr = 'Erreur 2003 somme des co�f n''est pas egal � 5'

2050	END




	SUBROUTINE LireProfil(LUi,k,NumP,NumSType,CoefJour
     &                      ,CoefMens,CoefHebdo,CoefHre,CoefA_P,MessErr)
		!Permet de lire les coefficients de pond�ration d'un profil,
		!� partir du ficher texte des coefficients.
      USE TrnsysConstants
      USE TrnsysFunctions

		IMPLICIT NONE
		INTEGER       NumP(5)      		 !n� du profil � lire
		INTEGER  	  NumSType(5,12)	 !r�partition mensuelle des semaines 
		INTEGER  	  CoefJour(5,8,7)	 !r�patition hebdomadaire des jours 
	    INTEGER		  LUi,k,i,j,A

		DOUBLE PRECISION		  CoefMens(5,12)	 !coef. de pond�ration mensuels
		DOUBLE PRECISION          CoefHebdo(5,8,7)   !coef. de pond�ration hebdomadaires
		DOUBLE PRECISION		  CoefHre(5,8,24)	 !coef. de pond�ration horaires
	    DOUBLE PRECISION          STEP               !pas de pas du calcul
		DOUBLE PRECISION          CoefA_P(5)         !Coef Provisoire de passage Qan en Qh
		DOUBLE PRECISION		  V(4)	
		
		CHARACTER*100 MessErr		     !message d'erreur
        CHARACTER (len=maxLabelLength) FichierECS
		
		
		MessErr = ''
        FichierECS=getLUfileName(Lui)
        OPEN (Lui,File=FichierECS)

		REWIND(LUi) 
		!Positionnement sur la premi�re ligne du profil
		DO I=1,(5+(NumP(k)-1)*29)
		  READ(LUi,*)
		END DO

		READ (LUi,*)			!---------------
		READ (LUi,*)			!Nom du profil
		
		READ (Lui,*) CoefA_P(k)

		!Caract�ristiqeus mensuelles
		READ(LUi,*,END=3001,ERR=3002) (NumSType(k,I),I=1,12)
		READ(LUi,*,END=3003,ERR=3004) (CoefMens(k,I),I=1,12)
		!Caract�ristiques hebdomadaires
		DO I=1,8
			READ(LUi,*,END=3005,ERR=3006)  (CoefJour(k,I,J),J=1,7)	
			READ(LUi,*,END=3007,ERR=3008) (CoefHebdo(k,I,J),J=1,7)
		END DO

		!Caract�ristiques horaires
		DO I=1,8
			READ(LUi,*,END=3009,ERR=3010) (CoefHre(k,I,J),J=1,24)
		END DO
		GOTO 3050
!Traitements des erreurs
3001		MessErr = 'Erreur 3001 de fin de fichier
     .              (r�partition semaine type)'
		GOTO 3050
3002		MessErr = 'Erreur 3002 de lecture de fichier
     .			  (r�partition semaine type)'
		GOTO 3050
3003		MessErr = 'Erreur 3003 de fin de fichier
     .			  (r�partition mensuelle des coef)'
		GOTO 3050
3004		MessErr = 'Erreur 3004 de lecture de fichier
     .			  (r�partition mensuelle des coef)'
		GOTO 3050
3005		MessErr = 'Erreur 3005 de fin de fichier
     .			  (r�partition des jours types)'
		GOTO 3050
3006		MessErr = 'Erreur 3006 de lecture de fichier
     .			  (r�partition des jours types)'
		GOTO 3050
3007		MessErr = 'Erreur 3007 de fin de fichier
     .			  (r�partition hebdomadaire des coef)'
		GOTO 3050
3008		MessErr = 'Erreur 3008 de lecture de fichier
     .			  (r�partition hebdomadaire des coef)'
		GOTO 3050
3009		MessErr = 'Erreur 3009 de fin de fichier
     .			  (r�partition horaire des coef)'
		GOTO 3050
3010		MessErr = 'Erreur 3010 de lecture de fichier
     .			  (r�partition horaire des coef)'
		GOTO 3050
3011		MessErr = 'Erreur 3011 de fin de fichier
     .			  (positionnement sur un profil)'
		GOTO 3050
3012		MessErr = 'Erreur 3012 de lecture de fichier
     .			  (positionnement sur un profil)'
		GOTO 3050
3050		CLOSE(1)
3100	END

C***************************************************************************************	
 	SUBROUTINE IndiceTranche(TIME,STEP,CoefCycle,Valeur)
		!Permet de lire le coefficient de pond�ration de l'heure sp�cifi�e
		IMPLICIT NONE

		INTEGER		NumTran			!n� d'une tranche horaire de 10 min
		INTEGER		MinCours		!r�el temporaire
		INTEGER     TmpInt,A		!entier temporaire

		DOUBLE PRECISION		TIME,STEP		!heure consid�r�e
		DOUBLE PRECISION        CoefCycle(5,5)
		DOUBLE PRECISION        Valeur,Valeur1,Valeur2
		DOUBLE PRECISION        Epsilon
		
		Epsilon =0.00000001
		TmpInt   = INT(TIME+Epsilon)				!partie enti�re de l'heure
		MinCours  =INT((TIME-TmpInt+Epsilon)*10) 	!partie d�cimale de l'heure
		
		Valeur=1

			A=INT(TIME+Epsilon)+1-INT((TIME+Epsilon)/5)*5
			NumTran  = 5
			IF (MinCours >= 0 .AND. MinCours < 2)	NumTran = 1
			IF (MinCours >= 2 .AND. MinCours < 4)	NumTran = 2
			IF (MinCours >= 4 .AND. MinCours < 6)	NumTran = 3
			IF (MinCours >= 6 .AND. MinCours < 8)	NumTran = 4
			IF (MinCours >= 8 .AND. MinCours < 1)	NumTran = 5
			
			If (Step.EQ.1) then 
				Valeur=1
			Else
				Valeur=CoefCycle(NumTran,A)
			End If
			
			IF (Step.EQ.0.25) then
				
				If (NumTran.EQ.1) Then
					Valeur1	=CoefCycle(2,A)
					Valeur	=(4*Valeur+1*Valeur1)/5
				End If
				
				If (NumTran.EQ.2) Then
						Valeur1	=CoefCycle(3,A)
						Valeur	=(3*Valeur+2*Valeur1)/5
				End If
			
				If (NumTran.EQ.3) Then
						Valeur1	=CoefCycle(4,A)
						Valeur	=(2*Valeur+3*Valeur1)/5
				End If
			
				If (NumTran.EQ.4) Then
						Valeur1	=CoefCycle(5,A)
						Valeur	=(1*Valeur+4*Valeur1)/5
				End If
										
			End If
			
			IF (Step.EQ.0.50) then
					If (NumTran.EQ.1) Then
						Valeur1	=CoefCycle(2,A)
						Valeur2	=CoefCycle(3,A)
						Valeur	=(2*Valeur1+2*Valeur+1*Valeur2)/5
					Else
						Valeur1	=CoefCycle(4,A)+CoefCycle(5,A)
						Valeur	=(2*Valeur1+1*Valeur)/5
					End If						
		    End If
	END





