!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3140

! Object: Selecteur d'entr�e multiple 5 groupes GI=> 5 groupes GI
! Simulation Studio Model: Type3140
! 

! Author: DF
! Editor: TEP2E
! Date:	 April 26, 2014
! last modified: April 26, 2014
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Mode Entr�e-Sortie	- [11;11]
!			Nbre Input	- [1;20]
!			Parametre Out 1 	- [0;10]
!			Parametre Out 2 	- [0;10]
!			Parametre Out 3 	- [0;10]
!			Parametre Out 4 	- [0;10]
!			Parametre Out 5 	- [0;10]

! *** 
! *** Model Inputs 
! *** 
!			Groupe 1 Input	- [-Inf;+Inf]
!			Groupe 2 Input	- [-Inf;+Inf]
!			Groupe 3 Input	- [-Inf;+Inf]
!			Groupe 4 Input	- [-Inf;+Inf]
!			Groupe 5 Input	- [-Inf;+Inf]

! *** 
! *** Model Outputs 
! *** 
!			Groupe 1 Input	- [-Inf;+Inf]
!			Groupe 2 Input	- [-Inf;+Inf]
!			Groupe 3 Input	- [-Inf;+Inf]
!			Groupe 4 Input	- [-Inf;+Inf]
!			Groupe 5 Input	- [-Inf;+Inf]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Simulation Studio)
!************************************************************************

!-----------------------------------------------------------------------------------------------------------------------
! This TRNSYS component skeleton was generated from the TRNSYS studio based on the user-supplied parameters, inputs, 
! outputs, and derivatives.  The user should check the component formulation carefully and add the content to transform
! the parameters, inputs and derivatives into outputs.  Remember, outputs should be the average value over the timestep
! and not the value at the end of the timestep; although in many models these are exactly the same values.  Refer to 
! existing types for examples of using advanced features inside the model (Formats, Labels etc.)
!-----------------------------------------------------------------------------------------------------------------------


      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type3140

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType
      
      Integer NbInputmax,NbgroupeInputmax,NbgroupeOutputmax
      
      Parameter(NbInputmax=20)
      Parameter(NbgroupeInputmax=10)
      Parameter(NbgroupeOutputmax=10)

!    PARAMETERS
      DOUBLE PRECISION Mode_EntreeSortie
      DOUBLE PRECISION NbreInput
      INTEGER NbGroupeInput
      INTEGER NbGroupeOutput
      INTEGER Parametre(1:NbgroupeOutputmax)

!    INPUTS
      DOUBLE PRECISION Entree(1:NbgroupeInputmax,1:NbInputmax)

!    Calculs
      Integer i,j,k, m
      DOUBLE PRECISION Sortie(1:NbgroupeOutputmax,1:NbInputmax)  
      DOUBLE PRECISION Valeur


!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then
        
        NbreInput           = NINT(getParameterValue(2))
        NbGroupeInput       = NINT(getParameterValue(3))
        NbGroupeOutput      = NINT(getParameterValue(4))
        k                   = 4+NbGroupeOutput
        i                   = NbGroupeInput*NbreInput
        j                   = NbGroupeOutput*NbreInput
        
		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(k)           !The number of parameters that the the model wants
		Call SetNumberofInputs(i)                   !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)         !The number of derivatives that the the model wants
		Call SetNumberofOutputs(j)                 !The number of outputs that the the model produces
		Call SetIterationMode(1)                             !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)                   !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)               !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
        Mode_EntreeSortie   = getParameterValue(1)
        NbreInput           = NINT(getParameterValue(2))
        NbGroupeInput       = NINT(getParameterValue(3))
        NbGroupeOutput      = NINT(getParameterValue(4))
      
        do i=1, NbGroupeOutput
          Parametre(i) = NINT(getParameterValue(4+i))
        end do
          Goto 1000
      
		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last

      If(getIsReReadParameters()) Then

		!Read in the Values of the Parameters from the Input File
        Mode_EntreeSortie   = getParameterValue(1)
        NbreInput           = NINT(getParameterValue(2))
        NbGroupeInput       = NINT(getParameterValue(3))
        NbGroupeOutput      = NINT(getParameterValue(4))
      
        do i=1, NbGroupeOutput
          Parametre(i) = NINT(getParameterValue(4+i))
        end do
      EndIf
!-----------------------------------------------------------------------------------------------------------------------
1000  CONTINUE
!Read the Inputs
      Do i=1, NbreInput
          Do j=1, NbGroupeInput
              k=NbGroupeInput*(i-1)+j
              Entree(j,i) = GetInputValue(k)
          End Do
      End Do
		


	!Check the Inputs for Problems (#,ErrorType,Text)
	!Sample Code: If( IN1 <= 0.) Call FoundBadInput(1,'Fatal','The first input provided to this model is not acceptable.')
 
      If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
    i=1
    Do i=1, NbreInput
          Do j=1, NbGroupeInput
              k=NbGroupeInput*(i-1)+j
              m = Parametre(j)
              Valeur = Entree(m,i) 
              Call SetOutputValue(k, valeur)
          End Do
      End Do
 
      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

