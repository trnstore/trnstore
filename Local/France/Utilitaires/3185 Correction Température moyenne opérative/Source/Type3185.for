!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3185

! Object: Correction Top Fonction taux AN ou brassage
! Simulation Studio Model: Type3185
! 

! Author: DF
! Editor: TEP2E
! Date:	 March 03, 2019
! last modified: March 03, 2019
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Nbre Input	- [1;10]
!			Vitesse min	- [0;+Inf]
!			Vitesse max	- [0.2;+Inf]
!			CoefA	- [-Inf;+Inf]
!			CoefB	- [-Inf;+Inf]
!             Alpha

! *** 
! *** Model Inputs 
! *** 
!			Top	C [-Inf;+Inf]
!			Taux brassage	- [0;+Inf]

! *** 
! *** Model Outputs 
! *** 
!			Top corrig�e	C [-Inf;+Inf]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Studio)
!************************************************************************

!-----------------------------------------------------------------------------------------------------------------------
! This TRNSYS component skeleton was generated from the TRNSYS studio based on the user-supplied parameters, inputs, 
! outputs, and derivatives.  The user should check the component formulation carefully and add the content to transform
! the parameters, inputs and derivatives into outputs.  Remember, outputs should be the average value over the timestep
! and not the value at the end of the timestep; although in many models these are exactly the same values.  Refer to 
! existing types for examples of using advanced features inside the model (Formats, Labels etc.)
!-----------------------------------------------------------------------------------------------------------------------


      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type3185

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType
      
      INTEGER , PARAMETER ::  InpMax=10

!    PARAMETERS
      INTEGER          Mode
      INTEGER          Nbre_Input
      DOUBLE PRECISION Vitesse_min
      DOUBLE PRECISION Vitesse_max
      DOUBLE PRECISION CoefA
      DOUBLE PRECISION CoefB
      DOUBLE PRECISION Alpha
      DOUBLE PRECISION Beta
      
!    INPUTS
      DOUBLE PRECISION Tair(1:InpMax)
      DOUBLE PRECISION Trm(1:InpMax)
      DOUBLE PRECISION Taux(1:InpMax)
      
!     OUTPUT
      DOUBLE PRECISION Imp_TOP(1:InpMax)
      
!     Caculs
      INTEGER i,j,k
      DOUBLE PRECISION Vitesse
      DOUBLE PRECISION Delta
      DOUBLE PRECISION Var1, Var2
      DOUBLE PRECISION TOP(1:InpMax)
      DOUBLE PRECISION TOP_C(1:InpMax)
      DOUBLE PRECISION TOP_CBack(1:InpMax)      
      
      
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time        = getSimulationTime()
      Timestep    = getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
          Nbre_Input = NINT(getParameterValue(1))
          Do i=1, Nbre_Input
              Var1 = getStaticArrayValue(Nbre_Input+i)
              Call SetStaticArrayValue(i, Var1)
          End Do
		Return
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then
      Nbre_Input = NINT(getParameterValue(2))
		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(8)           !The number of parameters that the the model wants
		Call SetNumberofInputs(3*Nbre_Input)    !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)          !The number of derivatives that the the model wants
		Call SetNumberofOutputs(Nbre_Input)     !The number of outputs that the the model produces
		Call SetIterationMode(1)                !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(3*Nbre_Input,0)      !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0)     !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      Mode            = NINT(getParameterValue(1))
      Nbre_Input      = NINT(getParameterValue(2))
      Vitesse_min     = getParameterValue(3)
      Vitesse_max     = getParameterValue(4)
      CoefA           = getParameterValue(5)
      CoefB           = getParameterValue(6)
      Alpha           = getParameterValue(7)
      Beta            = getParameterValue(8)
      
      Do i=1, Nbre_Input
          k=1+3*(i-1)
          Tair(i)  = GetInputValue(k)
          Trm(i)   = GetInputValue(k+1)
          Taux(i)  = GetInputValue(k+2)
      End Do
      
      Do i=1, 2*Nbre_Input
          Call SetStaticArrayValue(i,20.0)
      End Do
      
   
      Goto 400

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
      Mode            = NINT(getParameterValue(1))
      Nbre_Input      = NINT(getParameterValue(2))
      Vitesse_min     = getParameterValue(3)
      Vitesse_max     = getParameterValue(4)
      CoefA           = getParameterValue(5)
      CoefB           = getParameterValue(6)
      Alpha           = getParameterValue(7)
      Beta            = getParameterValue(8)

		
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Do i=1, Nbre_Input
          k=1+3*(i-1)
          Tair(i)  = GetInputValue(k)
          Trm(i)   = GetInputValue(k+1)
          Taux(i)  = GetInputValue(k+2)
      End Do
		

	!Check the Inputs for Problems (#,ErrorType,Text)
	!Sample Code: If( IN1 <= 0.) Call FoundBadInput(1,'Fatal','The first input provided to this model is not acceptable.')
 
      If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------
      Do i=1, Nbre_Input
          Top_CBack(i)=getStaticArrayValue(i)
      End Do
      
!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
400   CONTINUE
      
      If (Mode==0) then
          Do i=1, Nbre_Input
              Top_C(i) = Alpha*Tair(i)+(1.0-Alpha)*Trm(i)
          End Do
      Else
          If (Mode==1) then
              Do i=1, Nbre_Input
                  Delta = 0.0
                  Top(i) = 0.5*Tair(i)+0.5*Trm(i)
                  If (Top(i)>25.0) then
                      Vitesse = CoefA+CoefB*Taux(i)
                      Vitesse = Max(Vitesse, Vitesse_min)
                      Vitesse = Min(Vitesse, Vitesse_max) 
                      If (Vitesse>0.2) then
                          Delta = (1.767*log(Vitesse)+2.84)
                      End If
                  End If
                  Top_C(i) = Top(i)-Delta
              End Do
          Else
              Do i=1, Nbre_Input
                      Vitesse = CoefA+CoefB*Taux(i)
                      Vitesse = Max(Vitesse, Vitesse_min)
                      Vitesse = Min(Vitesse, Vitesse_max)
                  Top_C(i) = 0.522*Tair(i)+0.478*Trm(i)-0.21*(Vitesse*(37.8-Tair(i)))**0.5
              End do
          End If
      End If
      
      If (getIsStartTime()) then
          TOP_CBack=Top_c
      End If
      
      Do i=1, Nbre_Input
          Imp_Top(i) = Alpha*Top_C(i)+(1.0-Alpha)*TOP_CBack(i)
      End Do
      
      
!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
	Do i=1, Nbre_Input
          Call SetOutputValue(i, Imp_Top(i)) ! Top corrig�e
      End Do
!-----------------------------------------------------------------------------------------------------------------------
      
      Do i=1, Nbre_Input
          k=Nbre_Input+i
          Call SetStaticArrayValue(k,Imp_Top(i))
      End Do
!-----------------------------------------------------------------------------------------------------------------------
      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

