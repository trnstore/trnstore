!	Copyright 2021 Didier FOUQUET
!	
!	This program is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
!	French

!	Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
!   suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ;
!   soit la version 3 de la licence, soit toute version ultérieure.
	
!   Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la
!   garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU 
!   General Public License pour plus de détails.
    
!	Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; 
!   si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.


      Subroutine Type3187

! Object: PPD-PMV 
! Simulation Studio Model: Type3187
! 

! Author: DF
! Editor: TEP2E
! Date:	 January 14, 2016
! last modified: January 14, 2016
! 
! 
! *** 
! *** Model Parameters 
! *** 
!			Nbre Adultes Hommes	- [0;+Inf]
!			Nbre Aduldes Femmes	- [0;+Inf]
!			Nbre Enfants	- [0;+Inf]

! *** 
! *** Model Inputs 
! *** 
!			Engagement	- [0;+Inf]
!			V�tement en Clo	- [0;2.5]
!			M�tabolisme en met	- [0;5]
!			Temp Air 	C [-Inf;+Inf]
!			Temp moyenne Surface TMsurf	C [-Inf;+Inf]
!			Hygrom�trie de l'air	% (base 100) [0;100]
!			Vitesse de l'air	m/s [0;+Inf]

! *** 
! *** Model Outputs 
! *** 
!			Total Sensible+Latent	kJ/hr [-Inf;+Inf]
!			Latent H20	kg/hr [-Inf;+Inf]
!			PMV	- [-3;3]
!			PPD	% (base 100) [0;100]
!			Perte de chaleur diffusion au travers de la peau	kJ/hr [-Inf;+Inf]
!			Perte de chaleur par sudation	kJ/hr [-Inf;+Inf]
!			Perte de chaleur latente par respiration	kJ/hr [-Inf;+Inf]
!			Perte de chaleur s�che par respiration	kJ/hr [-Inf;+Inf]
!			Perte de chaleur par rayonnement	- [-Inf;+Inf]

! *** 
! *** Model Derivatives 
! *** 

! (Comments and routine interface generated by TRNSYS Studio)
!************************************************************************

!-----------------------------------------------------------------------------------------------------------------------
! This TRNSYS component skeleton was generated from the TRNSYS studio based on the user-supplied parameters, inputs, 
! outputs, and derivatives.  The user should check the component formulation carefully and add the content to transform
! the parameters, inputs and derivatives into outputs.  Remember, outputs should be the average value over the timestep
! and not the value at the end of the timestep; although in many models these are exactly the same values.  Refer to 
! existing types for examples of using advanced features inside the model (Formats, Labels etc.)
!-----------------------------------------------------------------------------------------------------------------------


      Use TrnsysConstants
      Use TrnsysFunctions

!-----------------------------------------------------------------------------------------------------------------------

!DEC$Attributes DLLexport :: Type3187

!-----------------------------------------------------------------------------------------------------------------------
!Trnsys Declarations
      Implicit None

      Double Precision Timestep,Time
      Integer CurrentUnit,CurrentType


!    PARAMETERS
      DOUBLE PRECISION Nbre_Hommes
      DOUBLE PRECISION Nbre_Femmes
      DOUBLE PRECISION Nbre_Enfants
      DOUBLE PRECISION Nbre_m2

!    INPUTS
      DOUBLE PRECISION Engagement
      DOUBLE PRECISION Clo
      DOUBLE PRECISION Met
      DOUBLE PRECISION Temp_Air
      DOUBLE PRECISION TMsurf
      DOUBLE PRECISION Phi
      DOUBLE PRECISION Vitesse_air
      
!     Calculs
      Double precision Surface
      Double precision Icl, W, MW, HCF
      Double precision TCLA, P1,P2,P3,P4,P5
      Double precision XN, XF, EPS
      Double precision PPD, PMV
      Double precision HL1, HL2, HL3, HL4, HL5, HL6
      !Double precision F_PVSTeta !En Pa
      Double precision PA, FCL, TAA, TRA, ERP
      Double precision HCN, HC, TCL, TS
      INTEGER          N

      Double precision F1, F2
      Double precision Hc1
      Double precision Sensible, Total, H20
      Double precision BorneInf,  BorneSup, milieu
      Double precision Var1
      
      Double precision F_PVSTeta3187
      Double precision F_Tcl3187
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
      Time=getSimulationTime()
      Timestep=getSimulationTimeStep()
      CurrentUnit = getCurrentUnit()
      CurrentType = getCurrentType()

!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
      If(getIsVersionSigningTime()) Then
		Call SetTypeVersion(17)
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do Any Last Call Manipulations Here
      If(getIsLastCallofSimulation()) Then
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "After Convergence" Manipulations That May Be Required at the End of Each Timestep
      If(getIsEndOfTimestep()) Then
		Return
      EndIf

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
      If(getIsFirstCallofSimulation()) Then

		!Tell the TRNSYS Engine How This Type Works
		Call SetNumberofParameters(4)       !The number of parameters that the the model wants
		Call SetNumberofInputs(7)           !The number of inputs that the the model wants
		Call SetNumberofDerivatives(0)      !The number of derivatives that the the model wants
		Call SetNumberofOutputs(10)         !The number of outputs that the the model produces
		Call SetIterationMode(1)            !An indicator for the iteration mode (default=1).  Refer to section 8.4.3.5 of the documentation for more details.
		Call SetNumberStoredVariables(0,0)  !The number of static variables that the model wants stored in the global storage array and the number of dynamic variables that the model wants stored in the global storage array
		Call SetNumberofDiscreteControls(0) !The number of discrete control functions set by this model (a value greater than zero requires the user to use Solver 1: Powell's method)

		Return

      EndIf
      
!-----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Intial Time
      If (getIsStartTime()) Then
      Nbre_Hommes     = getParameterValue(1)
      Nbre_Femmes     = getParameterValue(2)
      Nbre_Enfants    = getParameterValue(3)
      Nbre_m2         = getParameterValue(4)

      Engagement      = GetInputValue(1)
      Clo             = GetInputValue(2)
      Met             = GetInputValue(3)
      Temp_Air        = GetInputValue(4)
      TMsurf          = GetInputValue(5)
      Phi             = GetInputValue(6)
      Vitesse_air     = GetInputValue(7)

      Goto 100

		Return

      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
      If(getIsReReadParameters()) Then
		!Read in the Values of the Parameters from the Input File
      Nbre_Hommes     = getParameterValue(1)
      Nbre_Femmes     = getParameterValue(2)
      Nbre_Enfants    = getParameterValue(3)
      Nbre_m2         = getParameterValue(4)
		
      EndIf
!-----------------------------------------------------------------------------------------------------------------------

!Read the Inputs
      Engagement      = GetInputValue(1)
      Clo             = GetInputValue(2)
      Met             = GetInputValue(3)
      Temp_Air        = GetInputValue(4)
      TMsurf          = GetInputValue(5)
      Phi             = GetInputValue(6)
      Vitesse_air     = GetInputValue(7)
		
 
      If(ErrorFound()) Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!    *** PERFORM ALL THE CALCULATION HERE FOR THIS MODEL. ***
!-----------------------------------------------------------------------------------------------------------------------
 100  Continue     
      Surface =1.9*Nbre_Hommes+1.6*Nbre_Femmes+1.2*Nbre_Enfants+Nbre_m2
      
      PA      = Phi/100.*F_PVSTeta3187(Temp_air)
      ICL     = 0.155*Clo
      MW      = Met*58.15
      
      If (ICL<0.78) then
          FCL = 1.0+1.29*ICL
      Else
          FCL = 1.05+0.645*ICL
      End IF
      Hc1 = 12.1*SQRT(Vitesse_Air)
      
      TAA = Temp_Air  +273.0
      TRA = TMsurf    +273.0
      
      !==Calcul Temp�rature surface vetement     
      BorneInf    = min(0.0, Temp_air-10.0)
      BorneSup    = 50.0
      Milieu      = (BorneSup+BorneInf)/2.
      !Tcl => Temp�rature surface du corps habill�
      N   =0
      DO WHILE (N<200)
          Hc=2.28*ABS(BorneInf-Temp_Air)**0.25
          If (Hc<Hc1) Hc=Hc1
          F1=F_Tcl3187(BorneInf, MW, Icl, Fcl, TrA, Hc, Temp_air)
          
          Hc=2.28*ABS(Milieu-Temp_Air)**0.25
          If (Hc<Hc1) Hc=Hc1
          F2=F_Tcl3187(Milieu, MW, Icl, Fcl, TrA, Hc, Temp_air)
          

          IF ((F1*F2)>0.0) then
              BorneInf= Milieu
          Else
              BorneSup= Milieu
          End If
          N=N+1
          If ((ABS(BorneSup-BorneInf)<0.001)) EXIT
          Milieu =(BorneSup+BorneInf)/2.
      End Do
      
      Tcl = (BorneSup+BorneInf)/2.
      HL1 = 3.05*0.001*(5733.-6.99*MW-PA)     !Perte de chaleur par diffusion au travers de la peau
      
      If (MW>58.15) Then
          HL2 = 0.42*(MW-58.15)               !Perte de chaleur par sudation
      Else
          HL2 = 0.0
      End IF
      
      HL3 = 1.72*0.00001*MW*(5867.0-PA)               !Perte de chaleur latente par respiration
      HL4 = 0.0014*MW*(34.0-Temp_Air)                 !Perte de chaleur s�che par respiration
      HL5 = 3.96E-8*FCL*((Tcl+273.1)**4-TRA**4)       !Perte de chaleur par rayonnement
      HL6 = FCL*HC*(TCL-Temp_Air)                     !Perte de chaleur par convection
      
      
      Var1=HL1+HL2+HL3+HL4+HL5+HL6
      
      !Calculs des indices PMV et PPD
      TS  = 0.303*EXP(-0.036*MW)+0.028
      PMV = TS*(MW-HL1-HL2-HL3-HL4-HL5-HL6)

      PPD         = 100.-95.*Exp(-0.03353*PMV**4-0.2179*PMV**2)
      
      Sensible    = (MW-HL1-HL2-HL3)*surface
      Total       =  MW*surface
      H20         = (Total-Sensible)/672.0

!-----------------------------------------------------------------------------------------------------------------------
!Set the Outputs from this Model (#,Value)
		Call SetOutputValue(1, Total*3.6)                       ! Total Sensible+Latent
          Call SetOutputValue(2, Sensible*3.6)                    ! Total Sensible
		Call SetOutputValue(3, H20)                             ! Latent H20
		Call SetOutputValue(4, PMV)                             ! PMV
		Call SetOutputValue(5, PPD)                             ! PPD
		Call SetOutputValue(6, HL1*surface*3.6)                 ! Perte de chaleur diffusion au travers de la peau
		Call SetOutputValue(7, HL2*surface*3.6)                 ! Perte de chaleur par sudation
		Call SetOutputValue(8, HL4*surface*3.6)                 ! Perte de chaleur latente par respiration
		Call SetOutputValue(9, HL4*surface*3.6)                 ! Perte de chaleur s�che par respiration
		Call SetOutputValue(10,HL5*surface*3.6)                 ! Perte de chaleur par rayonnement

!-----------------------------------------------------------------------------------------------------------------------

 
      Return
      End
!-----------------------------------------------------------------------------------------------------------------------

      Function F_Tcl3187(Tcl_C, MW, Icl, Fcl, Tr_K, Hc, Ta_C)
      Implicit None
      Double precision F_Tcl3187
      Double precision Tcl_C, MW, Icl, Fcl, Tr_K, Hc, Ta_C
      Double precision Var1, Var2, Var3, Var4
          Var1 = 35.7-0.028*MW
          Var2 = 3.96E-8*Fcl*((Tcl_C+273.15)**4-Tr_K**4)
          Var3 = Fcl*Hc*(Tcl_C-Ta_C)
          F_Tcl3187 = Tcl_C-(Var1-ICL*(Var2+Var3))
      End Function
      
      
      Function F_PVSTeta3187(T)  !en Pa entre 0 et 110  Coef Var DF precision
      Implicit None
      Double precision F_PVSTeta3187
      Double precision Var
      Double precision T
      If ((T<-10.0).or.(T>250.0)) F_PVSTeta3187=-999999.0      
      If (T<0.0) Then
          Var=0.610065223 + 0.0484339282*T + 0.00135290361*T**2
          Else
          If (T<60.) then
             Var=0.52531024 + 0.0800547031*T - 0.00122674501*T**2 + 0.0000876111269*T**3
          Else
             If (T<120.0) then
                  Var = -72.2133441 + 3.31767329*T - 0.0506372743*T**2 + 0.0003481269*T**3
              Else
                  Var =-1498.9149 + 34.186519*T - 0.27626344*T**2 + 0.000908057591*T**3
              End If
          End IF
          End If
         F_PVSTeta3187=Var*1E+3
      End function

