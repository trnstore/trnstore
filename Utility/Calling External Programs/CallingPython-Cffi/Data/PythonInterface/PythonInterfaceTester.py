# Python module to test the Python interface for the TRNSYS Type calling Python using CFFI
# Data exchange with TRNSYS uses a dictionary, called TRNData in this file (it is the argument of all functions).
# Data for this module will be in a nested dictionary under the module name,
# i.e. if this file is called "MyScript.py", the inputs will be in TRNData["MyScript"]["inputs"]
# for convenience the module name is saved in thisModule
#
# MKu, 2022-02-17

import numpy
import os
import sys

thisModule = os.path.splitext(os.path.basename(__file__))[0]


# CalculateOutputs: function called at TRNSYS initialization
# ----------------------------------------------------------------------------------------------------------------------
def CalculateOutputs(TRNData):

    print(f"\n| Python Log")
    print(f"|\n| The CalculateOutputs() function has been called\n|")

    # Write all variables present in the TRNData[thisModule] dict
    print(f"| Content of TRNData[{thisModule}]")
    for key, value in TRNData[thisModule].items():
        print(f"|     {key}: {value}")

    print(f"|\n| The last item on python path is:           {sys.path[-1]}")
    print(f"| This should be the Fortran executable dir: {TRNData[thisModule]['Fortran executable directory']}")

    inputs = TRNData[thisModule]["inputs"]
    outputs = TRNData[thisModule]["outputs"]
    
    outputs[0] = inputs[0] + 1.0
    outputs[1] = inputs[1] * 2.0
    outputs[2] = numpy.power(inputs[2], 2.0)
    outputs[3] = numpy.power(inputs[2], 3.0)
    outputs[4] = 999.0

    TRNData[thisModule]["outputs"] = outputs

    print(f"|\n| This function has set the outputs to: {outputs}") 
    
    print("|\n| End of the CalculateOutputs function - returning to Fortran\n")
    
    return

