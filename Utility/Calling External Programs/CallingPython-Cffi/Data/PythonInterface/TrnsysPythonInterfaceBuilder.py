"""
Build the DLL that will provide an interface between TRNSYS and Python using CFFI.
This script creates PythonInterface.c and builds PythonInterface.dll.
The created DLL exports the functions defined in TrnsysPythonInterfaceFunctions.py.
"""
# Inspired by Noah Brenowitz's blog post (https://www.noahbrenowitz.com/post/calling-fortran-from-python/)
# and adapted from his repository (https://github.com/nbren12/call_py_fort)
# Other source: CFFI documentation (https://cffi.readthedocs.io/en/latest/embedding.html)
#
# This interface declares 4 functions (which are implemented in the associated PythonInterfaceFunctions.py script):
# - CallPythonFunction: call a Python function in a module
# - ChangePythonWorkingDir: Change Python current working directory and add new working dir to path
# - GetPythonArray: Get a 1-D numpy array of double precision numbers from Python
# - SetPythonArray: Set a 1-D numpy array of double precision numbers in Python
# - SetPythonDouble: Set a double precision number in Python
# - SetPythonInteger: Set an integer number in Python
# - SetPythonString: Set a string variable in Python
#
# Bruno Marcotte, Nicolas Bernier, Michaël Kummert
# Polytechnique Montréal
#
# Rev 1.0, 2022-02

import sys
import os
import cffi
import shutil

# Create ffi builder
ffibuilder = cffi.FFI()

# Header with function declarations
header = """
extern int CallPythonFunction(char *, char *);
extern int ChangePythonWorkingDir(char *);
extern int GetPythonArray(char *, char *, double *, int*);
extern int SetPythonArray(char *, char *, double *, int*);
extern int SetPythonDouble(char *, char *, double *);
extern int SetPythonInteger(char *, char *, int *);
extern int SetPythonString(char *, char *, char *);
"""
ffibuilder.embedding_api(header)

# Get function implementation from Python script
ffibuilder.set_source("PythonInterface", "")
with open('TrnsysPythonInterfaceFunctions.py', 'r') as file:
    python_code = file.read()
ffibuilder.embedding_init_code(python_code)

# Generate C code and compile/build the interface DLL
print("")
print("------------------------------------------------------------------------------------")
print("Generating the Python Interface DLL. You can safely ignore warnings C4022 and C4047.")
print("------------------------------------------------------------------------------------\n")
ffibuilder.compile(target="PythonInterface.*", verbose=True)

# Move .lib, .exp, and .obj files from \Release and delete that directory
shutil.move("Release/PythonInterface.exp", "PythonInterface.exp")
shutil.move("Release/PythonInterface.lib", "PythonInterface.lib")
shutil.move("Release/PythonInterface.obj", "PythonInterface.obj")
os.rmdir("Release")

# Copy created DLL to the TRNSYS Exe directory
if os.path.isfile("../../../Exe/TRNExe64.exe"):
    shutil.copy("PythonInterface.dll", "../../../Exe/PythonInterface.dll")
    print("")
    print("----------------------------------------------------------------------------")
    print(f"PythonInterface.dll was copied to the TRNSYS Exe directory ({os.path.abspath('../../../Exe')})")
    print("----------------------------------------------------------------------------")

else:
    print("")
    print("------------------------------------------------------------------------------------------------")
    print("You should now copy PythonInterface.dll to the TRNSYS Exe directory (by default C:\\TRNSYS18\\Exe)")
    print("------------------------------------------------------------------------------------------------")
    
input("Press ENTER to continue...")