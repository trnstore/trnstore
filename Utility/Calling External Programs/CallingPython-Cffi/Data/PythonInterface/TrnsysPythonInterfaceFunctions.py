"""
Define data exchange functions that will be accessible from TRNSYS.
The data exchange functions operate on a global dictionary called TRNData.
TRNData is a nested dictionary, each TRNSYS unit has its own sub-dictionary in TRNData 
"""
# Inspired by Noah Brenowitz's blog post (https://www.noahbrenowitz.com/post/calling-fortran-from-python/)
# and adapted from his repository (https://github.com/nbren12/call_py_fort)
# Other source: CFFI documentation (https://cffi.readthedocs.io/en/latest/embedding.html)
#
# This script and the associated Fortran code implement 4 functions:
# - CallPythonFunction: call a Python function in a module
# - ChangePythonWorkingDir: Change Python current working directory and add new working dir to path
# - GetPythonArray: Get a 1-D numpy array of double precision numbers from Python
# - SetPythonArray: Set a 1-D numpy array of double precision numbers in Python
# - SetPythonDouble: Set a double precision number in Python
# - SetPythonInteger: Set an integer number in Python
# - SetPythonString: Set a string variable in Python
#
# Bruno Marcotte, Nicolas Bernier, Michaël Kummert
# Polytechnique Montréal
#
# Rev 1.0, 2022-02

from PythonInterface import ffi
import numpy as np  # For array variable exchange
import importlib    # For CallPythonFunction
import os           # For ChangePythonWorkingDir
import sys          # For ChangePythonWorkingDir

from collections import defaultdict
# Initialize global dictionary for data exchange with Fortran
TRNData = defaultdict(dict)

# CallPythonFunction: call any Python function within a module
@ffi.def_extern(error=1)
def CallPythonFunction(moduleName, functionName):
    """
    Call a python function in a module by name. The arguments of the corresponding Fortran function 
    are the module name and the function name as null-terminated C strings.
    The function should take one argument, the TRNData dict. 
    """
    # Get strings from the cdata
    moduleName = ffi.string(moduleName).decode("UTF-8")
    functionName = ffi.string(functionName).decode("UTF-8")
    # Import the python module
    mod = importlib.import_module(moduleName)
    # Get The function we want to call
    fun = getattr(mod, functionName)
    # Call the function - this function can edit the TRNData dict in place
    fun(TRNData)
    return 0

# ChangePythonWorkingDirectory: Change Python current working directory
@ffi.def_extern(error=1)
def ChangePythonWorkingDir(newWorkingDir):
    """
    Change the current working directory in Python and add the new working dir to the path.
    Note that adding the working dir to path is required to load simple modules implemented
    in a .py file in the current directory.
    The argument of the corresponding Fortran function is the new working directory 
    (null-terminated C string using FORWARD SLASH (/) as the path separator).
    """
    # Get string from the cdata
    newWorkingDir = ffi.string(newWorkingDir).decode("UTF-8")
    # Change working directory
    os.chdir(newWorkingDir)
    # Add working directory to path
    sys.path.append(os.getcwd())
    return 0

# GetPythonArray: get a 1-D numpy array of double precision numbers from Python
@ffi.def_extern(error=1)
def GetPythonArray(unitTag, varName, array, length):
    """
    Get a 1-D numpy array of double precision numbers from the TRNData dict. The arguments of the 
    corresponding Fortran function are the TRNSYS unit tag, the array name (null-terminated C strings), 
    the array itself (array of double), and its length (integer).
    """
    # Get strings from the cdata
    unitTag = ffi.string(unitTag).decode("UTF-8")
    varName = ffi.string(varName).decode("UTF-8")
    # Create numpy array pointing to the Fortran array
    array = np.frombuffer(ffi.buffer(array, length[0] * ffi.sizeof("double")), "float64")
    # Get numpy array to be sent to Fortran
    npArray = TRNData.get(unitTag, {}).get(varName, np.zeros(length[0]))
    # Set values of the Fortran array to the ones of the numpy array
    array[:] = npArray.ravel()
    return 0

# SetPythonArray: set a 1-D numpy array of double precision numbers in Python
@ffi.def_extern(error=1)
def SetPythonArray(unitTag, varName, array, length):
    """
    Set a 1-D numpy array of double precision numbers in the TRNData dict. The arguments of the 
    corresponding Fortran function are the TRNSYS unit tag, the array name (null-terminated C strings), 
    the array itself (array of double), and its length (integer).
    """
    # Get strings from the cdata
    unitTag = ffi.string(unitTag).decode("UTF-8")
    varName = ffi.string(varName).decode("UTF-8")
    # Create numpy array pointing to the Fortran array
    array = np.frombuffer(ffi.buffer(array, length[0] * ffi.sizeof("double")), "float64")
    # Create a copy of the numpy array and set it in the data exchange dict
    TRNData[unitTag][varName] = array.copy()
    return 0

# SetPythonDouble: set a scalar double precision value in Python
@ffi.def_extern(error=1)
def SetPythonDouble(unitTag, varName, dbleScalar):
    """
    Set a scalar double precision number in the TRNData dict. The arguments of the corresponding 
    Fortran function are the TRNSYS unit tag, the var name in the dict (null-terminated C strings), 
    and the double precision number
    """
    # Get strings from the cdata
    unitTag = ffi.string(unitTag).decode("UTF-8")
    varName = ffi.string(varName).decode("UTF-8")
    # Set the variable in dict to the scalar value
    TRNData[unitTag][varName] = dbleScalar[0]
    return 0

# SetPythonInteger: set a scalar integer value in Python
@ffi.def_extern(error=1)
def SetPythonInteger(unitTag, varName, intScalar):
    """
    Set a scalar integer number in the TRNData dict. The arguments of the corresponding 
    Fortran function are the TRNSYS unit tag, the var name in the dict (null-terminated C strings), 
    and the integer number
    """
    # Get strings from the cdata
    unitTag = ffi.string(unitTag).decode("UTF-8")
    varName = ffi.string(varName).decode("UTF-8")
    # Set the variable in dict to the scalar value
    TRNData[unitTag][varName] = intScalar[0]
    return 0

# SetPythonString: Set a string in Python
@ffi.def_extern(error=1)
def SetPythonString(unitTag, varName, string):
    """
    Set a string in the TRNData dict. The arguments of the corresponding Fortran function 
    are the TRNSYS unit tag, the string name in the dict, and the string itself, 
    all as null-terminated C strings.
    """
    # Get strings from the cdata
    unitTag = ffi.string(unitTag).decode("UTF-8")
    varName = ffi.string(varName).decode("UTF-8")
    string = ffi.string(string).decode("UTF-8")
    # Set the string variable to the correct value
    TRNData[unitTag][varName] = string
    return 0
