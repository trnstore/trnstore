# TRNSYS-Python-Cffi License

## MIT License

Copyright © 2022 Nicolas Bernier, Bruno Marcotte, Michaël Kummert

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Acknowledgements and references

This software relies on using the [CFFI](https://pypi.org/project/cffi/) package:  
Rigo, A., & Fijalkowski, M. (2022). CFFI - Foreign Function Interface for Python calling C code (1.15).

The Python interface was inspired by Noah Brenowitz's blog post ["Calling Python from Fortran (not the other way around)"](https://www.noahbrenowitz.com/post/calling-fortran-from-python/)  
The code was adapted from his `call_py_fort` [repository](https://github.com/nbren12/call_py_fort)
