# Python module for the TRNSYS Type calling Python using CFFI
# Data exchange with TRNSYS uses a dictionary, called TRNData in this file (it is the argument of all functions).
# Data for this module will be in a nested dictionary under the module name,
# i.e. if this file is called "MyScript.py", the inputs will be in TRNData["MyScript"]["inputs"]
# for convenience the module name is saved in thisModule
#
# This example module just takes 3 inputs and outputs them after multiplying them by 2, but it outputs a log file
# with some information on the Python calls and it also saves a history of inputs and outputs to a file.
#
# MKu, 2022-05-04

import numpy
import os
import sys
import datetime

thisModule = os.path.splitext(os.path.basename(__file__))[0]

# This module uses a log file to write information about the Python calculations
global logFile
# Variable used to collect history of all calls
global history


# Initialization: function called at TRNSYS initialization
# ----------------------------------------------------------------------------------------------------------------------
def Initialization(TRNData):

    global logFile
    global history
    
    # Short names for convenience
    stepNo = TRNData[thisModule]["current time step number"]
    nSteps = TRNData[thisModule]["total number of time steps"]
    nInputs = TRNData[thisModule]["number of inputs"]
    nOutputs = TRNData[thisModule]["number of outputs"]

    # Get TRNSYS input (deck) file basename (filename without extension) to create log file named "***-PythonLog.log"
    TrnsysInputFileName = os.path.basename(TRNData[thisModule]["TRNSYS input file path"])
    TrnsysInputFileBaseName = os.path.splitext(TrnsysInputFileName)[0]
    logFile = open(TrnsysInputFileBaseName + "-PythonLog.log", "w")
    # Write Python version and path
    logFile.write(f"Python version:\n{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}\n\n")
    logFile.write(f"Path to Python:\n{sys.prefix}\n\n")
    logFile.write("Python `sys.path`:\n")
    logFile.write("\n".join(sys.path))
    logFile.write("\n\n")

    logFile.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ") + f"| TRNSYS time = N/A | time step N/A | Initialization call\n")

    # Write all variables present in the TRNData[thisModule] dict to the log file
    logFile.write(f"    Content of TRNData[{thisModule}]\n")
    for key, value in TRNData[thisModule].items():
        logFile.write(f"        {key}: {value}\n")
    logFile.write("\n")
    
    # Initialize the history variables to the correct size
    history = {}
    history["time"] = numpy.zeros(nSteps)
    history["inputs"] = numpy.zeros([nSteps, nInputs])
    history["outputs"] = numpy.zeros([nSteps, nOutputs])
  
    return


# StartTime: function called at TRNSYS starting time (not an actual time step, initial values should be reported)
# ----------------------------------------------------------------------------------------------------------------------
def StartTime(TRNData):

    # This component does nothing except logging at start time (The TRNSYS Type will assign default values to outputs)

    # Short names for convenience
    time = TRNData[thisModule]["time"]
    stepNo = TRNData[thisModule]["current time step number"]
    inputs = TRNData[thisModule]["inputs"]
    outputs = TRNData[thisModule]["outputs"]

    logFile.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ") + f"| TRNSYS time = {time} | time step {stepNo} | Start time call\n")
    # Write all variables present in the TRNData[thisModule] dict to the log file
    logFile.write(f"    Content of TRNData[{thisModule}]\n")
    for key, value in TRNData[thisModule].items():
        logFile.write(f"        {key}: {value}\n")
    logFile.write("\n")

    # Store history - Note that in TRNSYS, time step number 0 is start time, so e.g. in an hourly simulation from 0 to 24 there would be 25 time steps, numbered from 0 to 24
    history["time"][stepNo] = time
    history["inputs"][stepNo,:] = inputs
    history["outputs"][stepNo,:] = outputs

    return


# Iteration: function called at each TRNSYS iteration within a time step
# ----------------------------------------------------------------------------------------------------------------------
def Iteration(TRNData):

    # Short names for convenience
    time = TRNData[thisModule]["time"]
    stepNo = TRNData[thisModule]["current time step number"]
    inputs = TRNData[thisModule]["inputs"]
    outputs = TRNData[thisModule]["outputs"]

    # Calculate the outputs
    outputs = 2.0 * inputs

    # Set outputs in TRNData
    TRNData[thisModule]["outputs"] = outputs

    logFile.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ") + f"| TRNSYS time = {time} | time step {stepNo} | Iteration call\n")
   
    return

# EndOfTimeStep: function called at the end of each time step, after iteration and before moving on to next time step
# ----------------------------------------------------------------------------------------------------------------------
def EndOfTimeStep(TRNData):

    # This model has nothing to do during the end-of-step call, except for logging

    # Short names for convenience
    time = TRNData[thisModule]["time"]
    stepNo = TRNData[thisModule]["current time step number"]
    nSteps = TRNData[thisModule]["total number of time steps"]
    inputs = TRNData[thisModule]["inputs"]
    outputs = TRNData[thisModule]["outputs"]

    logFile.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ") + f"| TRNSYS time = {time} | time step {stepNo} | End of step call\n")

    # Store history - Note that in TRNSYS, time step number 0 is start time, so e.g. in an hourly simulation from 0 to 24 there would be 25 time steps, numbered from 0 to 24
    history["time"][stepNo] = time
    history["inputs"][stepNo,:] = inputs
    history["outputs"][stepNo,:] = outputs

    # Instructions to be performed only at the end of simulation (this time step is the last one)
    if stepNo == nSteps-1:     # Remember: TRNSYS steps go from 0 to (number of steps - 1)
        # Save history to a CSV file
        data2File = numpy.hstack((history["time"][:, None],history["inputs"],history["outputs"]))
        TrnsysInputFileName = os.path.basename(TRNData[thisModule]["TRNSYS input file path"])
        TrnsysInputFileBaseName = os.path.splitext(TrnsysInputFileName)[0]
        numpy.savetxt(TrnsysInputFileBaseName+"-history.csv", data2File, delimiter=",", header="time,input1,input2,input3,output1,output2,output3", comments="")
        
    return


# LastCallOfSimulation: function called at the end of the simulation (once) - outputs are meaningless at this call
# ----------------------------------------------------------------------------------------------------------------------
def LastCallOfSimulation(TRNData):

    # NOTE: TRNSYS performs this call AFTER the executable (the online plotter if there is one) is closed. 
    # Python errors in this function will be difficult (or impossible) to diagnose as they will produce no message.
    # A recommended alternative for "end of simulation" actions it to implement them in the EndOfTimeStep() part, 
    # within a condition that the last time step has been reached.
    #
    # Example (to be placed in EndOfTimeStep()):
    #
    # stepNo = TRNData[thisModule]["current time step number"]
    # nSteps = TRNData[thisModule]["total number of time steps"]
    # if stepNo == nSteps-1:     # Remember: TRNSYS steps go from 0 to (number of steps - 1)
    #     do stuff that needs to be done only at the end of simulation

    # Short names for convenience
    time = TRNData[thisModule]["time"]
    stepNo = TRNData[thisModule]["current time step number"]
    # Print type of call to log file
    logFile.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ") + f"| TRNSYS time = {time} | time step {stepNo} | End of simulation call\n")
    # Close log file
    logFile.close()

    return