! PythonInterfaceTester: Program testing the interface used by the TRNSYS Type calling Python with CFFI
! ----------------------------------------------------------------------------------------------------------------------
!
! Copyright � 2022 Nicolas Bernier, Bruno Marcotte, and Micha�l Kummert. MIT license (see License.md)

program PythonInterfaceTester

use, intrinsic :: iso_c_binding
use, intrinsic :: ieee_exceptions
use kernel32, only : GetCurrentDirectory

implicit none

! --- Interface to the functions implemented in PythonInterface.dll ----------------------------------------------------

interface

    function CallPythonFunction(moduleName, functionName) result(errorCode) bind(c, name='CallPythonFunction')
        ! Call a python function in a module by name. 
        ! Arguments: module name ("module.script" or just "script"), and function name, as null-terminated C strings
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(kind=c_char) :: moduleName, functionName
        integer(c_int) :: errorCode
    end function CallPythonFunction

    function ChangePythonWorkingDir(newWorkingDir) result(errorCode) bind(c, name='ChangePythonWorkingDir')
        ! Change current working directory in Python and add the new working dir to the path
        ! Arguments: new working directory, as null-terminated C string (!use forward slash as path separator!)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(kind=c_char) :: newWorkingDir
        integer(c_int) :: errorCode
    end function ChangePythonWorkingDir

    function GetPythonArray(moduleName, varName, array, n) result(errorCode) bind(c, name="GetPythonArray")
        ! Get a 1-D numpy array of double precision numbers from Python
        ! Arguments: module name for this unit (null-terminated C string), array name in the data exchange dictionary (null-terminated C string), Fortran array (1-D array of double of the correct length), and its length (integer)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        real(c_double) :: array(n)
        integer(c_int) :: n, errorCode
    end function GetPythonArray

    function SetPythonArray(moduleName, varName, array, length) result(errorCode) bind(c, name="SetPythonArray")
        ! Set a 1-D numpy array of double precision numbers in the Python data exchange dictionary
        ! Arguments: module name for this unit (null-terminated C string), array name in the data exchange dictionary (null-terminated C string), Fortran array (1-D array of double of the correct length), and its length (integer)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        real(c_double) :: array(length)
        integer(c_int) :: length
        integer(c_int) :: errorCode
    end function SetPythonArray

    function SetPythonDouble(moduleName, varName, dbleScalar) result(errorCode) bind(c, name="SetPythonDouble")
        ! Set a scalar double precision number in the Python data exchange dictionary
        ! Arguments: module name for this unit (null-terminated C string), variable name in the data exchange dictionary (null-terminated C string), Fortran scalar (double precision)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        real(c_double) :: dbleScalar
        integer(c_int) :: errorCode
    end function SetPythonDouble

    function SetPythonInteger(moduleName, varName, intScalar) result(errorCode) bind(c, name="SetPythonInteger")
        ! Set a scalar double precision number in the Python data exchange dictionary
        ! Arguments: module name for this unit (null-terminated C string), variable name in the data exchange dictionary (null-terminated C string), Fortran scalar (double precision)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        integer(c_int) :: intScalar
        integer(c_int) :: errorCode
    end function SetPythonInteger

    function SetPythonString(moduleName, varName, string) result(errorCode) bind(c, name="SetPythonString")
        ! Set a string in the Python data exchange dictionary
        ! Arguments: module name for this unit, variable name in the data exchange dictionary, string to be set in Python (all as null-terminated C-strings)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        character(c_char) :: string
        integer(c_int) :: errorCode
    end function SetPythonString

end interface

! --- Local variables --------------------------------------------------------------------------------------------------

integer :: errorCode, strLen, anInt = 987
real(8) :: inputs(4) = (/1.0, 2.0, 3.0, 4.0/)
real(8) :: outputs(5) = -1.0
real(8) :: time = 1.23456d0
logical :: pythonFileFound
character (len=300) :: pythonFileName, moduleName, message = "This string has been set by Fortran"
character (len=256) :: currentWorkingDir, newWorkingDir

! --- Main program -----------------------------------------------------------------------------------------------------

pythonFileName = "PythonInterfaceTester.py"
! Remove the ".py" extension from the module name passed to Python
moduleName = pythonFileName(1:index(pythonFileName,'.',back=.true.)-1) !Find last dot, trim string there (also remove the dot)

print *, ""
print *, "PythonInterfaceTester"
print *, "---------------------"
print *, ""


! Check that the Python script exists, otherwise abort
inquire(file=trim(pythonFileName), exist=pythonFileFound)
if ( .not. pythonFileFound ) then
    write(*,'("The Python module was not found. The file should be in the same directory as the TRNSYS input file. Searched for:", a)') trim(pythonFileName)
    stop 1
endif

! Change Python Working directory (in this case it is not necessary in principle, but it will also add the current dir to path)
! Get current dir
strLen = GetCurrentDirectory(len(currentWorkingDir), currentWorkingDir)
currentWorkingDir = currentWorkingDir(1:index(currentWorkingDir,char(0))-1) !Trim string from terminating null character
newWorkingDir = ReplaceSubstring(currentWorkingDir, "\", "/")
write(*, '("The Fortran program has found ", a, " in ", a)') trim(pythonFileName), trim(newWorkingDir)

errorCode = ChangePythonWorkingDir(trim(newWorkingDir) // char(0))
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not change current working directory"
    stop
endif

! Set double scalar value (always null-terminate strings!)
errorCode = SetPythonDouble(trim(moduleName)//char(0), "a double precision value"C, time)
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not set a scalar double value"
    stop
endif

! Set integer scalar value (always null-terminate strings!)
errorCode = SetPythonInteger(trim(moduleName)//char(0), "an integer number"C, anInt)
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not set a scalar integer value"
    stop
endif

! Set string variable (always null-terminate strings!)
errorCode = SetPythonString(trim(moduleName)//char(0), "message"C, trim(message)//char(0))
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not set a string variable"
    stop
endif

! Set other string variable (always null-terminate strings!)
errorCode = SetPythonString(trim(moduleName)//char(0), "Fortran executable directory"C, trim(currentWorkingDir)//char(0))
errorCode = SetPythonString(trim(moduleName)//char(0), "Name of the current Python module"C, trim(moduleName)//char(0))

! Set "inputs" array variable
errorCode = SetPythonArray(trim(moduleName)//char(0), "inputs"C, inputs, size(inputs))
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not set an array variable"
    stop
endif

! Initialize "outputs" array variable
errorCode = SetPythonArray(trim(moduleName)//char(0), "outputs"C, outputs, size(outputs))
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not set an array variable"
    stop
endif

! Call Python function which will set outputs
errorCode = CallPythonFunction(trim(moduleName)//char(0), "CalculateOutputs"C)
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not call Python function"
    stop
endif

! Call built-in Python function to print all variables in the data exchange dictionary
!print *, ""
!print *, "Data exchange dictionary with the builtin print function called by Fortran (before letting Python calculate outputs)"
!errorCode = CallPythonFunction("builtins"C, "print"C)
!if (errorCode /= 0) then
!    print *, "Fortran program aborted. Could not call Python function"
!    stop
!endif

! Get "outputs" array variable from Python
errorCode = GetPythonArray(trim(moduleName)//char(0), "outputs"C, outputs, size(outputs))
if (errorCode /= 0) then
    print *, "Fortran program aborted. Could not get an array variable"
    stop
endif

!print *, ""
!print *, "Data exchange dictionary with the builtin print function called by Fortran (after letting Python calculate outputs)"
!errorCode = CallPythonFunction("builtins"C, "print"C)
!if (errorCode /= 0) then
!    print *, "Fortran program aborted. Could not call Python function"
!    stop
!endif

! Print inputs and outputs
print *, ""
print *, "Program inputs and outputs on the Fortran side"
write(*, '(" Program inputs:  ", <size(inputs)>g)') inputs
write(*, '(" Program outputs: ", <size(outputs)>g)') outputs

print *, ""
print *, "End of PythonInterfaceTester - Press ENTER to continue"
print *, "------------------------------------------------------"
print *, ""
read(*,*)


! ----------------------------------------------------------------------------------------------------------------------
! --- Internal (contained) functions and subroutines -------------------------------------------------------------------
! ----------------------------------------------------------------------------------------------------------------------
contains
    
! ReplaceSubstring: replace substring within a string 
! Source: https://stackoverflow.com/questions/58938347 by user https://stackoverflow.com/users/2088694/scientist
! ----------------------------------------------------------------------------------------------------------------------

recursive function ReplaceSubstring(string, search, substitute) result(modifiedString)

implicit none

character(len=*), intent(in)  :: string, search, substitute
character(len=:), allocatable :: modifiedString
integer                       :: i, stringLen, searchLen

stringLen = len(string)
searchLen = len(search)
if (stringLen==0 .or. searchLen==0) then
    modifiedString = ""
    return
elseif (stringLen<searchLen) then
    modifiedString = string
    return
end if
i = 1
do
    if (string(i:i+searchLen-1)==search) then
        modifiedString = string(1:i-1) // substitute // ReplaceSubstring(string(i+searchLen:stringLen),search,substitute)
        exit
    end if
    if (i+searchLen>stringLen) then
        modifiedString = string
        exit
    end if
    i = i + 1
    cycle
end do
end function ReplaceSubstring

end program PythonInterfaceTester


