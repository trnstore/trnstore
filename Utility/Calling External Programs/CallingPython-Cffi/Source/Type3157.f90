! TRNSYS Type 3157: Calling Python (with CFFI)
! ----------------------------------------------------------------------------------------------------------------------
!
! This routine calls Python using CFFI
!
! The name of the main Python script is provided using a LABEL in the input file. 
! The script should implement tests to handle special calls 
! (initialization time step, end of simulation, etc.) 
!
! Inputs 
! ----------------------------------------------------------------------------------------------------------------------
! No | Variable      | Description                                                    | Input Units    | Internal Units 
! ---|---------------|----------------------------------------------------------------|----------------|----------------
!  i | input i       | Type 3157 will pass nI inputs to Python                        | any            | any
!    | (i=1..nI)     | nI should be <= nMaxinputs set in this code                    |                |
!
! Parameters
! ----------------------------------------------------------------------------------------------------------------------
! No | Variable      | Description                                                    | Param. Units   | Internal Units 
! ---|---------------|----------------------------------------------------------------|----------------|----------------
!  1 | mode          | Not Implemented yet                                            | -              | -
!  2 | nI            | Number of inputs sent to Python                                | -              | -
!  3 | nO            | Number of outputs from Python                                  | -              | -             
!  4 | iterationMode | Describes the iteration mode of the component                  | -              | -
!    |               |  1 - Standard iterative component                              |                |              
!    |               |  2 - Real-time controller. That kind of component is only      |                |              
!    |               |      called after the other components have converged,         |                |              
!    |               |      and after the integrators and printers. It keeps its      |                |              
!    |               |      output value constant over the next time step             |                |              
!    |               |  3 - Non-iterative component called after convergence, but     |                | 
!    |               |      before integrators and printers. E.g. statistics routines |                |
!    |               |      that send their outputs to integrators or printers.       |                |              
!
! Outputs 
! ----------------------------------------------------------------------------------------------------------------------
! No | Variable      | Description                                                    | Output  Units  | Internal Units 
! ---|---------------|----------------------------------------------------------------|----------------|----------------
!  i | output i      | Type 3157 will receive nO outputs from Python                  | any            | any
!    | (i=1..nO)     | nO should be <= nMaxOutputs set in this code                   |                |
!
!
! Special cards: Labels
! ----------------------------------------------------------------------------------------------------------------------
! No | Description
! ---|------------------------------------------------------------------------------------------------------------------
!  1 | Name of the Python script to be called (should be in the TRNSYS project directory or on Python's search path)
!    | The name should include the .py extension. The module name (without the .py extension) is also used 
!    | to distinguish between different TRNSYS units (instances) of this Type.
!
! 
! Contained subroutines and functions
! ----------------------------------------------------------------------------------------------------------------------   
! Interaction with the TRNSYS kernel
!   GetParameters : Read all parameters and labels - no check is done (only in Initialization)
!   GetInputs : Get all input values
!   SetOutputs : Set all output values
! Manipulations during specific call types from TRNSYS    
!   Initialization: Read parameters and check them for problems, perform other initialization tasks
!   StartTime: Perform Python computations at time = start time
!   Iteration: Perform Python computations for iterative call
!   EndOftimeStep: Perform Python instructions for end of step call
!   LastCallOfSimulation: Perform Python instructions for last call of simulation
! Utility routines
!   ReplaceSubstring: replace substring within a string 
!
!
! Required external librairies
! ----------------------------------------------------------------------------------------------------------------------
!
! PythonInterface.lib is required to compile and build this Type
! PythonInterface.dll should be accessible to TRNSYS at runtime (i.e. it should be in .\Exe)
!
!
! ----------------------------------------------------------------------------------------------------------------------
! Copyright � 2022 Nicolas Bernier, Bruno Marcotte, and Micha�l Kummert. MIT license (see License.md)

subroutine Type3157
!dec$ attributes dllexport :: Type3157

use TrnsysConstants, only: maxLabelLength, maxPathLength, maxMessageLength, maxFileWidth
use TrnsysFunctions
use, intrinsic :: iso_c_binding

implicit none

! --- Interface to the functions implemented in PythonInterface.dll ----------------------------------------------------

interface

    function CallPythonFunction(moduleName, functionName) result(errorCode) bind(c, name='CallPythonFunction')
        ! Call a python function in a module by name. 
        ! Arguments: module name ("module.script" or just "script"), and function name, as null-terminated C strings
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(kind=c_char) :: moduleName, functionName
        integer(c_int) :: errorCode
    end function CallPythonFunction

    function ChangePythonWorkingDir(newWorkingDir) result(errorCode) bind(c, name='ChangePythonWorkingDir')
        ! Change current working directory in Python and add the new working dir to the path
        ! Arguments: new working directory, as null-terminated C string (!use forward slash as path separator!)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(kind=c_char) :: newWorkingDir
        integer(c_int) :: errorCode
    end function ChangePythonWorkingDir

    function GetPythonArray(moduleName, varName, array, n) result(errorCode) bind(c, name="GetPythonArray")
        ! Get a 1-D numpy array of double precision numbers from Python
        ! Arguments: module name for this unit (null-terminated C string), array name in the data exchange dictionary (null-terminated C string), Fortran array (1-D array of double of the correct length), and its length (integer)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        real(c_double) :: array(n)
        integer(c_int) :: n, errorCode
    end function GetPythonArray

    function SetPythonArray(moduleName, varName, array, length) result(errorCode) bind(c, name="SetPythonArray")
        ! Set a 1-D numpy array of double precision numbers in the Python data exchange dictionary
        ! Arguments: module name for this unit (null-terminated C string), array name in the data exchange dictionary (null-terminated C string), Fortran array (1-D array of double of the correct length), and its length (integer)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        real(c_double) :: array(length)
        integer(c_int) :: length
        integer(c_int) :: errorCode
    end function SetPythonArray

    function SetPythonDouble(moduleName, varName, dbleScalar) result(errorCode) bind(c, name="SetPythonDouble")
        ! Set a scalar double precision number in the Python data exchange dictionary
        ! Arguments: module name for this unit (null-terminated C string), variable name in the data exchange dictionary (null-terminated C string), Fortran scalar (double precision)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        real(c_double) :: dbleScalar
        integer(c_int) :: errorCode
    end function SetPythonDouble

    function SetPythonInteger(moduleName, varName, intScalar) result(errorCode) bind(c, name="SetPythonInteger")
        ! Set a scalar double precision number in the Python data exchange dictionary
        ! Arguments: module name for this unit (null-terminated C string), variable name in the data exchange dictionary (null-terminated C string), Fortran scalar (double precision)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        integer(c_int) :: intScalar
        integer(c_int) :: errorCode
    end function SetPythonInteger

    function SetPythonString(moduleName, varName, string) result(errorCode) bind(c, name="SetPythonString")
        ! Set a string in the Python data exchange dictionary
        ! Arguments: module name for this unit, variable name in the data exchange dictionary, string to be set in Python (all as null-terminated C-strings)
        ! Returned value: error code (should be 0)
        use iso_c_binding
        character(c_char) :: moduleName, varName
        character(c_char) :: string
        integer(c_int) :: errorCode
    end function SetPythonString

end interface

! --- Constants --------------------------------------------------------------------------------------------------------

integer, parameter :: nMaxInputs = 200, nMaxOutputs = 200    ! Used to size arrays
integer, parameter :: nP = 4    ! No of parameters

! --- Local variables --------------------------------------------------------------------------------------------------

integer :: nI, nO    ! No of inputs, outputs, and derivatives
integer :: mode, iterationMode
real(8) :: inputs(nMaxInputs) = 0, outputs(nMaxOutputs) = 0    ! Arrays with all inputs and outputs
real(8) :: time, tStart, tStep, tStop 
integer :: pythonErrorCode
character (len=maxLabelLength) :: pythonFileName, pythonModuleName
character (len=maxMessageLength) :: msg
character (len=maxPathLength) :: workingDir

!--- Version signing call: set the version number for this Type --------------------------------------------------------

if (GetIsVersionSigningTime()) then
    Call SetTypeVersion(18)
    return
endif

! --- Very first call of simulation (initialization call) --------------------------------------------------------------

if (GetIsFirstCallofSimulation()) then
    call Initialization()    
    return
endif

! --- For all other calls, re-read parameters and inputs, and get TRNSYS variables -------------------------------------

if (ErrorFound()) return ! Abort if errors were flagged by TRNSYS
call GetParameters()
call GetInputs()

! --- Start time call: not a real time step, there are no iterations and no "end of timestep" call ---------------------

if (GetIsStartTime()) then
    call StartTime()
    call SetOutputs()
    return
endif

! ---- Last call in the simulation (after last timestep has converged) -------------------------------------------------

if (GetIsLastCallofSimulation()) then
    call LastCallOfSimulation()  
    return
endif    
    
! --- End of timestep call (after convergence or too many iterations) --------------------------------------------------

if (GetIsEndOfTimestep()) then
    call EndOfTimeStep()
    call SetOutputs()
    return
endif

! --- Normal iterative call --------------------------------------------------------------------------------------------

call Iteration()
call SetOutputs()


return  ! End of Type3157 main routine


! ----------------------------------------------------------------------------------------------------------------------
! --- Internal (contained) functions and subroutines -------------------------------------------------------------------
! ----------------------------------------------------------------------------------------------------------------------
contains
 
    
! EndOftimeStep: Perform Python instructions for end of step call
! ----------------------------------------------------------------------------------------------------------------------
subroutine EndOfTimeStep()
implicit none

integer :: i

! Get inputs
call GetInputs()
! Set current values in the data exchange dictionary
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "current time step number", GetTimeStepNumber())
pythonErrorCode = pythonErrorCode + SetPythonDouble(trim(pythonModuleName)//char(0), "time", GetSimulationTime())
pythonErrorCode = pythonErrorCode + SetPythonArray(trim(pythonModuleName)//char(0), "inputs", inputs, nI)
! Standard types should not change their outputs at this call. Give the previous values to Python.
do i = 1, nO
    outputs(i) = GetOutputValue(i)
end do
pythonErrorCode = pythonErrorCode + SetPythonArray(trim(pythonModuleName)//char(0), "outputs", outputs, nO)
if (pythonErrorCode /= 0) then
    call ShowMessage("End of time step: Cannot set variables in the data exchange dictionary", "fatal")
    return
endif

! Call the EndOfTimeStep function
pythonErrorCode = pythonErrorCode + CallPythonFunction(trim(pythonModuleName)//char(0), "EndOfTimeStep"C)
if (pythonErrorCode /= 0) then
    call ShowMessage("Cannot perform Python computations at end of time step", "fatal")
    return
endif

! Get the value of outputs
pythonErrorCode = pythonErrorCode + GetPythonArray(trim(pythonModuleName)//char(0), "outputs", outputs, nO)
if (pythonErrorCode /= 0) then
    call ShowMessage("End of time step: Cannot get outputs from the data exchange dictionary", "fatal")
    return
endif

end subroutine EndOfTimeStep


! GetInputs : get all input values
! ----------------------------------------------------------------------------------------------------------------------
subroutine GetInputs()
implicit none
integer i

do i = 1, nI
    inputs(i) = GetInputValue(i)
end do

end subroutine GetInputs


! GetParameters : Read all parameters and labels - no check is done (only in Initialization)
! ----------------------------------------------------------------------------------------------------------------------
subroutine GetParameters()
implicit none

mode = nint(GetParameterValue(1))
nI = nint(GetParameterValue(2))
nO = nint(GetParameterValue(3))
iterationMode = nint(GetParameterValue(4))
pythonFileName = getLabel(GetCurrentUnit(),1)
! Remove the ".py" extension from the module name passed to Python
pythonModuleName = pythonFileName(1:index(pythonFileName,'.',back=.true.)-1) !Find last dot, trim string there (also remove the dot)

end subroutine GetParameters


! Initialization: read parameters and check them for problems, perform other initialization tasks
! ----------------------------------------------------------------------------------------------------------------------
subroutine Initialization()

use, intrinsic :: ieee_exceptions
implicit none

logical :: halting(5), pythonFileFound

! Tell TRNSYS how many parameters we need
call SetNumberOfParameters(nP)       !The number of parameters that the the model wants
if (ErrorFound()) return    ! Abort if errors were flagged by TRNSYS when checking the number of parameters

! Read and check parameters
call GetParameters()
if (mode /= 0) call FoundBadParameter(1,'Fatal','Only mode 0 is currenly implemented.')    
if (nI > nMaxInputs) then
    write(msg,'("The number of inputs is greater than the maximum allowed. This maximum value is set in the source code, it is currently = ",g)') nMaxInputs
    call FoundBadParameter(2,"Fatal",msg)    
endif
if (nO > nMaxOutputs) then
    write(msg,'("The number of outputs is greater than the maximum allowed. This maximum value is set in the source code, it is currently = ",g)') nMaxOutputs
    call FoundBadParameter(3,"Fatal",msg)    
endif
if ((iterationMode < 1) .or. (iterationMode > 3)) call FoundBadParameter(4,'Fatal','Iteration mode must be between 1, 2 or 3')    
if (ErrorFound()) return ! Abort if errors were flagged by TRNSYS
! Check that the Python file exists, otherwise abort
inquire(file=trim(pythonFileName), exist=pythonFileFound)
if ( .not. pythonFileFound ) then
    write(msg,'("The Python file was not found. The file should be in the same directory as the TRNSYS input file. Searched for: ", a)') trim(pythonFileName)
    call ShowMessage(msg, "fatal")
    return
endif

! Tell TRNSYS what this Type needs
call SetNumberofInputs(nI) 
call SetNumberofOutputs(nO) 
call SetNumberofDerivatives(0)
call SetNumberStoredVariables(0,0) 
call SetNumberofDiscreteControls(0) 
call SetIterationMode(iterationMode) 
if (ErrorFound()) return ! Abort if errors were flagged by TRNSYS

! The first time the Python interface is used, floating point exceptions can be triggered when numpy is imported
! The workaround implemented here is documented in https://github.com/numpy/numpy/issues/20504
! To prevent a TRNSYS crash, halting is disabled for all floating point errors before the first call
call ieee_get_halting_mode(ieee_all, halting) ! Remember halting behavior to set it back later
call ieee_set_halting_mode(ieee_all, .false.) ! Disable all 
! Initialize Python data exchange dictionary by setting the name of the TRNSYS input file (which will trigger an "import numpy" on the Python side)
pythonErrorCode = SetPythonString(trim(pythonModuleName)//char(0), "TRNSYS input file path"C, GetDeckFileName() // char(0))
! Error flags are reset, and then halting behavior is restored to its original state
call ieee_set_flag(ieee_all, .false.)
call ieee_set_halting_mode(ieee_all, halting)
if (pythonErrorCode /= 0) then
    call ShowMessage("Cannot initialize Python dict", "fatal")
    return
endif

! Change Python current working directory to the TRNSYS input (deck) file directory
workingDir = ReplaceSubstring(GetTrnsysInputFileDir(), "\", "/")
pythonErrorCode = ChangePythonWorkingDir(trim(workingDir) // char(0))
if (pythonErrorCode /= 0) then
    call ShowMessage("Cannot change Python working directory", "fatal")
    return
endif

! Set some initial values in the data exchange dictionary
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "number of inputs", nI)
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "number of outputs", nO)
pythonErrorCode = pythonErrorCode + SetPythonDouble(trim(pythonModuleName)//char(0), "simulation start time", GetSimulationStartTime())
pythonErrorCode = pythonErrorCode + SetPythonDouble(trim(pythonModuleName)//char(0), "simulation stop time", GetSimulationStopTime())
pythonErrorCode = pythonErrorCode + SetPythonDouble(trim(pythonModuleName)//char(0), "simulation time step", GetSimulationTimeStep())
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "total number of time steps", GetnTimeSteps())
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "current time step number", GetTimeStepNumber())

if (pythonErrorCode /= 0) then
    call ShowMessage("Cannot set variables in the data exchange dictionary", "fatal")
    return
endif

! Call the Initialization function
! The first time the Python module is loaded, floating point exceptions can be triggered when packages are imported
! The workaround implemented here is documented in https://github.com/numpy/numpy/issues/20504
! To prevent a TRNSYS crash, halting is disabled for all floating point errors before the first call
call ieee_get_halting_mode(ieee_all, halting) ! Remember halting behavior to set it back later
call ieee_set_halting_mode(ieee_all, .false.) ! Disable all 
pythonErrorCode = pythonErrorCode + CallPythonFunction(trim(pythonModuleName)//char(0), "Initialization"C)
! Error flags are reset, and then halting behavior is restored to its original state
call ieee_set_flag(ieee_all, .false.)
call ieee_set_halting_mode(ieee_all, halting)
if (pythonErrorCode /= 0) then
    call ShowMessage("Error calling the Python Initialization() function", "fatal")
    return
endif

end subroutine Initialization


! Iteration: Perform Python computations for iterative call
! ----------------------------------------------------------------------------------------------------------------------
subroutine Iteration()
implicit none

integer :: i

! Get inputs
call GetInputs()
! Set current values in the data exchange dictionary
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "current time step number", GetTimeStepNumber())
pythonErrorCode = pythonErrorCode + SetPythonDouble(trim(pythonModuleName)//char(0), "time", GetSimulationTime())
pythonErrorCode = pythonErrorCode + SetPythonArray(trim(pythonModuleName)//char(0), "inputs", inputs, nI)
! Give previous output values to Python as a starting point
do i = 1, nO
    outputs(i) = GetOutputValue(i)
end do
pythonErrorCode = pythonErrorCode + SetPythonArray(trim(pythonModuleName)//char(0), "outputs", outputs, nO)
if (pythonErrorCode /= 0) then
    call ShowMessage("Iterative call: Cannot set variables in the data exchange dictionary", "fatal")
    return
endif

! Call the Iteration function
pythonErrorCode = pythonErrorCode + CallPythonFunction(trim(pythonModuleName)//char(0), "Iteration"C)
if (pythonErrorCode /= 0) then
    call ShowMessage("Cannot perform Python computations at iteration", "fatal")
    return
endif

! Get the value of outputs
pythonErrorCode = pythonErrorCode + GetPythonArray(trim(pythonModuleName)//char(0), "outputs", outputs, nO)
if (pythonErrorCode /= 0) then
    call ShowMessage("Iteration: Cannot get outputs from the data exchange dictionary", "fatal")
    return
endif

end subroutine Iteration


! LastCallOfSimulation: Perform Python instructions for last call of simulation
! ----------------------------------------------------------------------------------------------------------------------
subroutine LastCallOfSimulation()
implicit none

! Set current values in the data exchange dictionary
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "current time step number", GetTimeStepNumber())
pythonErrorCode = pythonErrorCode + SetPythonDouble(trim(pythonModuleName)//char(0), "time", GetSimulationTime())

! Call the LastCallOfSimulation function
pythonErrorCode = pythonErrorCode + CallPythonFunction(trim(pythonModuleName)//char(0), "LastCallOfSimulation"C)
if (pythonErrorCode /= 0) then
    call ShowMessage("Cannot perform Python computations at last call of simulation", "fatal")
    return
endif

end subroutine LastCallOfSimulation


! ReplaceSubstring: replace substring within a string 
! Source: https://stackoverflow.com/questions/58938347 by user https://stackoverflow.com/users/2088694/scientist
! ----------------------------------------------------------------------------------------------------------------------
recursive function ReplaceSubstring(string, search, substitute) result(modifiedString)

implicit none

character(len=*), intent(in)  :: string, search, substitute
character(len=:), allocatable :: modifiedString
integer                       :: i, stringLen, searchLen

stringLen = len(string)
searchLen = len(search)
if (stringLen==0 .or. searchLen==0) then
    modifiedString = ""
    return
elseif (stringLen<searchLen) then
    modifiedString = string
    return
end if
i = 1
do
    if (string(i:i+searchLen-1)==search) then
        modifiedString = string(1:i-1) // substitute // ReplaceSubstring(string(i+searchLen:stringLen),search,substitute)
        exit
    end if
    if (i+searchLen>stringLen) then
        modifiedString = string
        exit
    end if
    i = i + 1
    cycle
end do
end function ReplaceSubstring


! SetOutputs : Set all output values
! ---------------------------------------------------------------------------------------------------------------------- 
subroutine SetOutputs()
implicit none
integer i

do i = 1, nO
    call SetOutputValue(i, outputs(i))
end do

end subroutine SetOutputs
    

! StartTime: Perform Python computations at time = start time
! ----------------------------------------------------------------------------------------------------------------------
subroutine StartTime()
implicit none

integer :: i

! Get inputs
call GetInputs()
! Set current values in the data exchange dictionary
pythonErrorCode = pythonErrorCode + SetPythonInteger(trim(pythonModuleName)//char(0), "current time step number", GetTimeStepNumber())
pythonErrorCode = pythonErrorCode + SetPythonDouble(trim(pythonModuleName)//char(0), "time", GetSimulationTime())
pythonErrorCode = pythonErrorCode + SetPythonArray(trim(pythonModuleName)//char(0), "inputs", inputs, nI)
! Set outputs to -9999
do i = 1, nO
    outputs(i) = -9999.0
end do
pythonErrorCode = pythonErrorCode + SetPythonArray(trim(pythonModuleName)//char(0), "outputs", outputs, nO)
if (pythonErrorCode /= 0) then
    call ShowMessage("StartTime: Cannot set variables in the data exchange dictionary", "fatal")
    return
endif

! Call the StartTime function
pythonErrorCode = pythonErrorCode + CallPythonFunction(trim(pythonModuleName)//char(0), "StartTime"C)
if (pythonErrorCode /= 0) then
    call ShowMessage("Cannot perform Python computations at start time", "fatal")
    return
endif

! Get the initial value of outputs
pythonErrorCode = pythonErrorCode + GetPythonArray(trim(pythonModuleName)//char(0), "outputs", outputs, nO)
if (pythonErrorCode /= 0) then
    call ShowMessage("StartTime: Cannot get outputs from the data exchange dictionary", "fatal")
    return
endif

end subroutine StartTime

    
end subroutine Type3157
    
    
