# Calling Python from TRNSYS with CFFI

v0.6.0 (2022-05-06)

To install the required files, unzip the file CallingPython-Cffi v0.6.0 (2022-05-06).zip to your TRNSYS installation directory (by default C:\TRNSYS18)

Please refer to the user manual for complete instructions (PDF file in TRNLib\CallingPython-Cffi\Documentation)

Copyright © 2022 Nicolas Bernier, Bruno Marcotte, Michaël Kummert. MIT License (see TRNLib\CallingPython-Cffi\License.md)

## Citation

To cite this software, please use:  

Bernier, Nicolas, Bruno Marcotte, and Michaël Kummert. 2022. Calling Python from TRNSYS with CFFI. Polytechnique Montréal. [DOI: 10.5281/zenodo.6523078](https://doi.org/10.5281/zenodo.6523078)

## Acknowledgements and references

This software relies on using the [CFFI](https://pypi.org/project/cffi/) package:  
Rigo, A., & Fijalkowski, M. (2022). CFFI - Foreign Function Interface for Python calling C code (1.15).

The Python interface was inspired by Noah Brenowitz's blog post ["Calling Python from Fortran (not the other way around)"](https://www.noahbrenowitz.com/post/calling-fortran-from-python/)  
The code was adapted from his `call_py_fort` [repository](https://github.com/nbren12/call_py_fort)
