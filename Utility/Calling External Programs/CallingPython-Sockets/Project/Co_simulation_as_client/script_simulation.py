"""
Example of coupling between a TRNSYS simulation and a python script, via Type 899 and sockets.

The script mimics a simulation that sends some cold water and receives it heated by Type 74 in the TRNSYS simulation.

How to use:
    In SimulationStudio, open Project.tmf and configure the instance of Type 899b to set the script path to this one.
    Launch the Trnsys project "Project.dck" which should execute this file with python automatically.
"""


import json
import socket
from math import sin, pi
from random import randint
import time

def compute(phase, outlet_temp, outlet_flow):
    """ Simulate the simulation's calculation. """
    radiation = 10. * sin(phase * 2. * pi) + 15.  # sine-shaped radiation, not realistic.
    inlet_temp = 5.0 + outlet_temp / 1000.0
    inlet_flow = 100.0 + outlet_flow / 1000.0
    return radiation, inlet_temp, inlet_flow


def simulation():
    """ Mimics a simple simulation, exchanging data with TRNSYS at every timestep. """

    step_count = 97

    # The first piece of data we will send to Trnsys.
    init_json = {
        # Coupled variables. (eg "radiation")
        # They must map to an index [0, 1, ...], following the order in the TRNSYS Type 899 input/output lists.
        "from_dimosim": {"Object": {"name": {"inlet_temperature": 1, "inlet_flowrate": 0, "radiation": 2}}},
        "to_dimosim": {"Object": {"name": {"outlet_flowrate": 0, "outlet_temperature": 1}}},
    }
    print('starting')
    
    # Open the client socket to connect to the Type 899's server socket.
    sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    print('socket open')
    sock.connect(("::1", 9999))
    print("Socket connected.")

    # first data exchange
    received = sock.recv(4096)
    received = json.loads(received.decode("ascii"))
    print("Received:", json.dumps(received, indent=4))

    data = json.dumps(init_json)  # transform the init json into a string
    encoded_data = data.encode("ascii")  # encode the string to a byte array
    sock.send(encoded_data)  # send to the client
    # examples of configuration check
    assert step_count == received["step_number"], "TRNSYS must have as many timesteps as the script"
    print("step count ok")
    assert not received["is_iterative"], "Type 899 must not be in iterative mode."
    print("type 899 mode ok")
    time.sleep(1)

    sent_heat = 0.0
    received_heat = 0.0

    # enter the simulation loop
    for step in range(step_count):
    
        # first, received Trnsys' data. Despite Type 899 being the socket-wise server, it sends first and receives last.
        received = sock.recv(4096)
        # convert to a python dictionary object
        received = json.loads(received.decode("ascii"))
        assert step == received["header"]["step"], "The script and TRNSYS must be at the same timestep."
        
        # extract the variables values
        outlet_temp = received["payload"]["Object"]["name"]["outlet_temperature"]
        print("outlet_temp = ", outlet_temp)
        #time.sleep(1)
        outlet_flow = received["payload"]["Object"]["name"]["outlet_flowrate"]

        # the enegy collected by the solar panel could be obtained directly via its output "useful energy gain"
        # but for the example, the script computes it so to simulate some calculation.
        # energy = flow rate * time * temperature (K) * specific heat capacity
        received_heat += outlet_flow * 0.5 * (outlet_temp + 273.15) * 4.19

        # Now we can compute the values we want to send back to Trnsys.
        radiation, inlet_temp, inlet_flow = compute(step/step_count * 1.5, outlet_temp, outlet_flow)

        sent_heat += inlet_flow * 0.5 * (inlet_temp + 273.15) * 4.19

        # Prepare the json to send
        header = {"step": step, "converged": True}
        payload = {"Object": {"name": {"inlet_temperature": inlet_temp,
                                       "inlet_flowrate": inlet_flow,
                                       "radiation": radiation}}}
        tosend = {"header": header, "payload": payload}
        # and send it
        sock.send(json.dumps(tosend).encode("ascii"))

    # end of the simulation
    sock.close()
    average_power = (received_heat - sent_heat) / step_count / 0.5 / 3.6
    print(f"Terminated. Average power of TRNSYS's solar collector: {average_power} W.")


def iterative_simulation():
    """
    Same as before, but in iterative mode: data is exchanged at every iteration,
    which is more fine-grained than every timestep. However it requires a certain load of synchronization overhead.

    Don't forge to check "1" in Type 899's parameter "iterative" so that it operates in iterative mode as well.
    """

    step_count = 97

    init_json = {
        "from_dimosim": {"Object": {"name": {"inlet_temperature": 1, "inlet_flowrate": 0, "radiation": 2}}},
        "to_dimosim": {"Object": {"name": {"outlet_flowrate": 0, "outlet_temperature": 1}}},
    }

    # Open the client socket to connect to the Type 899's server socket.
    sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    sock.connect(("::1", 9999))
    print("Socket connected.")

    # first data exchange
    data = json.dumps(init_json)  # transform the init json into a string
    encoded_data = data.encode("ascii")  # encode the string to a byte array
    sock.send(encoded_data)  # send to the client
    received = sock.recv(4096)
    received = json.loads(received.decode("ascii"))
    print("Received:", json.dumps(received, indent=4))
    assert received["is_iterative"], "Type 899 must be in iterative mode in this test. Change 'iterative' to '1' in " \
                                     "the parameters of Type 899's instance."

    sent_heat = 0.0
    received_heat = 0.0
    iterations = []

    # enter the simulation loop
    for step in range(step_count):

        """
        In a real simulation, there would be a certain number of iterations required until the system can hop to the
        next timestep, aka "converge". We will mimic this behaviour.
        
        The field ["header"]["converge"] in the exchange json is a boolean that indicates the state of the simulation:
        - for TRNSYS, it means that TRNSYS will converge.
        - for the other simulation, i.e. ours, it means we are ready to converge and will wait for TRNSYS to do so.
        
        That is, Type 899 won't let TRNSYS converge as long as we tell we won't either. It waits for us.
        But as soon we send converged=True in the json, TRNSYS might converge, so we need to wait for it. Once Type 899
        sent True in the json, TRNSYS is hopping to the next timestep we must do as well.
        
        Unlike this python script, Type 899 has no means to know whether the TRNSYS simulation is ready to converge or
        not. This is why the convergence handling is so odd.
        """

        # Tells whether TRNSYS is about to converge. It will be set to received["header"]["converge"].
        trnsys_converged = False

        # How many iterations are required for our simulation to converge. Once it is reached, we send through the json
        # that we are ready. This would normally be decided dynamically according to the state of the simulation.
        iterations_to_converge = randint(1, 10)

        iteration = 0
        sent_heat_iter = 0.0
        received_heat_iter = 0.0
        # enters in the iterative loop
        while not trnsys_converged:
            iteration += 1

            # received Trnsys' data
            received = sock.recv(4096)
            received = json.loads(received.decode("ascii"))

            trnsys_converged = received["header"]["converged"]

            outlet_temp = received["payload"]["Object"]["name"]["outlet_temperature"]
            outlet_flow = received["payload"]["Object"]["name"]["outlet_flowrate"]
            received_heat_iter = outlet_flow * 0.5 * (outlet_temp + 273.15) * 4.19

            # our computation
            radiation, inlet_temp, inlet_flow = compute(step / step_count * 1.5, outlet_temp, outlet_flow)

            # compute our convergence, normally decided according to the variables' behaviour.
            we_converged = iteration >= iterations_to_converge
            # 'True' does not mean we will necessarily converge, because we must wait for TRNSYS.

            # prepare the json to send
            header = {"step": step, "converged": we_converged}
            payload = {"Object": {"name": {"inlet_temperature": inlet_temp,
                                           "inlet_flowrate": inlet_flow,
                                           "radiation": radiation}}}
            tosend = {"header": header, "payload": payload}
            # and send it
            sock.send(json.dumps(tosend).encode("ascii"))

            # just in case, this should not happen.
            if step != received["header"]["step"]:
                print(f"Sync is lost!\nOur step: {step}\nTRNSYS step: {received['header']['step']}")

        iterations.append(iteration)
        sent_heat += sent_heat_iter
        received_heat += received_heat_iter

    # end of the simulation
    sock.close()
    average_power = (received_heat - sent_heat) / step_count / 0.5 / 3.6
    print(f"Terminated. Average power of TRNSYS's solar collector: {average_power} W.")
    print("\nNumber of iterations:", iterations)


if __name__ == "__main__":
    simulation()
    # iterative_simulation()
