import json
import socket
from math import sin, pi
from random import randint
from threading import Thread
import os
from io import StringIO


def test_prevent_trnsys_convergence():
    """
    Simulates a Dimosim simulation (lol) and tests Trnsys' ability to refrain its convergence.

    This test should be run with a specific Trnsys project, named TestPreventTrnsysConvergence. In this test, the
    Trnsys kernel will converge in one or two iterations only (2 at the first step and 1 afterwards), while Dimosim
    will require a random amount of iterations to converge. So Trnsys has got to wait before it converges.
    """

    step_count = 24

    init_json = {
        "from_dimosim": {"Object": {"name": {"sinus": 0}}},
        "to_dimosim": {"Object": {"name": {"time": 0}}},
        "metadata": {
            "step_number": step_count,
            "sample_time": 0.5,
            "is_iterative": True,
            "start_time": 7.0
        }
    }

    serv = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    serv.bind(("::1", 9999))
    serv.listen(1)
    print("Launching TRNSYS...")
    launch_external_prog("C:/TRNSYS18/Exe/TrnEXE64.exe", "C:/TRNSYS18/Examples/Calling_socket/Socket_server/TestPreventTrnsysConvergence.dck")
    sock, _ = serv.accept()
    serv.close()

    sock.sendall(json.dumps(init_json).encode())
    received = sock.recv(4096).decode()
    print("Received:", json.dumps(json.loads(received), indent=4))

    # how many iterations are required for this pseudo-simulation to converge.
    min_iters = [randint(1, 20) for _ in range(step_count)]
    iters = []
    # we enter the simulation
    for step in range(step_count):
        value = sin(step / 24. * 5. * pi)
        trnsys_converged = False
        dimosim_converged = False
        iteration = 0
        while (not dimosim_converged) or (not trnsys_converged):
            # call to IterativeSocketCoupling
            recv = sock.recv(4096)
            header = json.loads(recv.decode())["header"]
            assert step == header["step"], f"timesteps not synchronized (step {step}, iter {iteration})"
            trnsys_converged = header["converged"]

            header = {"step": step, "converged": dimosim_converged}
            tosend = json.dumps({
                "header": header,
                "payload": {"Object": {"name": {"sinus": value}}}
            }).encode()
            sock.sendall(tosend)

            # calls to other dimosim components
            # ...

            # things happening between two iterations in Dimosim
            iteration += 1
            dimosim_converged = not iteration < min_iters[step]

        iters.append(iteration)

    # the simulation ended
    assert len(sock.recv(16)) == 0, "Expected an empty array, as trnsys should have disconnected."

    print("Dimosim minimal iteration requirements:", min_iters)
    print("Reality:                               ", iters)

    for i in range(step_count):
        assert iters[i] == min_iters[i] + 2, "The simulations should take two more iterations than Dimosim needs."

def launch_external_prog(exe_path, args):
    """
    Launch an executable file with the provided arguments and environment variables. Intended to execute the client
    program dimosim wants to be coupled with, throught the address (host, port). This function might throw a wide range
    of errors.

    Args:
        exe_path: Path (absolute or relative) to a file to execute as a child process. Try with '/' first if possible.
        args: A string or a list or tuple of strings. Passed as arguments to the spawned process.

    Returns: The PID of the spawned process.
    """

    # the arguments passed to os.spawnv() must be in a list, args is casted so.
    if not isinstance(args, list):
        # tuples are turned into lists, otherwise we consider it's a singleton
        args = list(args) if isinstance(args, tuple) else [args]

    # Check the executable, just in case.
    if not os.access(exe_path, os.X_OK):
        print("File to execute '%s' does not exist or it is allowed to execute." % exe_path)
    # Finally, we spawn the process.
    pid = os.spawnv(os.P_NOWAIT, exe_path, [exe_path] + args)
    print("Executed a process instance. (%d)" % pid)
    return pid

if __name__ == "__main__":
    test_prevent_trnsys_convergence()