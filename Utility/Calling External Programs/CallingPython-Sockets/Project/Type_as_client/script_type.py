"""
Example of coupling between a TRNSYS simulation and a python script, via Type 899b and sockets.

The script sends some cold water and receives it heated by Type 74 in the TRNSYS simulation. It performs some
calculation in-between with the compute() function.

How to use:
    Launch the Trnsys project "Project.dck" which should execute this script with python automatically.

It can also be launched manually. For that, use Type 899a.
In that case the python script must open a server socket.
"""


import json
import socket
from random import randint
#import time


def compute(outlet_temp, outlet_flow):
    """ The function to be integrated in TRNSYS. Its inputs must
    come from Type 899 and its output must go back to it. """
    radiation = 10.0 - outlet_temp / 10.0
    inlet_temp = 5.0 + outlet_temp / 1000.0  # some random calculations
    inlet_flow = 100.0 + outlet_flow / 10000.0
    return radiation, inlet_temp, inlet_flow


def python_type():
    """ Couple a python function with Type 899 and call the function compute() """

    # The first piece of data we will send to Trnsys.
    init_json = {
        # Coupled variables. (eg "radiation")
        # They must map to an index [0, 1, ...], following the order in the TRNSYS Type 899 input/output lists.
        "from_dimosim": {"Object": {"name": {"inlet_temperature": 1, "inlet_flowrate": 0, "radiation": 2}}},
        "to_dimosim": {"Object": {"name": {"outlet_flowrate": 0, "outlet_temperature": 1}}},
    }

    # Open the client socket to connect to the Type 899b's server socket.
    sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    sock.connect(("::1", 9999))
    print("Socket connected.")

    # first data exchange
    first_received = sock.recv(4096)  # Type 899 sends some data about TRNSYS' simulation configuration.
    first_received = json.loads(first_received.decode("ascii"))
    print("Received:", json.dumps(first_received, indent=4))

    data = json.dumps(init_json)  # transform the init json into a string
    encoded_data = data.encode("ascii")  # encode the string to a byte array
    sock.send(encoded_data)  # send to the client

    # some assesment data, used to print a result at the end of the simulation
    sent_heat = 0.0
    received_heat = 0.0

    # enter the loop that maps to TRNSYS' own simulation loop
    keep_on = True
    while keep_on:
        #time.sleep(0.5)
        # first, received Trnsys' data. Despite Type 899 being the socket-wise server, it sends first and receives last.
        received = sock.recv(4096)

        # convert to a python dictionary object
        received = json.loads(received.decode("ascii"))
        # fetches the step of the TRNSYS simulation
        step = received["header"]["step"]
        #print(step)
        # if it's the last json, keep_on becomes False and the loop ends.
        keep_on = not received["header"]["is_last"]
        # extract the variables values
        outlet_temp = received["payload"]["Object"]["name"]["outlet_temperature"]
        outlet_flow = received["payload"]["Object"]["name"]["outlet_flowrate"]
        #import time
        #time.sleep(1)
        #print(step, outlet_temp)

        # the enegy collected by the solar panel could be obtained directly via its output "useful energy gain"
        # but for the example, the script computes it so to simulate some calculation.
        # energy = flow rate * time * temperature (K) * specific heat capacity
        received_heat += outlet_flow * 0.5 * (outlet_temp + 273.15) * 4.19

        # Now we can compute the values we want to send back to Trnsys.
        radiation, inlet_temp, inlet_flow = compute(outlet_temp, outlet_flow)

        sent_heat += inlet_flow * 0.5 * (inlet_temp + 273.15) * 4.19

        # Prepare the json to send
        header = {"step": step, "converged": True}  # always send the same step as received, and "converged" to True.
        payload = {"Object": {"name": {"inlet_temperature": inlet_temp,
                                       "inlet_flowrate": inlet_flow,
                                       "radiation": radiation}}}
        tosend = {"header": header, "payload": payload}
        # and send it
        sock.send(json.dumps(tosend).encode("ascii"))

    # end of the simulation
    sock.close()
    average_power = (received_heat - sent_heat) / first_received["step_number"] / first_received["sample_time"] / 3.6
    print(f"Terminated. Average power of TRNSYS's solar collector: {average_power} W.")


if __name__ == "__main__":
    python_type()
