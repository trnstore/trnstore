#ifndef _JSON_FORMATTER_H_
#define _JSON_FORMATTER_H_

//! A C++ class to make the handling of json formatting a bit more comfortable for the
//! main function. The existence of this class is entirely due to the fact that jsons must
//! contains the variables with the names of the Dimosim objects, instead of simply embedding
//! a list of all the variables in the same order as in Type 899's input/output list.
//! As such, two model jsons must be stored in a global scope, hence this class which hides
//! them away from the business of Type 899's main routine.
//! 
//! But if the format of the jsons ever changes to a simple variable list, make sure to
//! remove this ugly class and replace it by classic functions, as it was before August 31st
//! 2021, look in the git history.

#include "json.hpp"

/// <summary>
/// A class to encapsulate the json models sent by Dimosim, as the building and processing
/// of jsons currently require to know the formatting model, sent in the first init json
/// by Dimosim. Once instanciated with said init json, objects of this class can build
/// jsons to send from the variables (method wrap), and extract them from received jsons
/// (method unwrap).
/// </summary>
class JsonFormatter {
private:
	// These two are copies from dimosim's first transmitted json. They map each variable
	// to an index, so that the order of the variables is known.
	nlohmann::json to_send_model;
	nlohmann::json to_recv_model;

public:
	// default constructor so not to be annoyed by initialisation
	JsonFormatter() : to_recv_model{ nlohmann::json::object() },
		to_send_model{ nlohmann::json::object() } {};
	// actual constructor
	JsonFormatter(std::string s);

	void unwrap(const std::string& s) const;
	int wrap(
		std::string& target,
		unsigned int step,
		bool converged,
		bool is_last
	) const;
};


#endif
