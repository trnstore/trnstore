//! Client - server related stuff.
//! 
//! It handles the connection, initializes it, closes it, and in the meantime
//! it can even send and receive data. The error handling is primitive:
//! basically if an I/O error happens, the functions will return their error code.
//! init_connection only is smart enough to close the connection itself in error case.
//! It is possible to re-init the connection, though it's against the global desgin.
//! 
//! 
//! Use this module by calling, in this order:
//! 
//! socket = init_connection()
//! while(running) {
//!	  string_received = transmit_data(socket, string_to_send)
//! }
//! close_connection(socket)
//!
//! Its name "client.cpp" is due to Type899 being initially designed to handle a client
//! socket, always. But now it can also open a server socket. Oh and it can launch a
//! python process with the file specified in the External Files menu of Type 899b.

#include <cstdlib>
#include <cstdio>

#pragma comment(lib, "ws2_32.lib")

#include "framework.h"
#include <ws2tcpip.h>

/// <summary>
/// Attempts to connect the addresses and returns a socket as soon as a connection
/// is successfully established. Returns INVALID_SOCKET if an error occurs or no
/// address works.
/// </summary>
static SOCKET client_connect(struct addrinfo* addrinfos) {
	SOCKET socketfd = INVALID_SOCKET;
	// Iterate over all the possible sockets and addresses given by getaddrinfo.
	// This will likely be the two loopbacks "127.0.0.1:port" and "[::1]:port".
	for (struct addrinfo* ai = addrinfos; ai != NULL; ai = ai->ai_next) {
		// initialise a socket
		socketfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
		if (socketfd == INVALID_SOCKET) {
			PrintError();
			continue;
		}
		// connect
		int r = connect(socketfd, (SOCKADDR*)ai->ai_addr, (int)ai->ai_addrlen);
		if (r != SOCKET_ERROR) {
			// Ok, the socket is good.
			break;
		}
		if (closesocket(socketfd))
			PrintError();
		socketfd = INVALID_SOCKET;

		// One way for it to fail is to open the dimosim server on IPv4
		// while another foreigner server is listening on the same port
		// but on IPv6. Then this trnsys client will connect to the wrong server...
		// However, it is unlikely.
	}
	return socketfd;
}
/// <summary>
/// Opens a server for the addrinfo pointed the argumemnt. Then it lauches a python
/// script specified in the external files and returns a
/// socket connected with this script. If the file is not found, if python_file_path
/// is NULL or points to a '\0', then an error is logged and not server is set up, and
/// INVALID_SOCKET is returned.
/// </summary>
static SOCKET server_connect_python_launch(struct addrinfo* ai, const char* python_file_path) {
	if (python_file_path == NULL || python_file_path[0] == '\0') {
		throw_error("The python file path is unspecified or empty.");
		return INVALID_SOCKET;
	}
	// We don't iterate over the linked list of addrinfo (ai), as that would mean listening
	// to many sockets with some complex async i/o. This is not Type 899's job.
	// Instead we take into account the first entry of the list, and that's it. There should
	// be only one addrinfo entry if the user's input is an IP address. (not a domain name)

	int r;
	// initialise a socket
	SOCKET socketfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
	if (socketfd == INVALID_SOCKET) {
		PrintError();
		return INVALID_SOCKET;
	}
	// Set socket option RE USE ADDR, so that multiple Type899s or simulations can run
	// almost simultaneously, bypassing TCP's TIME_WAIT state. I hope this option does
	// prevent two sockets from binding on the same address when none of them is in the
	// TIME_WAIT state. In any case, if two Type899s try to bind at the time without
	// clients connecting, errors will arise.
	// docs.microsoft.com/en-us/windows/win32/winsock/using-so-reuseaddr-and-so-exclusiveaddruse
	BOOL optval = 1;
	r = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, (char*)&optval, sizeof(BOOL));
	if (r == SOCKET_ERROR)
		PrintError(); // if set_sockopt fails, we keep on regardless.
	// bind and listen
	r = bind(socketfd, (SOCKADDR*)ai->ai_addr, (int)ai->ai_addrlen);
	if (r == SOCKET_ERROR) {
		PrintError();
		closesocket(socketfd);
		return INVALID_SOCKET;
	}
	r = listen(socketfd, 0);
	if (r == SOCKET_ERROR) {
		PrintError();
		closesocket(socketfd);
		return INVALID_SOCKET;
	}
	// Launch the python script.

	// First we check if the file exists.
	HANDLE file = CreateFileA(python_file_path, GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (file == INVALID_HANDLE_VALUE) {
		// error: file probably does not exist
		int error = GetLastError();
		char buf[512];
		size_t l = sprintf_s(buf, "Error accessing file \"%s\": ", python_file_path);
		if (buf[0] != '\0') {
			strerror_s(buf + l, sizeof(buf) - l, error);
			throw_error(buf);
		}
		else
			throw_error("Error accessing python file. Double error "
				"formatting the cause of the first one.");
		closesocket(socketfd);
		return INVALID_SOCKET;
	}
		
	size_t len = 16 + strnlen(python_file_path, 512);
	LPSTR command = (LPSTR)malloc(sizeof(char) * len);
	if (command == NULL) {
		closesocket(socketfd);
		throw_error("Fatal: malloc() failed.");
		return INVALID_SOCKET;
	}
	sprintf_s(command, len, "\"python\" \"%s\"", python_file_path);
	if (command[0] == '\0') {
		// sprintf_s has failed, due to the buffer not being large enough.
		throw_error("The python file path in the special card is too long.");
		free(command);
		closesocket(socketfd);
		return INVALID_SOCKET;
	}

	// Launches the process. Inspired from
	// https://docs.microsoft.com/en-us/windows/win32/procthread/creating-processes
	auto dwCreationFlags = (should_create_console_for_python_script()) ?
		CREATE_NEW_CONSOLE : CREATE_NO_WINDOW;
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	// Start the child process.
	if (!CreateProcessA(NULL,
		command,   // Command line
		NULL,
		NULL,
		FALSE,
		dwCreationFlags,
		NULL,
		NULL,
		&si,
		&pi)
	) {
		throw_error("CreateProcess failed. Couldn't launch python.");
		free(command);
		closesocket(socketfd);
		return INVALID_SOCKET;
	}
	else {
		// Close process and thread handles.
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
	free(command);
	/*
	If the python file path is mistyped and python cannot launch it, there will be
	no backlog to inform the user. All hope resides in the CreateFileA() call that
	checks the existence of the file beforehand.

	About security, I suppose it couldn't be easier to write "script.py && virus.exe"
	as the python file path so it executes virus.exe after python. But it doesn't add
	a lot of risk since the hacker can probably put the virus in the python script.
	The socket work also add security issues.
	*/

	// accept the client connection
	SOCKET client_socket = accept(socketfd, NULL, NULL);
	// It is not this function's job to check the state of the client_socket.
	// We close the server and replace its socket by the client one.
	if (closesocket(socketfd))
		PrintError();
	return client_socket;
}

/// <summary>
/// Initialise the connection with a dimosim instance, which must have set up
/// a listening server beforeward. The function automatically fetches the host
/// and port through the special card/labels. The first exchange of the init
/// jsons is also operated by this function, widening the range of errors it may
/// encounter. Supports IPv4 and IPv6.
/// </summary>
/// <param name="first_exchange">- A mutable reference to a string that contains
/// the init JSon, returned by build_init_json. Then the function writes in this
/// string the init JSon of Dimosim. </param>
/// <returns> If successful, returns a SOCKET value to be passed to other
/// socket-related functions. close_connection(socket) must be called with the
/// returned SOCKET when the job is done.
/// If an error occured, INVALID_SOCKET is returned.
/// </returns>
SOCKET init_connection(std::string & first_exchange) {
	// Call WSAStartup() before using Windows sockets. It's okay to call it multiple times, which
	// will happen if the simulation contains multiple instances of Type 899, as long as the call
	// to WSACleanup() match
	WSADATA WsaData;
	if (WSAStartup(MAKEWORD(2, 2), &WsaData) != 0) {
		PrintError();
		return INVALID_SOCKET;
	}

	int r;

	// the server's address to connect to
	char HOST[64] = { 0 };
	char PORT[8] = { 0 };
	r = get_address_from_special_card(&HOST, &PORT);
	if (r) {
		warning("Failed to fetch HOST or PORT from the special card.");
		WSACleanup();
		return INVALID_SOCKET;
	}

	// address info
	struct addrinfo hints = { 0 };
	struct addrinfo *addrinfos;

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	r = getaddrinfo(HOST, PORT, &hints, &addrinfos);
	if (r) {
		char buf[1024];
		size_t offset = sprintf_s(buf, "getaddrinfo [%s]:%s, ", HOST, PORT);
		wcstombs_s(NULL, buf + offset, sizeof(buf) - offset, gai_strerror(r), _TRUNCATE);
		warning(buf);
		WSACleanup();
		return INVALID_SOCKET;
	}

	// Now... this code belongs no where in Type 899. It fetches the path of a python
	// script file and if it's not NULL, then we switch to a server socket and
	// launch the script. This way Type 899 can be paired with arbitrary python code
	// (or whatever program the python script may launch) instead of Dimosim.
	char* python_file_path = get_python_script_path_from_external_files();
	bool in_client_mode = (python_file_path == NULL);

	SOCKET socketfd;
	if (in_client_mode)
		socketfd = client_connect(addrinfos);
	else {
		socketfd = server_connect_python_launch(addrinfos, python_file_path);
		free(python_file_path);
	}
	freeaddrinfo(addrinfos);

	char buffer[8192];

	if (socketfd == INVALID_SOCKET) {
		if (in_client_mode)
			sprintf_s(buffer,
				"Couldn't connect to [%s]:%s, no response. Is the server up and are the addresses correct?",
				HOST, PORT);
		else
			sprintf_s(buffer,
				"Couldn't accept a connection to our server [%s]:%s.",
				HOST, PORT);
		warning(buffer);
		WSACleanup();
		return INVALID_SOCKET;
	}
	
	if (in_client_mode)
		sprintf_s(buffer, "Connected to [%s]:%s.", HOST, PORT);
	else
		sprintf_s(buffer, "A client connected."); // But the address will remain a mystery.
	notice(buffer);

	r = send(socketfd, first_exchange.c_str(), (int) first_exchange.length(), 0);
	if (r == SOCKET_ERROR) {
		PrintError();
		close_connection(socketfd);
		warning("Error sending data to the server.");
		return INVALID_SOCKET;
	}

	// receive back from the server
	r = recv(socketfd, buffer, sizeof(buffer) - 1, 0);
	if (r == SOCKET_ERROR) {
		PrintError();
		close_connection(socketfd);
		warning("Error receiving data from the server.");
		return INVALID_SOCKET;
	} else if (r == sizeof(buffer) - 1) {
		warning("The first received string is a bit too long... errors may happen ahead.");
	} else if (r == 0) {
		close_connection(socketfd);
		warning("The first received message is empty, the server might have closed connection already.");
		return INVALID_SOCKET;
	}
	buffer[r] = '\0';

	first_exchange = std::string(buffer);

	return socketfd;
}


/// <summary>
/// Send data to Dimosim and receive Dimosim's data in return. Call everytime you want to
/// exchange the coupled variables.
/// </summary>
/// <param name="socketfd">- The SOCKET handling the connection to work on.</param>
/// <param name="data">- The JSon to send, stringified as <code>wrap_json()</code> returns.</param>
/// <returns>If successful, returns whatever Dimosim sent under the form of a C++ string. Though it
/// is designed to be processed by <code>unwrap_json</code>.
/// If an error occurred, returns an empty string. </returns>
std::string transmit_data(SOCKET socketfd, std::string data) {
	// First, check the validity of the socket. This step may disappear. It may prove good only for debugging.
	if (socketfd == INVALID_SOCKET) {
		warning("transmit_data() was called with an INVALID_SOCKET. "
			"Initialize a socket with init_connection().");
		return std::string();
	}

	int r;

	// SEND //
	r = send(socketfd, data.c_str(), (int)data.size(), 0);

	if (r == SOCKET_ERROR) {
		PrintError();
		return std::string();
	} else if (r != (int)data.size()) {
		warning("Send did not send all its data. This error is not handled by this type.");
		return std::string();
	}

	// RECEIVE //
	static char buffer[8192];

	r = recv(socketfd, buffer, (int)sizeof(buffer) - 1, 0);
	if (r == SOCKET_ERROR) {
		PrintError();
		return std::string();
	}
	else if (r == 0) {
		warning("The remote socket disconnected gracefully, yet unexpectedly");
		return std::string();
	}
	else if (r == sizeof(buffer) - 1) {
		warning("The remote socket sent over 8192 bytes of data. Buffer overflow warning.");
	}
	
	buffer[r] = '\0';

	return std::string(buffer);
}


/// <summary>
/// Close the connection to the server (or client) and release the Windowns Socket
/// library's ressources (unless another TYPE 899 instance is still using it).
/// Call this function at the end of the simulation with the SOCKET given by
/// init_connection().
/// </summary>
/// <param name="socketfd">- The SOCKET to close.</param>
/// <returns>0 if the socket and stuff closed successfully,
/// 1 if socketfd is INVALID_SOCKET (then nothing is done),
/// -1 if an error occured.</returns>
int close_connection(SOCKET socketfd) {
	if (socketfd != INVALID_SOCKET) {
		int r = closesocket(socketfd);
		// tell that socketfd has returned to an invalid or uninitialised state.
		socketfd = INVALID_SOCKET;
		if (r) {
			PrintError();
			WSACleanup();
			return -1;
		}
		WSACleanup();
		return 0;
	} else {
		return 1;
	}
}

// Inspired from https://docs.microsoft.com/en-us/windows/win32/winsock/ipv6-enabled-client-code-2
void PrintError() {
	static char message[1024];

	// If this program were multithreaded, we'd want to use
	// FORMAT_MESSAGE_ALLOCATE_BUFFER instead of a static buffer here.
	// (And of course, free the buffer when we were done with it)

	FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS |
		FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		message, 1024, NULL);

	warning(message);
}
