/*
 * Here we handle all the JSon-related stuff.
 * The methods of the class JsonFormatter are defined here.
 * 
 */

// https://github.com/nlohmann/json
#include "json.hpp"

#include "framework.h"
#include "JsonFormatter.h"

#include "TRNSYS.h"

using json = nlohmann::json;
using namespace std;

string build_init_json(const struct init_data_to_dimosim * d) {

	json j = json::object();

	j["step_number"] = d->step_number;
	j["sample_time"] = d->timestep_duration;
	j["start_time"] = d->start_hour_of_year;
	j["convergence_threshold"] = d->convergence_threshold;
	j["is_iterative"] = d->is_iterative;
	
	return j.dump();
}

/// <summary>
/// Extract from the json's header the values of 'time_step' and 'converged'.
/// </summary>
/// <param name="s">- A string containing the json whose header must be inspected.</param>
/// <returns>(time_step, converged)</returns>
pair<unsigned int, bool> unwrap_header(const string & s) {
	json j = json::parse(s);
	auto converged = j["header"]["converged"];
	auto time_step = j["header"]["step"];
	return pair<unsigned int, bool>(time_step, converged);
}

/// <summary>
/// Construct a new JsonFormatter object from the models in the init json. It will then
/// be able to extract the coupled variable from received exchange jsons and build new
/// ones to send, embeding our coupled variables.
/// </summary>
/// <param name="s">- The first string received from the foreign simulation.</param>
JsonFormatter::JsonFormatter(string s) {
	// Transform the string into a json.
	json j = json::parse(s);

	// json templates! Copy them to a new json variable to copy the model. Hopefully.
	this->to_send_model = j["to_dimosim"];
	this->to_recv_model = j["from_dimosim"];
}

/* Note about optimization: unwrap_header(s) and JsonFormatter::unwrap(s) both parse
 * their argument 's', which is not optimal as the string s is parsed at least twice,
 * for no reason other that making the calls in the main .cpp file simpler. Sadly, it
 * is important that the header is inspected before we can know whether we must write
 * down the output values.
 */

/// <summary>
/// Extract variables' values from a json-format string, allegedly
/// received from dimosim, and set them as this Type's output values.
/// </summary>
/// <param name="s">- The string to read the json and variables from.</param>
void JsonFormatter::unwrap(const string & s) const {

	json j = json::parse(s);

	j = j["payload"];

	for (const auto& classes : j.items())
		for (const auto& objs : classes.value().items())
			for (const auto& vars : objs.value().items())
			{
				int index = this->to_recv_model[classes.key()][objs.key()][vars.key()];
				index += 1;
				double val = vars.value();
				setOutputValue(&index, &val);
			}

}

/// <summary>
/// Build a json-formatted string to contain the variables' values
/// to send to dimosim, and some metadata. The variables are read with
/// getInputValues() in the index order 1, 2, ...
/// </summary>
/// <param name="target">- Reference to set to the new string.</param>
/// <param name="step">- The current, 0-based, time step.</param>
/// <param name="converged">- The boolean returned by <code>getIsEndOfTimestep()</code></param>
/// <param name="is_last">- True if this json is the last one and the
/// simulation will end afterwards. False otherwise. </param>
/// <returns>0</returns>
int JsonFormatter::wrap(string & target, unsigned int step, bool converged, bool is_last) const {
	
	json payload = this->to_send_model;

	for (auto& objs : payload)
		for (auto& vars : objs)
			for (auto& value : vars)
				// value is initially the index (in var_vec) of the current variable
				// we re-assign value to be the acutal value of type double for the variable
			{
				int index = value + 1;
				value = getInputValue(&index);
			}
	
	json j = json::object();
	j["header"] = {};
	j["header"]["step"] = step; // Maybe it'll become a struct. "json_header", or "metedata"
	j["header"]["converged"] = converged;
	j["header"]["is_last"] = is_last;

	j["payload"] = payload;

	target = j.dump();

	return 0;
}