#ifndef _FRAMEWORK_H_
#define _FRAMEWORK_H_

#define WIN32_LEAN_AND_MEAN             // Exclure les en-têtes Windows rarement utilisés
// Fichiers d'en-tête Windows
#include <windows.h>
#include <winsock2.h>
#include <string>
#include <vector>
#include <cstdlib>
// The other header file, which contains a C++ class.
#include "JsonFormatter.h"

void throw_error(const char *);
void warning(const char* msg);
void notice(const char* msg);
void PrintError();
int get_address_from_special_card(char(*)[64], char(*)[8]);
bool should_create_console_for_python_script();
char* get_python_script_path_from_external_files();

SOCKET init_connection(std::string &s);
std::string transmit_data(SOCKET, std::string data);
int close_connection(SOCKET);

struct init_data_to_dimosim {
	unsigned int step_number;
	double timestep_duration;
	double start_hour_of_year;
	double convergence_threshold;
	bool is_iterative;
};

std::string build_init_json(const struct init_data_to_dimosim *data);
std::pair<unsigned int, bool> unwrap_header(const std::string& s);

#endif